from django.http import HttpResponse
from rest_framework import generics, filters
from serializers import *
from django.core import serializers
from django.db import connection
import json
from core.mixins import DepthSerializerMixin
from django.core.exceptions import ObjectDoesNotExist
import decimal
import numpy as np
from django.db.models import Q
import math
from rest_framework.response import Response
from rest_framework import status
from core.permissions import *
from rest_framework.decorators import api_view, permission_classes
from openpyxl import load_workbook
from openpyxl.styles import PatternFill, Font
from functools import reduce
from itertools import groupby
from operator import add, itemgetter
from collections import Counter
from core import models as core_models
from filters import *
from costmodel.tasks import create_plant_report, create_plant_group_report


class PlantGroupList(DepthSerializerMixin, generics.ListCreateAPIView):

    queryset = PlantGroup.objects.all()
    serializer_class = PlantGroupSerializer
    permission_classes = (permissions.IsAuthenticated, BelongsToOrganization, UserPermissions)
    filter_class = PlantGroupFilter

    def get_queryset(self):
        org_pk = self.kwargs['org_pk']
        return self.queryset.filter(organization=org_pk)


class PlantGroupDetail(DepthSerializerMixin, generics.RetrieveUpdateDestroyAPIView):

    queryset = PlantGroup.objects.all()
    serializer_class = PlantGroupSerializer
    permission_classes = (permissions.IsAuthenticated, BelongsToOrganization, UserPermissions)

    def get_queryset(self):
        org_pk = self.kwargs['org_pk']
        return self.queryset.filter(organization=org_pk)

    def put(self, request, org_pk, pk, version):
        # plant_group = self.get_object(pk)
        plant_group = PlantGroup.objects.get(id=pk)
        print(plant_group)
        serializer = PlantGroupSerializer(plant_group, data=request.data)
        if serializer.is_valid():
            plant_group = serializer.save()

            if 'cost_model' in request.data.keys() and 'plant_list' in request.data.keys():
                cost_model = CostModel.objects.get(id=request.data['cost_model'])
                plant_list = request.data['plant_list']
                pg_plant_list = []
                for plant in plant_group.plant.all():
                    pg_plant_list.append(plant.id)

                for plant in plant_list:
                    if long(plant) not in pg_plant_list:
                        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
                for plant in plant_list:
                    p = core_models.Plant.objects.get(id=plant)
                    p.cost_model.add(cost_model)
                    p.active_cost_model_id = cost_model
                    p.save()
            return Response(serializer.data)


class CostModelList(DepthSerializerMixin, generics.ListCreateAPIView):

    queryset = CostModel.objects.all()
    serializer_class = CostModelSerializer
    permission_classes = (permissions.IsAuthenticated, BelongsToOrganization, UserPermissions, HasCostModelLicense)
    filter_class = CostModelFilter
    lookup_url_kwarg = "org_pk"

    def get_queryset(self):
        """
        Restricts the returned Cost Models to a given organization.
        Optionally add a 'default' query parameter in the URL to include defaults.
        """
        org_pk = self.kwargs.get(self.lookup_url_kwarg)
        queryset = CostModel.objects.all()
        global_org = core_models.Organization.objects.filter(name="Default")
        organization = self.kwargs['org_pk']
        default = self.request.query_params.get('default', None)

        if default is not None:
            if default == "true":
                    queryset = queryset.filter(Q(organization=organization) | Q(organization=global_org))
            elif default == "false":
                    queryset = queryset.filter(organization=organization)
            else:
                queryset = queryset.filter(Q(organization=organization) | Q(organization=global_org))
        elif org_pk ==1:
            queryset = queryset.filter(Q(organization=organization) | Q(organization=global_org))
        else:
            queryset = queryset.filter(Q(organization=organization))
        return queryset


class CostModelDetail(DepthSerializerMixin, generics.RetrieveUpdateDestroyAPIView):

    queryset = CostModel.objects.all()
    serializer_class = CostModelSerializer
    permission_classes = (permissions.IsAuthenticated, BelongsToOrganization, UserPermissions)

    def get_queryset(self):
        org_pk = self.kwargs['org_pk']
        return self.queryset.filter(organization=org_pk)


class ServiceList(DepthSerializerMixin, generics.ListCreateAPIView):

    queryset = Service.objects.all()
    serializer_class = ServiceSerializer
    permission_classes = (permissions.IsAuthenticated, BelongsToOrganization, UserPermissions, HasCostModelLicense)
    filter_class = ServiceFilter

    def get_queryset(self):
        """
        Restricts the returned Service Types to a given organization.
        Optionally add a 'default' query parameter in the URL to include defaults.
        """
        queryset = Service.objects.all()
        global_org = core_models.Organization.objects.filter(name="Global")
        organization = self.kwargs['org_pk']
        default = self.request.query_params.get('default', None)
        if default is not None:
            if default == "true":
                queryset = queryset.filter(Q(organization=organization) | Q(organization=global_org))
            elif default == "false":
                queryset = queryset.filter(organization=organization)
            else:
                queryset = queryset.filter(Q(organization=organization) | Q(organization=global_org))
        else:
            queryset = queryset.filter(Q(organization=organization) | Q(organization=global_org))
        return queryset


class ServiceDetail(DepthSerializerMixin, generics.RetrieveUpdateDestroyAPIView):

    queryset = Service.objects.all()
    serializer_class = ServiceSerializer
    permission_classes = (permissions.IsAuthenticated, BelongsToOrganization, UserPermissions, HasCostModelLicense)

    def get_queryset(self):
        org_pk = self.kwargs['org_pk']
        return self.queryset.filter(organization=org_pk)


class ServiceValuesList(DepthSerializerMixin, generics.ListCreateAPIView):
    queryset = ServiceValues.objects.all()
    serializer_class = ServiceValuesSerializer
    permission_classes = (permissions.IsAuthenticated, BelongsToOrganization, UserPermissions, HasCostModelLicense)
    filter_class = ServiceValuesFilter

    def get_queryset(self):
        """
        Restricts the returned Services to a given organization.
        Optionally add a 'default' query parameter in the URL to include defaults.
        """
        queryset = ServiceValues.objects.all()
        global_org = core_models.Organization.objects.filter(name="Global")
        organization = self.kwargs['org_pk']
        default = self.request.query_params.get('default', None)
        if default is not None:
            if default == "true":
                queryset = queryset.filter(Q(organization=organization) | Q(organization=global_org))
            elif default == "false":
                queryset = queryset.filter(organization=organization)
            else:
                queryset = queryset.filter(Q(organization=organization) | Q(organization=global_org))
        else:
            queryset = queryset.filter(Q(organization=organization) | Q(organization=global_org))
        return queryset


class ServiceValuesDetail(DepthSerializerMixin, generics.RetrieveUpdateDestroyAPIView):

    queryset = ServiceValues.objects.all()
    serializer_class = ServiceValuesSerializer
    permission_classes = (permissions.IsAuthenticated, BelongsToOrganization, UserPermissions, HasCostModelLicense)

    def get_queryset(self):
        org_pk = self.kwargs['org_pk']
        return self.queryset.filter(organization=org_pk)


class FailureModeList(generics.ListCreateAPIView):

    queryset = FailureMode.objects.all()
    serializer_class = FailureModeSerializer
    permission_classes = (permissions.IsAuthenticated, UserPermissions, HasCostModelLicense)
    filter_class = FailureModeFilter


class FailureModeDetail(generics.RetrieveUpdateDestroyAPIView):

    queryset = FailureMode.objects.all()
    serializer_class = FailureModeSerializer
    permission_classes = (permissions.IsAuthenticated, UserPermissions, HasCostModelLicense)


class FailureModeValuesList(generics.ListCreateAPIView):

    queryset = FailureModeValues.objects.all()
    serializer_class = FailureModeValuesSerializer
    permission_classes = (permissions.IsAuthenticated, UserPermissions, HasCostModelLicense)
    filter_class = FailureModeValuesFilter


class FailureModeValuesDetail(generics.RetrieveUpdateDestroyAPIView):

    queryset = FailureModeValues.objects.all()
    serializer_class = FailureModeValuesSerializer
    permission_classes = (permissions.IsAuthenticated, UserPermissions, HasCostModelLicense)


class ApplicableUnitList(generics.ListCreateAPIView):

    queryset = ApplicableUnit.objects.all()
    serializer_class = ApplicableUnitSerializer
    permission_classes = (permissions.IsAuthenticated, CanEditEnnumerations, HasCostModelLicense)
    filter_class = ApplicableUnitFilter


class ApplicableUnitDetail(generics.RetrieveUpdateDestroyAPIView):

    queryset = ApplicableUnit.objects.all()
    serializer_class = ApplicableUnitSerializer
    permission_classes = (permissions.IsAuthenticated, CanEditEnnumerations, HasCostModelLicense)


class FailureDistributionTypeList(generics.ListCreateAPIView):

    queryset = FailureDistributionType.objects.all()
    serializer_class = FailureDistributionTypeSerializer
    permission_classes = (permissions.IsAuthenticated, CanEditEnnumerations, HasCostModelLicense)
    filter_class = FailureDistributionTypeFilter


class FailureDistributionTypeDetail(generics.RetrieveUpdateDestroyAPIView):

    queryset = FailureDistributionType.objects.all()
    serializer_class = FailureDistributionTypeSerializer
    permission_classes = (permissions.IsAuthenticated, CanEditEnnumerations, HasCostModelLicense)


class LaborRateList(DepthSerializerMixin, generics.ListCreateAPIView):

    queryset = LaborRate.objects.all()
    serializer_class = LaborRateSerializer
    permission_classes = (permissions.IsAuthenticated, BelongsToOrganization, UserPermissions, HasCostModelLicense)
    filter_class = LaborRateFilter

    def get_queryset(self):
        # org_pk = self.kwargs['org_pk']
        # return self.queryset.filter(organization=org_pk)
        """
        Restricts the returned Labor Rates to a given organization.
        Optionally add a 'default' query parameter in the URL to include defaults.
        """
        queryset = LaborRate.objects.all()
        global_org = core_models.Organization.objects.filter(name="Default")
        organization = self.kwargs['org_pk']
        default = self.request.query_params.get('default', None)
        if default is not None:
            if default == "true":
                queryset = queryset.filter(Q(organization=organization) | Q(organization=global_org))
            elif default == "false":
                queryset = queryset.filter(organization=organization)
            else:
                queryset = queryset.filter(Q(organization=organization) | Q(organization=global_org))
        else:
            queryset = queryset.filter(Q(organization=organization) | Q(organization=global_org))
        return queryset


class LaborRateDetail(DepthSerializerMixin, generics.RetrieveUpdateDestroyAPIView):

    queryset = LaborRate.objects.all()
    serializer_class = LaborRateSerializer
    permission_classes = (permissions.IsAuthenticated, BelongsToOrganization, UserPermissions, HasCostModelLicense)

    def get_queryset(self):
        org_pk = self.kwargs['org_pk']
        return self.queryset.filter(organization=org_pk)


class AssetTypeList(generics.ListCreateAPIView):

    queryset = AssetType.objects.all()
    serializer_class = AssetTypeSerializer
    permission_classes = (permissions.IsAuthenticated, CanEditEnnumerations, HasCostModelLicense)
    filter_class = AssetTypeFilter


class AssetTypeDetail(generics.RetrieveUpdateDestroyAPIView):

    queryset = AssetType.objects.all()
    serializer_class = AssetTypeSerializer
    permission_classes = (permissions.IsAuthenticated, CanEditEnnumerations, HasCostModelLicense)


class OMTypeList(generics.ListCreateAPIView):

    queryset = OMType.objects.all()
    serializer_class = OMTypeSerializer
    permission_classes = (permissions.IsAuthenticated, CanEditEnnumerations, HasCostModelLicense)
    filter_class = OMTypeFilter


class OMTypeDetail(generics.RetrieveUpdateDestroyAPIView):

    queryset = OMType.objects.all()
    serializer_class = OMTypeSerializer
    permission_classes = (permissions.IsAuthenticated, CanEditEnnumerations, HasCostModelLicense)


class ServiceTypeList(generics.ListCreateAPIView):

    queryset = ServiceType.objects.all()
    serializer_class = ServiceTypeSerializer
    permission_classes = (permissions.IsAuthenticated, CanEditEnnumerations, HasCostModelLicense)
    filter_class = ServiceTypeFilter


class ServiceTypeDetail(generics.RetrieveUpdateDestroyAPIView):

    queryset = ServiceType.objects.all()
    serializer_class = ServiceTypeSerializer
    permission_classes = (permissions.IsAuthenticated, CanEditEnnumerations, HasCostModelLicense)


class ServiceProviderList(DepthSerializerMixin, generics.ListCreateAPIView):

    queryset = ServiceProvider.objects.all()
    serializer_class = ServiceProviderSerializer
    permission_classes = (permissions.IsAuthenticated, CanEditEnnumerations, HasCostModelLicense)
    filter_class = ServiceProviderFilter


class ServiceProviderDetail(generics.RetrieveUpdateDestroyAPIView):

    queryset = ServiceProvider.objects.all()
    serializer_class = ServiceProviderSerializer
    permission_classes = (permissions.IsAuthenticated, CanEditEnnumerations, HasCostModelLicense)


class DefinitionTypeList(generics.ListCreateAPIView):

    queryset = DefinitionType.objects.all()
    serializer_class = DefinitionTypeSerializer
    permission_classes = (permissions.IsAuthenticated, CanEditEnnumerations, HasCostModelLicense)
    filter_class = DefinitionTypeFilter


class DefinitionTypeDetail(generics.RetrieveUpdateDestroyAPIView):

    queryset = DefinitionType.objects.all()
    serializer_class = DefinitionTypeSerializer
    permission_classes = (permissions.IsAuthenticated, CanEditEnnumerations, HasCostModelLicense)


class WarrantyTypeList(generics.ListCreateAPIView):

    queryset = WarrantyType.objects.all()
    serializer_class = WarrantyTypeSerializer
    permission_classes = (permissions.IsAuthenticated, CanEditEnnumerations, HasCostModelLicense)
    filter_class = WarrantyTypeFilter


class WarrantyTypeDetail(generics.RetrieveUpdateDestroyAPIView):

    queryset = WarrantyType.objects.all()
    serializer_class = WarrantyTypeSerializer
    permission_classes = (permissions.IsAuthenticated, CanEditEnnumerations, HasCostModelLicense)


class CountryList(generics.ListCreateAPIView):

    queryset = Country.objects.all()
    serializer_class = CountrySerializer
    permission_classes = (permissions.IsAuthenticated, CanEditEnnumerations,)
    filter_class = CountryFilter


class CountryDetail(generics.RetrieveUpdateDestroyAPIView):

    queryset = Country.objects.all()
    serializer_class = CountrySerializer
    permission_classes = (permissions.IsAuthenticated, CanEditEnnumerations,)


class StateList(generics.ListCreateAPIView):

    queryset = State.objects.all()
    serializer_class = StateSerializer
    permission_classes = (permissions.IsAuthenticated, CanEditEnnumerations,)
    filter_class = StateFilter

    # def get_queryset(self):
    #     c_pk = self.kwargs['c_pk']
    #     return self.queryset.filter(country=c_pk)


class StateDetail(generics.RetrieveUpdateDestroyAPIView):

    queryset = State.objects.all()
    serializer_class = StateSerializer
    permission_classes = (permissions.IsAuthenticated, CanEditEnnumerations,)
    filter_class = StateFilter

    # def get_queryset(self):
    #     c_pk = self.kwargs['c_pk']
    #     return self.queryset.filter(country=c_pk)


class CityList(generics.ListCreateAPIView):

    queryset = City.objects.all()
    serializer_class = CitySerializer
    permission_classes = (permissions.IsAuthenticated, CanEditEnnumerations,)
    filter_class = CityFilter

    # def get_queryset(self):
    #     s_pk = self.kwargs['s_pk']
    #     return self.queryset.filter(state=s_pk)


class CityDetail(generics.RetrieveUpdateDestroyAPIView):

    queryset = City.objects.all()
    serializer_class = CitySerializer
    permission_classes = (permissions.IsAuthenticated, CanEditEnnumerations,)

    # def get_queryset(self):
    #     s_pk = self.kwargs['s_pk']
    #     return self.queryset.filter(state=s_pk)


def plant_group_count(request, version):
    org_id = request.GET.get('org_id', '')
    cursor = connection.cursor()
    cursor.execute("SELECT COUNT(*) FROM costmodel_osparc.costmodel_plantgroup WHERE organization_id = %s", [org_id])
    data = cursor.fetchall()
    return HttpResponse(json.dumps(data), content_type='application/json;charset=utf8')


def plant_count(request, version):
    org_id = request.GET.get('org_id', '')
    cursor = connection.cursor()
    # commenting global organization query and hardcoding it to 1.
    # cursor.execute("SELECT id FROM costmodel_osparc.core_organization WHERE name = 'Global'")
    # global_org_id = cursor.fetchall()
    global_org_id = u'1'
    cursor.execute("SELECT COUNT(*) FROM costmodel_osparc.core_plant WHERE organization_id = %s OR organization_id = %s", [org_id, global_org_id])
    data = cursor.fetchall()
    return HttpResponse(json.dumps(data), content_type='application/json;charset=utf8')


def costmodel_count(request, version):
    org_id = request.GET.get('org_id', '')
    cursor = connection.cursor()
    # commenting global organization query and hardcoding it to 1.
    # cursor.execute("SELECT id FROM costmodel_osparc.core_organization WHERE name = 'Global'")
    global_org_id = u'1'
    cursor.execute("SELECT id FROM costmodel_osparc.core_organization WHERE name = 'Global'")
    # global_org_id = cursor.fetchall()
    cursor.execute("SELECT count(*) FROM costmodel_osparc.costmodel_costmodel WHERE organization_id = %s or organization_id = %s", [org_id, global_org_id])
    data = cursor.fetchall()
    return HttpResponse(json.dumps(data), content_type='application/json;charset=utf8')


def service_count(request, version):
    org_id = request.GET.get('org_id', '')
    # commenting global organization query and hardcoding it to 1.
    # cursor.execute("SELECT id FROM costmodel_osparc.core_organization WHERE name = 'Global'")
    global_org_id = u'1'
    cursor = connection.cursor()
    # cursor.execute("SELECT id FROM costmodel_osparc.core_organization WHERE name = 'Global'")
    # global_org_id = cursor.fetchall()
    cursor.execute("SELECT count(*) FROM costmodel_osparc.costmodel_servicevalues WHERE organization_id = %s or organization_id = %s", [org_id, global_org_id])
    data = cursor.fetchall()
    return HttpResponse(json.dumps(data), content_type='application/json;charset=utf8')


class PlantReportsList(DepthSerializerMixin, generics.ListCreateAPIView):

    queryset = ReportQuery.objects.all()
    serializer_class = PlantReportsSerializer
    permission_classes = (permissions.IsAuthenticated, BelongsToOrganization, UserPermissions)
    filter_class = PlantReportsFilter

    def get_queryset(self):
        org_pk = self.kwargs['org_pk']
        return self.queryset.filter(organization=org_pk)


class PlantReportsDetail(DepthSerializerMixin, generics.RetrieveUpdateDestroyAPIView):

    queryset = ReportQuery.objects.all()
    serializer_class = PlantReportsSerializer
    permission_classes = (permissions.IsAuthenticated, BelongsToOrganization, UserPermissions)

    def get_queryset(self):
        org_pk = self.kwargs['org_pk']
        return self.queryset.filter(organization=org_pk)


@api_view(['GET'])
@permission_classes((permissions.IsAuthenticated, HasCostModelLicense))
def results(request, version):
    # Get Plant ID and Cost Model ID from URL
    costmodel_id = request.GET.get('costmodel', '')
    plant_id = request.GET.get('plant', '')

    org_id = request.GET.get('organization', '')
    if org_id:
        p = core_models.Plant.objects.get(id=plant_id)
        cm = CostModel.objects.get(id=costmodel_id)

        try:
            rq = ReportQuery.objects.create(
                    report_name= p.name + " - " + cm.name,
                    report_type="Plant Report",
                    organization=core_models.Organization.objects.get(id=org_id),
                    result="",
                    costmodel=costmodel_id,
                    plant=plant_id,
            )

            task_id = create_plant_report.delay(costmodel_id, plant_id, rq.id)
            rq.task_id = task_id
            rq.save()

            return Response({'status': 'success'})
        except:
            return Response({'status': 'error'})
    else:
        total_labor_hours_per_year = []
        total_labor_cost = []
        total_matl_cost = []
        avg_annual_expense = []
        npv_life = []

        percentage_of_total_npv = []
        service_list = []
        service_instance_list = []
        service_id_list = []

        try:
            # Fetch the Plant and Cost Model
            costmodel = CostModel.objects.get(id=costmodel_id)
            plant = core_models.Plant.objects.get(id=plant_id)
            plant_name = plant.name
            costmodel_name = costmodel.name

            if plant.plant_finance is None:
                return Response({'status': 'error', 'details': 'Plant Finance Object does not exist'}, status=400)

            if plant.plant_equipment is None:
                return Response({'status': 'error', 'details': 'Plant Equipment Object does not exist'}, status=400)

            for service_values in costmodel.service_values.all():
                service_id_list.append(service_values.id)
                service_list.append(service_values.service.name)
                service_instance_list.append(service_values.name)

                try:
                    number_of_units = eval(service_values.number_of_units)
                except (ZeroDivisionError, TypeError, AttributeError):
                    number_of_units = 0

                try:
                    labor_hours_per_unit = eval(service_values.labor_hours_per_unit)
                except (ZeroDivisionError, TypeError, AttributeError):
                    labor_hours_per_unit = 0

                try:
                    material_cost_per_unit = eval(service_values.material_cost_per_unit)
                except (ZeroDivisionError, TypeError, AttributeError):
                    material_cost_per_unit = 0

                # Calculate Total Labor Cost
                total_labor_cost.append(float(number_of_units) * float(labor_hours_per_unit) * float(service_values.labor_rate.rate_per_hour) * float(service_values.labor_rate.overhead_multiplier))

                # Calculate Total Material and Other Cost
                total_matl_cost.append(float(number_of_units) * float(material_cost_per_unit))

                # Calculate Total Labor Hours per Year
                total_labor_hours_per_year.append(float(number_of_units) * float(labor_hours_per_unit) / float(service_values.mean_interval))

            # Calculate Total Cost for Measure
            total_cost_for_measure = [x + y for x, y in zip(total_labor_cost, total_matl_cost)]
            d_total_cost_for_measure = ["%.2f" % v for v in total_cost_for_measure]

            total_matl_cost = [decimal.Decimal(x) for x in total_matl_cost]
            total_labor_cost = [decimal.Decimal(x) for x in total_labor_cost]
            d_total_labor_cost = ["%.2f" % v for v in total_labor_cost]
            d_total_labor_hours_per_year = ["%.2f" % v for v in total_labor_hours_per_year]

            # Calculate Annual Cash Flow
            columns = plant.plant_finance.analysis_period
            rows = costmodel.service_values.count()
            annual_cash_flow = [[0 for x in range(columns)] for y in range(rows)]
            d_annual_cash_flow = [[0 for x in range(columns)] for y in range(rows)]
            # npv_life = np.zeros(rows)

            warranty_years = 0

            # for i in range(rows):
            # for i in range(len(annual_cash_flow)) and service_values in costmodel.service_values.all():
            for i, k, l, m, service_values in map(None, range(len(annual_cash_flow)), total_cost_for_measure, total_labor_cost, total_matl_cost, costmodel.service_values.all()):
                # for j in range(columns):
                for j in range(len(annual_cash_flow[i])):

                    # Determine the number of years covered by warranty
                    if service_values.warranty_type.name == "Module (Product)":
                        warranty_years = plant.plant_finance.module_warranty
                    elif service_values.warranty_type.name == "Inverter":
                        warranty_years = plant.plant_finance.inverter_warranty
                    elif service_values.warranty_type.name == "EPC":
                        warranty_years = plant.plant_finance.epc_warranty
                    elif service_values.warranty_type.name == "Monitoring":
                        warranty_years = plant.plant_finance.monitoring_warranty

                    if j+1 > warranty_years:
                        if service_values.failure_distribution_type.id == 1:
                            if service_values.mean_interval < 1.00:
                                # A
                                annual_cash_flow[i][j] = float((k/float(service_values.mean_interval)) * float(pow((1+(plant.plant_finance.inflation_rate/100)), j+1)))

                            else:
                                # B
                                annual_cash_flow[i][j] = (int((j+1)/service_values.mean_interval) - int((j+1-1)/service_values.mean_interval)) * float(k) * float(pow((1+(plant.plant_finance.inflation_rate/100)), (j+1)))

                        else:
                            # WEIBULL.DIST(j, service_values.shape_factor, service_values.mean_interval, False)
                            x = j+1
                            a = service_values.shape_factor
                            b = service_values.mean_interval
                            weibull = float(a/pow(b, a)) * float(pow(x, a-1)) * float(math.exp(-(pow((x/b), a))))
                            # C
                            annual_cash_flow[i][j] = float(weibull * float(k) * float(pow((1+(plant.plant_finance.inflation_rate/100)), j+1)))

                    else:
                        if service_values.failure_distribution_type.id == 1:
                            if service_values.mean_interval < 1.00:
                                # D
                                annual_cash_flow[i][j] = float((float(m * (1-service_values.material_warranty_covered) + l * (1-service_values.labor_warranty_covered)) / service_values.mean_interval) * (pow((1+(plant.plant_finance.inflation_rate/100)), j+1)))
                            else:
                                # E
                                annual_cash_flow[i][j] = (int((j+1)/service_values.mean_interval) - int((j+1-1) / service_values.mean_interval)) * float(m * (1-service_values.material_warranty_covered) + l * (1-service_values.labor_warranty_covered)) * float(pow((1+(plant.plant_finance.inflation_rate/100)), j+1))

                        else:
                            # WEIBULL.DIST(j, service_values.shape_factor, service_values.mean_interval, False)
                            x = j+1
                            a = service_values.shape_factor
                            b = service_values.mean_interval
                            weibull = float(a / pow(b, a)) * float(pow(x, a - 1)) * float(math.exp(-(pow((x / b), a))))
                            # F
                            annual_cash_flow[i][j] = float(weibull * float(m * (1-service_values.material_warranty_covered) + l * (1-service_values.labor_warranty_covered)) * float((pow((1+(plant.plant_finance.inflation_rate/100)), j+1))))

            npv = 0.00
            for i in range(len(annual_cash_flow)):
                # Calculate Average Annual Expense
                avg_annual_expense.append(sum(annual_cash_flow[i]) / len(annual_cash_flow[i]))
                d_avg_annual_expense = ["%.2f" % v for v in avg_annual_expense]

                values = [float(j) for j in (annual_cash_flow[i])]
                # Calculate NPV Project Life
                for key, value in enumerate(values):
                    npv += (value / (pow((1 + float(plant.plant_finance.discount_rate/100)), key + 1)))
                npv_life.append(npv)
                npv = 0.00
                # npv_life.append(np.npv(float(plant.plant_finance.discount_rate/100), [float(j) for j in (annual_cash_flow[i])]))
                total_npv_life = sum(npv_life)
                d_total_npv_life = "%.2f" % total_npv_life
                d_npv_life = ["%.2f" % v for v in npv_life]

            sum_avg_annual_expense = sum(float(v) for v in avg_annual_expense)
            d_sum_avg_annual_expense = ["%.2f" % sum_avg_annual_expense]

            for i in range(len(npv_life)):
                # Calculate Percentage of Total NPV
                percentage_of_total_npv.append((npv_life[i]/reduce(lambda x, y: x + y, npv_life))*100)
                twodecimals_percentage_of_total_npv = ["%.2f" % v for v in percentage_of_total_npv]

            for i in range(len(annual_cash_flow)):
                d_annual_cash_flow[i] = ["%.2f" % v for v in annual_cash_flow[i]]
                # Calculate Sum of Annual Cash Flow
                sum_acf = [sum(x) for x in zip(*[[float(y) for y in x] for x in annual_cash_flow])]  # (*annual_cash_flow)]
                d_sum_acf = ["%.2f" % v for v in sum_acf]

                sum_annual_cash_flow = []
            for i, j in map(None, range(columns), d_sum_acf):
                sum_annual_cash_flow.append({'year': i+1, 'value': j})

            # Calculate NPV by O&M Type
            cost_per_year_by_om_type = Counter()
            for service_values, j in map(None, costmodel.service_values.all(), avg_annual_expense):
                cost_per_year_by_om_type[service_values.service.om_type.name] += float(j)

            npv_by_om_type = Counter()
            for service_values, j in map(None, costmodel.service_values.all(), npv_life):
                npv_by_om_type[service_values.service.om_type.name] += float(j)

            npv_by_activity = []

            for i, j in map(None, cost_per_year_by_om_type.items(), npv_by_om_type.items()):
                npv_by_activity.append(
                     {'label': i[0], 'cost_per_year': "%.2f" %(i[1]), 'npv': "%.2f" % (j[1]),
                      'npv_per_wp': "%.4f" % (j[1]/(plant.dc_rating*1000)),
                      'value': "%.2f" % ((j[1]/total_npv_life)*100)})

            # Calculate NPV by Asset Type
            cost_per_year_by_om_category = Counter()
            for service_values, j in map(None, costmodel.service_values.all(), avg_annual_expense):
                cost_per_year_by_om_category[service_values.service.asset_type.name] += float(j)

            npv_by_om_category = Counter()
            for service_values, j in map(None, costmodel.service_values.all(), npv_life):
                npv_by_om_category[service_values.service.asset_type.name] += float(j)

            npv_life_by_om_category = []

            for i, j in map(None, cost_per_year_by_om_category.items(), npv_by_om_category.items()):
                npv_life_by_om_category.append(
                     {'asset_type': i[0], 'cost_per_year': "%.2f" % (i[1]), 'npv': "%.2f" % (j[1]),
                      'percentage_of_npv': "%.2f" % ((j[1] / total_npv_life)*100)})

            # Calculate NPV by Service Provider
            cost_per_year_by_sp = Counter()
            for service_values, j in map(None, costmodel.service_values.all(), avg_annual_expense):
                cost_per_year_by_sp[service_values.service_provider.name] += float(j)

            npv_by_sp = Counter()
            for service_values, j in map(None, costmodel.service_values.all(), npv_life):
                npv_by_sp[service_values.service_provider.name] += float(j)

            npv_life_by_sp = []

            for i, j in map(None, cost_per_year_by_sp.items(), npv_by_sp.items()):
                npv_life_by_sp.append(
                     {'service_provider': i[0], 'cost_per_year': "%.2f" % (i[1]), 'npv': "%.2f" % (j[1]),
                      'percentage_of_npv': "%.2f" % ((j[1] / total_npv_life)*100)})

            # Calculate Reserve Account
            columns = plant.plant_finance.analysis_period
            rows = costmodel.service_values.count()
            reserve_account = [[0 for x in range(columns)] for y in range(rows)]
            d_reserve_account = [[0 for x in range(columns)] for y in range(rows)]

            warranty_years = 0

            for i, k, l, m, service_values in map(None, range(len(reserve_account)), total_cost_for_measure, total_labor_cost, total_matl_cost, costmodel.service_values.all()):
                for j in range(len(reserve_account[i])):

                    # Calculate No of Units
                    try:
                        number_of_units = eval(service_values.number_of_units)
                    except (ZeroDivisionError, TypeError):
                        number_of_units = 0

                    # Determine the number of years covered by warranty
                    if service_values.warranty_type.name == "Module (Product)":
                        warranty_years = plant.plant_finance.module_warranty
                    elif service_values.warranty_type.name == "Inverter":
                        warranty_years = plant.plant_finance.inverter_warranty
                    elif service_values.warranty_type.name == "EPC":
                        warranty_years = plant.plant_finance.epc_warranty
                    elif service_values.warranty_type.name == "Monitoring":
                        warranty_years = plant.plant_finance.monitoring_warranty

                    if j + 1 > warranty_years:
                        if service_values.failure_distribution_type.id == 1:
                            if service_values.mean_interval < 1.00:
                                # A
                                reserve_account[i][j] = max(annual_cash_flow[i][j], (float((k / float(service_values.mean_interval)) * float(pow((1 + (plant.plant_finance.inflation_rate / 100)), j + 1)))))

                            else:
                                # B
                                reserve_account[i][j] = max(annual_cash_flow[i][j], (int((j+1) / service_values.mean_interval) - int((j+1-1) / service_values.mean_interval) * float(k) * float(pow((1 + (plant.plant_finance.inflation_rate / 100)), j + 1))))

                        else:
                            # WEIBULL.DIST(j, service_values.shape_factor, service_values.mean_interval, False)
                            x = j + 1
                            a = service_values.shape_factor
                            b = service_values.mean_interval
                            weibull = float(a / pow(b, a)) * float(pow(x, a - 1)) * float(math.exp(-(pow((x / b), a))))

                            # n_by_N
                            n_by_N = calculate_n_by_N(number_of_units, weibull, plant.plant_finance.desired_confidence_that_reserve_covers_cost)

                            # C
                            # Old Reserve Account formula
                            # reserve_account[i][j] = max(annual_cash_flow[i][j], (pow((float(plant.desired_confidence_that_reserve_covers_cost)), ((1-weibull)/weibull)) * float(k) * float(pow((1 + (plant.plant_finance.inflation_rate / 100)), j + 1))))

                            # New Reserve Account formula - Uses n_by_N_calculation
                            reserve_account[i][j] = max(annual_cash_flow[i][j], (n_by_N * float(k) * float(pow((1 + (plant.plant_finance.inflation_rate / 100)), j + 1))))

                            # print("service_values = " + str(service_values.service.name) + " Year = " + str(j + 1) + " Reserve Account = " + str(reserve_account[i][j]) + " ACF = " + str(annual_cash_flow[i][j]))

                    else:
                        if service_values.failure_distribution_type.id == 1:
                            if service_values.mean_interval < 1.00:
                                # D
                                reserve_account[i][j] = max(annual_cash_flow[i][j], (float((float(m * (1 - service_values.material_warranty_covered) + l * (1 - service_values.labor_warranty_covered)) / service_values.mean_interval) * (pow((1 + (plant.plant_finance.inflation_rate / 100)), j + 1)))))
                            else:
                                # E
                                reserve_account[i][j] = max(annual_cash_flow[i][j], (float(float(int((j + 1) / service_values.mean_interval) - int((j + 1 - 1) / service_values.mean_interval)) * float(m * (1 - service_values.material_warranty_covered) + l * (1 - service_values.labor_warranty_covered)) * (pow((1 + (plant.plant_finance.inflation_rate / 100)), j + 1)))))

                        else:
                            # WEIBULL.DIST(j, service_values.shape_factor, service_values.mean_interval, False)
                            x = j + 1
                            a = service_values.shape_factor
                            b = service_values.mean_interval
                            weibull = float(a / pow(b, a)) * float(pow(x, a - 1)) * float(math.exp(-(pow((x / b), a))))

                            # n_by_N
                            n_by_N = calculate_n_by_N(number_of_units, weibull, plant.plant_finance.desired_confidence_that_reserve_covers_cost)

                            # F
                            # Old Reserve Account formula
                            # reserve_account[i][j] = max(annual_cash_flow[i][j], (pow(float(plant.desired_confidence_that_reserve_covers_cost), ((1-weibull)/weibull)) * float(m * (1 - service_values.material_warranty_covered) + l * (1 - service_values.labor_warranty_covered)) * float((pow((1 + (plant.plant_finance.inflation_rate / 100)), j + 1)))))

                            # New Reserve Account formula
                            reserve_account[i][j] = max(annual_cash_flow[i][j], (n_by_N * float(m * (1 - service_values.material_warranty_covered) + l * (1 - service_values.labor_warranty_covered)) * float((pow((1 + (plant.plant_finance.inflation_rate / 100)), j + 1)))))

            for i in range(len(reserve_account)):
                d_reserve_account[i] = ["%.2f" % v for v in reserve_account[i]]
                # Calculate Sum of Reserve Account
                sum_ra = [sum(x) for x in zip(*[[float(y) for y in x] for x in reserve_account])]
                d_sum_ra = ["%.2f" % v for v in sum_ra]

                sum_reserve_account = []
            for i, j in map(None, range(columns), d_sum_ra):
                sum_reserve_account.append({'year': str(i + 1), 'value': j})

            annual_production = []
            for j in range(columns):
                annual_production.append({'year': j + 1,
                                          'annual_production_cash_flow': "%.2f" %(plant.dc_rating * plant.energy_yield*(pow((1-plant.plant_equipment.whole_system_degradation_rate), j+1))+0.0000000000001),
                                          'annual_production_reserve_account': "%.2f" %(plant.dc_rating * plant.energy_yield * (pow((1 - plant.plant_equipment.whole_system_degradation_rate), j + 1)) + 0.0000000000001)
                                          })

            annual_average = []
            for i, j in map(None, range(columns), sum_ra):
                annual_average.append({'year': str(i + 1),
                                       'annual_average_cash_flow': "%.5f" %(j/(plant.dc_rating*plant.energy_yield*(pow((1-plant.plant_equipment.whole_system_degradation_rate), i+1))+0.0000000000001)),
                                       'annual_average_reserve_account': "%.5f" %(j/(plant.dc_rating*plant.energy_yield*(pow((1-plant.plant_equipment.whole_system_degradation_rate), i+1))+0.0000000000001))
                                       })

            ir = plant.plant_finance.inflation_rate / 100
            dr = plant.plant_finance.discount_rate / 100

            annualized_om_costs = total_npv_life / float(
                (1 + ir) / (dr - ir) * (1 - (pow(((1 + ir) / (1 + dr)), plant.plant_finance.analysis_period))))

            maximum_reserve_account = max([x['value'] for x in sum_reserve_account])

            # Calculate npv_annual_om_costs_per_kwh
            npv_annual_om_costs = 0.00
            annual_production_cash_flow = []
            for d in annual_production:
                annual_production_cash_flow.append(d['annual_production_cash_flow'])
            values = [float(j) for j in annual_production_cash_flow]

            for key, value in enumerate(values):
                npv_annual_om_costs += (value / (pow((1 + float(plant.plant_finance.discount_rate / 100)), key + 1)))

            final_results = [{'annualized_om_costs': "%.2f" % annualized_om_costs,
                              'annualized_unit_om_costs': "%.2f" % (annualized_om_costs / plant.dc_rating),
                              'maximum_reserve_account': maximum_reserve_account,
                              'npv_om_costs': d_total_npv_life,
                              'npv_per_wp': "%.4f" % (total_npv_life / (plant.dc_rating*1000)),
                              'npv_annual_om_costs_per_kwh': "%.5f" % (float(d_total_npv_life)/npv_annual_om_costs)}]

            # Calculate Failure Mode Analysis Data
            columns = plant.plant_finance.analysis_period  # service_values ID, Failure Mode, Cost of Failure, Expected Cost of Failure, Flag (O&M Cost < Expected Energy Cost)
            rows = costmodel.service_values.count()
            failure_mode_analysis = [[0 for x in range(columns)] for y in range(rows)]

            # for i, k, service_values in map(None, range(len(failure_mode_analysis)), total_cost_for_measure,
            #                          costmodel.service_values.all()):
            #     for j in range(len(failure_mode_analysis[i])):
            #         if service_values.failure_mode_values is not None:
            #             for failure in service_values.failure_mode_values.all():
            #                 # failure_mode_analysis[i][1] = j.name
            #                 failure_mode_analysis[i][j] = failure.outage_time * 0.05 * float(plant.inverter_capacity)
            #                 if j.failure_distribution.id == 1:
            #                     failure_mode_analysis[i][3] = failure_mode_analysis[i][2] * j.failure_parameter_1
            #                 elif j.failure_distribution.id == 2:
            #                     # WEIBULL.DIST(1, a, b, True)
            #                     x = 1
            #                     a = j.failure_parameter_1
            #                     b = j.failure_parameter_2
            #                     weibull = 1 - (math.exp(-(pow((x / b), a))))
            #                     failure_mode_analysis[i][3] = weibull * failure_mode_analysis[i][2]
            #
            #                 if k < failure_mode_analysis[i][2]:
            #                     failure_mode_analysis[i][4] = 1

            dict_failure_mode_analysis = []

            for k, service_values, acf in map(None, total_cost_for_measure, costmodel.service_values.all(), annual_cash_flow):

                if service_values.failure_mode_values is not None:
                    for j, lcoe in map(None, service_values.failure_mode_values.all(), annual_average):
                        if j is not None:
                            cost_of_failure = j.outage_time * float(lcoe['annual_average_cash_flow']) * float(plant.plant_equipment.inverter_capacity)

                            if j.failure_distribution.id == 1:
                                expected_cost_of_failure = []
                                for year in range(plant.plant_finance.analysis_period):
                                    expected_cost_of_failure.append(cost_of_failure * j.failure_parameter_1)
                            elif j.failure_distribution.id == 2:
                                expected_cost_of_failure = []
                                for year in range(plant.plant_finance.analysis_period):
                                    x = year
                                    a = j.failure_parameter_1
                                    b = j.failure_parameter_2
                                    weibull = 1 - (math.exp(-(pow((x / b), a))))
                                    expected_cost_of_failure.append(weibull * cost_of_failure)
                            # print("expected_cost_of_failure"+str(expected_cost_of_failure))
                            # print("acf"+str(acf))
                            flag = []
                            for a, b in map(None, acf, expected_cost_of_failure):
                                if a < b:
                                    flag.append(1)
                                else:
                                    flag.append(0)
                            dict_failure_mode_analysis.append({'service_type_name': service_values.service.name,
                                                               'service_name': service_values.name,
                                                               # 'Failure Mode': json.dumps(j.__dict__),
                                                               'failure_mode': j.failure_mode.name + ' ' + j.failure_mode.description,
                                                               'cost_of_failure': "%.2f" %(cost_of_failure),
                                                               'expected_cost_of_failure': ["%.2f" % v for v in expected_cost_of_failure],
                                                               'annual_cash_flow': ["%.2f" % v for v in acf],
                                                               'flag': flag})

            data = {'plant_name': plant_name, 'costmodel_name': costmodel_name, 'service_id_list': service_id_list,
                    'service_list': service_list, 'service_instance_list': service_instance_list,
                    'total_labor_hours_per_year': d_total_labor_hours_per_year, 'total_labor_cost': d_total_labor_cost,
                    'total_material_and_other_cost': total_matl_cost, 'total_cost_for_measure': d_total_cost_for_measure,
                    'average_annual_expense': d_avg_annual_expense, 'sum_average_annual_expense': d_sum_avg_annual_expense,
                    'npv_project_life': d_npv_life, 'sum_npv_project_life': d_total_npv_life,
                    'percentage_of_total_npv': twodecimals_percentage_of_total_npv,
                    'annual_cash_flow': d_annual_cash_flow, 'sum_annual_cash_flow': sum_annual_cash_flow,
                    'npv_by_activity': npv_by_activity, 'npv_by_om_category': npv_life_by_om_category,
                    'npv_by_service_provider': npv_life_by_sp, 'final_results': final_results,
                    'reserve_account': d_reserve_account, 'sum_reserve_account': sum_reserve_account,
                    'annual_production': annual_production, 'annual_average': annual_average,
                    'dict_failure_mode_analysis': dict_failure_mode_analysis
                    }

            # return HttpResponse(json.dumps(data, cls=DjangoJSONEncoder), content_type='application/json;charset=utf8')
            return Response(data)

        except ObjectDoesNotExist:
            return Response({'status': 'error', 'details': 'Cost Model or Plant does not exist'}, status=400)

        except ValueError:
            return Response({'status': 'error', 'details': 'Plant ID and Cost Model ID parameters are required'}, status=400)



def calculate_n_by_N(N, Q, R):
    P = 1-Q
    # term = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10]

    rows = 11
    columns = 2
    n_by_N_r = [[0 for x in range(columns)] for y in range(rows)]

    n_by_N_r[1][0] = 0.1
    n_by_N_r[2][0] = 0.2
    n_by_N_r[3][0] = 0.3
    n_by_N_r[4][0] = 0.4
    n_by_N_r[5][0] = 0.5
    n_by_N_r[6][0] = 0.6
    n_by_N_r[7][0] = 0.7
    n_by_N_r[8][0] = 0.8
    n_by_N_r[9][0] = 0.9
    n_by_N_r[10][0] = 1.0

    n_by_N_r[0][1] = pow(P, N)
    n_by_N_r[1][1] = n_by_N_r[0][1] + N * pow(P, (N - 1)) * Q
    n_by_N_r[2][1] = n_by_N_r[1][1] + N * (N - 1) * pow(P, (N - 2)) * pow(Q, 2) / math.factorial(2)
    n_by_N_r[3][1] = n_by_N_r[2][1] + N * (N - 1) * (N - 2) * pow(P, (N - 3)) * pow(Q, 3) / math.factorial(3)
    n_by_N_r[4][1] = n_by_N_r[3][1] + N * (N - 1) * (N - 2) * (N - 3) * pow(P, (N - 4)) * pow(Q, 4) / math.factorial(4)
    n_by_N_r[5][1] = n_by_N_r[4][1] + N * (N - 1) * (N - 2) * (N - 3) * (N - 4) * pow(P, (N - 5)) * pow(Q, 5) / math.factorial(5)
    n_by_N_r[6][1] = n_by_N_r[5][1] + N * (N - 1) * (N - 2) * (N - 3) * (N - 4) * (N - 5) * pow(P, (N - 6)) * pow(Q, 6) / math.factorial(6)
    n_by_N_r[7][1] = n_by_N_r[6][1] + N * (N - 1) * (N - 2) * (N - 3) * (N - 4) * (N - 5) * (N - 6) * pow(P, (N - 7)) * pow(Q, 7) / math.factorial(7)
    n_by_N_r[8][1] = n_by_N_r[7][1] + N * (N - 1) * (N - 2) * (N - 3) * (N - 4) * (N - 5) * (N - 6) * (N - 7) * pow(P, (N - 8)) * pow(Q, 8) / math.factorial(8)
    n_by_N_r[9][1] = n_by_N_r[8][1] + N * (N - 1) * (N - 2) * (N - 3) * (N - 4) * (N - 5) * (N - 6) * (N - 7) * (N - 8) * pow(P, (N - 9)) * pow(Q, 9) / math.factorial(9)
    n_by_N_r[10][1] = n_by_N_r[9][1] + N * (N - 1) * (N - 2) * (N - 3) * (N - 4) * (N - 5) * (N - 6) * (N - 7) * (N - 8) * (N - 9) * pow(P, (N - 10)) * pow(Q, 10) / math.factorial(10)

    # Calculating n_by_N using closest value
    n_by_N_r_columns = zip(*n_by_N_r)
    closest_R_index = n_by_N_r_columns[1].index(min(n_by_N_r_columns[1], key=lambda x: abs(x - float(R))))
    # n_by_N = n_by_N_r[closest_R_index][0]

    # Calculating n_by_N using Interpolate
    n_by_N_list = n_by_N_r_columns[0]
    R_list = n_by_N_r_columns[1]
    n_by_N = np.interp(float(R), R_list, n_by_N_list)

    return n_by_N


def plants_in_group(plant_group_id):

    plant_list = []

    plant_group = PlantGroup.objects.get(id=plant_group_id)
    for plant in plant_group.plant.all():
        plant_list.append(plant)

    for group in plant_group.plant_group.all():
        more_plants = plants_in_group(group.id)
        for plant in more_plants:
            if plant not in plant_list:
                plant_list.append(plant)

    return plant_list


@api_view(['GET'])
@permission_classes((permissions.IsAuthenticated, HasCostModelLicense))
def plant_group_results(request, version):
    # Get Plant Group ID from URL
    plant_group_id = request.GET.get('plant_group', '')
    org_id = request.GET.get('organization', '')

    try:
        plant_group = PlantGroup.objects.get(id=plant_group_id)
        # get the plant group name based on plant_group id
        rq = ReportQuery.objects.create(
                report_name=plant_group.name,
                report_type="Plant Group Report",
                organization=core_models.Organization.objects.get(id=org_id),
                result="",
                plant_group=plant_group_id
        )

        task_id = create_plant_group_report.delay(plant_group_id, rq.id)
        rq.task_id = task_id
        rq.save()

        return Response({'status': 'success'})
    except:
        return Response({'status': 'error'})


def merge_records_by(key, combine):
    """Returns a function that merges two records rec_a and rec_b.
       The records are assumed to have the same value for rec_a[key]
       and rec_b[key].  For all other keys, the values are combined
       using the specified binary operator.
    """
    return lambda rec_a, rec_b: {
        k: rec_a[k] if k == key else combine(float(rec_a[k]), float(rec_b[k]))
        for k in rec_a
    }


def merge_list_of_records_by(key, combine):
    """Returns a function that merges a list of records, grouped by
       the specified key, with values combined using the specified
       binary operator."""
    keyprop = itemgetter(key)
    return lambda lst: [
        reduce(merge_records_by(key, combine), records)
        for _, records in groupby(sorted(lst, key=keyprop), keyprop)
    ]


@api_view(['GET'])
@permission_classes((permissions.IsAuthenticated, HasCostModelLicense))
def export(request, version):
    # Get Plant ID from URL
    plant_id = request.GET.get('plant', '')

    try:
        # Fetch the Plant and Cost Model
        plant = core_models.Plant.objects.get(id=plant_id)
        costmodel = plant.active_cost_model
        # Writing to a template xlsx file
        filename = 'Export Template.xlsx'
        wb = load_workbook(filename)

        inputs_ws = wb['Inputs']
        inputs_ws['D5'] = plant.uuid
        inputs_ws['D6'] = plant.name
        inputs_ws['D7'] = plant.description
        inputs_ws['D8'] = plant.activation_date

        inputs_ws['D10'] = plant.address_line_1
        inputs_ws['D11'] = plant.address_line_2
        inputs_ws['D12'] = plant.city.name
        inputs_ws['D13'] = plant.county
        inputs_ws['D14'] = plant.state.name
        inputs_ws['D15'] = plant.country
        inputs_ws['D16'] = plant.postal_code
        inputs_ws['D17'] = plant.latitude
        inputs_ws['D18'] = plant.longitude
        inputs_ws['D19'] = plant.time_zone

        inputs_ws['D21'] = plant.dc_rating
        inputs_ws['D22'] = plant.derate_factor
        inputs_ws['D23'] = plant.energy_yield
        inputs_ws['D24'] = plant.site_area

        inputs_ws['D26'] = plant.storage_original_capacity
        inputs_ws['D27'] = plant.storage_current_capacity
        inputs_ws['D28'] = plant.storage_state_of_charge

        inputs_ws['D30'] = plant.pv_market_sector.name
        inputs_ws['D31'] = plant.weather_source.name if plant.weather_source else ''
        inputs_ws['D32'] = plant.solar_anywhere_site

        inputs_ws['D36'] = plant.plant_finance.system_installed_cost
        inputs_ws['D37'] = plant.plant_finance.analysis_period
        inputs_ws['D38'] = plant.plant_finance.discount_rate
        inputs_ws['D39'] = plant.plant_finance.inflation_rate
        inputs_ws['D40'] = plant.plant_finance.desired_confidence_that_reserve_covers_cost
        inputs_ws['D41'] = plant.plant_finance.working_hours

        inputs_ws['D43'] = plant.plant_finance.inverter_warranty
        inputs_ws['D44'] = plant.plant_finance.module_warranty
        inputs_ws['D45'] = plant.plant_finance.epc_warranty
        inputs_ws['D46'] = plant.plant_finance.monitoring_warranty

        inputs_ws['D50'] = plant.plant_equipment.inverter_type.name
        inputs_ws['D51'] = plant.plant_equipment.inverter_replacement_cost
        inputs_ws['D52'] = plant.plant_equipment.inverter_capacity
        inputs_ws['D53'] = plant.plant_equipment.number_of_inverters
        inputs_ws['D54'] = plant.plant_equipment.number_of_transformers

        inputs_ws['D56'] = plant.plant_equipment.module_type.name
        inputs_ws['D57'] = plant.plant_equipment.module_type.degradation_rate
        inputs_ws['D58'] = plant.plant_equipment.module_efficiency
        inputs_ws['D59'] = plant.plant_equipment.module_power
        inputs_ws['D60'] = plant.plant_equipment.modules_per_string

        inputs_ws['D62'] = plant.plant_equipment.array_area
        inputs_ws['D63'] = plant.plant_equipment.number_of_modules
        inputs_ws['D64'] = plant.plant_equipment.number_of_strings
        inputs_ws['D65'] = plant.plant_equipment.strings_per_combiner_box
        inputs_ws['D66'] = plant.plant_equipment.number_of_combiner_boxes
        inputs_ws['D67'] = plant.plant_equipment.combiner_boxes_per_dcd
        inputs_ws['D68'] = plant.plant_equipment.number_of_dc_disconnects

        inputs_ws['D70'] = plant.plant_equipment.mounting_type.name
        inputs_ws['D71'] = plant.plant_equipment.mounting_location.name
        inputs_ws['D72'] = plant.plant_equipment.roof_slope_type.name
        inputs_ws['D73'] = plant.plant_equipment.roof_type.name
        inputs_ws['D74'] = plant.plant_equipment.inspection_technique.name
        inputs_ws['D75'] = plant.plant_equipment.array_area_per_roof_attachment
        inputs_ws['D76'] = plant.plant_equipment.number_of_roof_attachments
        inputs_ws['D77'] = plant.plant_equipment.gcr

        inputs_ws['D79'] = plant.plant_equipment.modules_per_row
        inputs_ws['D80'] = plant.plant_equipment.total_rows
        inputs_ws['D81'] = plant.plant_equipment.tracking.name
        inputs_ws['D82'] = plant.plant_equipment.rows_per_tracked_block
        inputs_ws['D83'] = plant.plant_equipment.total_tracking_blocks
        inputs_ws['D84'] = plant.plant_equipment.foundations_per_row

        inputs_ws['D86'] = plant.plant_equipment.tilt
        inputs_ws['D87'] = plant.plant_equipment.azimuth

        row = 92

        for condition in plant.environmental_condition.all():
            inputs_ws.cell(row=row, column=3).value = condition.name
            row += 1

        request.GET = request.GET.copy()
        request.GET['plant'] = plant.id
        request.GET['costmodel'] = plant.active_cost_model.id
        r = results(request, version)

        report_ws = wb['Report']
        report_ws['B1'] = plant.name
        report_ws['B2'] = costmodel.name
        report_ws['B4'] = float(r.data['final_results'][0]['annualized_om_costs'])
        report_ws['B5'] = float(r.data['final_results'][0]['annualized_unit_om_costs'])
        report_ws['B6'] = float(r.data['final_results'][0]['maximum_reserve_account'])
        report_ws['B7'] = float(r.data['final_results'][0]['npv_om_costs'])
        report_ws['B8'] = float(r.data['final_results'][0]['npv_per_wp'])
        report_ws['B9'] = float(r.data['final_results'][0]['npv_annual_om_costs_per_kwh'])

        row = 3
        for dict in r.data['npv_by_service_provider']:
            report_ws.cell(row=row, column=4).value = dict['service_provider']
            report_ws.cell(row=row, column=5).value = float(dict['cost_per_year'])
            report_ws.cell(row=row, column=6).value = float(dict['npv'])
            report_ws.cell(row=row, column=7).value = float(dict['percentage_of_npv'])
            row += 1

        row = 3
        for dict in r.data['npv_by_om_category']:
            report_ws.cell(row=row, column=9).value = dict['asset_type']
            report_ws.cell(row=row, column=10).value = float(dict['cost_per_year'])
            report_ws.cell(row=row, column=11).value = float(dict['npv'])
            report_ws.cell(row=row, column=12).value = float(dict['percentage_of_npv'])
            row += 1

        report_ws = wb['Detailed Measures']
        row = 3

        cm_service_values = costmodel.service_values.all()
        number_of_cm_service_values = len(cm_service_values)

        for service_values in cm_service_values:
            report_ws.cell(row=row, column=1).value = service_values.service.name
            report_ws.cell(row=row, column=2).value = service_values.service.description
            report_ws.cell(row=row, column=3).value = service_values.name
            report_ws.cell(row=row, column=4).value = service_values.description
            report_ws.cell(row=row, column=5).value = service_values.service.om_type.name
            report_ws.cell(row=row, column=6).value = service_values.service.asset_type.name
            report_ws.cell(row=row, column=7).value = service_values.material_warranty_covered
            report_ws.cell(row=row, column=8).value = service_values.labor_warranty_covered
            report_ws.cell(row=row, column=9).value = service_values.warranty_type.name
            if service_values.applicable_unit is not None:
                report_ws.cell(row=row, column=10).value = service_values.applicable_unit.name
            report_ws.cell(row=row, column=11).value = service_values.service_provider.name
            report_ws.cell(row=row, column=12).value = service_values.labor_rate.rate_per_hour
            report_ws.cell(row=row, column=13).value = service_values.mean_interval
            report_ws.cell(row=row, column=14).value = service_values.shape_factor
            report_ws.cell(row=row, column=15).value = service_values.failure_distribution_type.name
            report_ws.cell(row=row, column=16).value = service_values.number_of_units
            report_ws.cell(row=row, column=17).value = service_values.labor_hours_per_unit
            report_ws.cell(row=row, column=19).value = service_values.material_cost_per_unit
            report_ws.cell(row=row, column=23).value = service_values.notes
            report_ws.cell(row=row, column=24).value = service_values.reference
            row += 1

        row = 3
        for value in r.data['total_labor_hours_per_year']:
            report_ws.cell(row=row, column=18).value = float(value)
            row += 1

        row = 3
        for value in r.data['total_labor_cost']:
            report_ws.cell(row=row, column=20).value = float(value)
            row += 1

        row = 3
        for value in r.data['total_material_and_other_cost']:
            report_ws.cell(row=row, column=21).value = value
            row += 1

        row = 3
        for value in r.data['total_cost_for_measure']:
            report_ws.cell(row=row, column=22).value = float(value)
            row += 1

        row = 3
        for value in r.data['average_annual_expense']:
            report_ws.cell(row=row, column=25).value = float(value)
            row += 1

        row = 3
        for value in r.data['npv_project_life']:
            report_ws.cell(row=row, column=26).value = float(value)
            row += 1

        row = 3
        for value in r.data['percentage_of_total_npv']:
            report_ws.cell(row=row, column=27).value = float(value)
            row += 1

        row = 3
        for values in r.data['annual_cash_flow']:
            column1 = 28
            for value in values:
                report_ws.cell(row=row, column=column1).value = float(value)
                column1 += 1
            row += 1

        # format cells
        ws = wb.get_sheet_by_name('Detailed Measures')
        min_row_value = number_of_cm_service_values + 4
        max_row_value = number_of_cm_service_values + 6
        ws['A'+str(number_of_cm_service_values + 4)].value = 'ANNUAL TOTAL'
        ws['A'+str(number_of_cm_service_values + 5)].value = 'ANNUAL PRODUCTION'
        ws['A'+str(number_of_cm_service_values + 6)].value = 'Annual Average $/kWh/year'
        for row in ws.iter_rows(min_row=min_row_value, max_row=max_row_value, min_col=1):
            for cell in row:
                cell.fill = PatternFill(fill_type="solid", fgColor="000000", patternType='solid', bgColor="000000")
                cell.font = Font(b=True, color="fffefe")
                cell.number_format = 'General'
        for row in ws.iter_rows(min_row=max_row_value+1):
            for cell in row:
                cell.fill = PatternFill("solid", fgColor="fffefe")

        # Value for total NPV Project Life
        total_npv_project_life = r.data['sum_npv_project_life']
        report_ws.cell(row=number_of_cm_service_values + 4, column=26).value = float(total_npv_project_life)

        # Value for Total Avg Annual Exp
        total_avg_ann_exp = r.data['sum_average_annual_expense'][0]
        report_ws.cell(row=number_of_cm_service_values + 4, column=25).value = float(total_avg_ann_exp)

        # fill in values for annual cash flow totals
        len_annual_cash_flow = len(r.data['annual_cash_flow'][0])
        column_for_sum = 28
        sum_column = 0
        
        for i in range(len_annual_cash_flow):
            for value in r.data['annual_cash_flow']:
                sum_column += float(value[i])
            report_ws.cell(row=number_of_cm_service_values + 4, column=column_for_sum).value = float(sum_column)
            annual_production_cash_flow = float(r.data['annual_production'][i]['annual_production_cash_flow'])
            report_ws.cell(row=number_of_cm_service_values + 5, column=column_for_sum).value = annual_production_cash_flow
            report_ws.cell(row=number_of_cm_service_values + 6, column=column_for_sum).value = float(sum_column)/annual_production_cash_flow
            column_for_sum += 1
            sum_column = 0

        row = 3
        for values in r.data['reserve_account']:
            # column2 = column1 + 1
            column2 = 68
            for value in values:
                report_ws.cell(row=row, column=column2).value = float(value)
                column2 += 1
            row += 1

        # fill in header for Reserve Cash Account
        # report_ws.cell(row=1, column=28 + (len_annual_cash_flow + 1)).value = 'Reserve Account'

        # fill in values for reserve account totals
        len_reserve_account = len(r.data['reserve_account'][0])
        column = 68
        sum_reserve_account_column = 0
        
        for i in range(len_reserve_account):
            for value in r.data['reserve_account']:
                sum_reserve_account_column += float(value[i])
            report_ws.cell(row=number_of_cm_service_values + 4, column=column).value = float(sum_reserve_account_column)
            annual_production_reserve_account = float(r.data['annual_production'][i]['annual_production_reserve_account'])
            report_ws.cell(row=number_of_cm_service_values + 5, column=column).value = annual_production_reserve_account
            report_ws.cell(row=number_of_cm_service_values + 6, column=column).value = float(sum_reserve_account_column)/annual_production_reserve_account
            column += 1
            sum_reserve_account_column = 0



        response = HttpResponse(content_type='application/ms-excel')
        response['Content-Disposition'] = 'attachment; filename="PV O&M Cost Model-'+str(plant.name)+'.xlsx"'
        wb.save(response)

        return response

    except ObjectDoesNotExist:
        return Response({'status': 'error', 'details': 'Cost Model or Plant does not exist'}, status=400)


class DefaultServiceList(DepthSerializerMixin, generics.ListAPIView):

    queryset = ServiceValues.objects.all()
    serializer_class = ServiceValuesSerializer
    filter_class = ServiceValuesFilter

    def get_queryset(self):
        global_org = core_models.Organization.objects.filter(name='Default')
        deftype = DefinitionType.objects.filter(name='Best Practices')

        qs = super(DefaultServiceList, self).get_queryset()

        return qs.filter(Q(organization=global_org, defined_by=deftype))


class DefaultCostModelValuesList(DepthSerializerMixin, generics.ListAPIView):

    queryset = CostModel.objects.all()
    serializer_class = CostModelSerializer
    filter_class = CostModelFilter
    lookup_url_kwarg = "org_pk"

    def get_queryset(self):
        org_pk = self.kwargs.get(self.lookup_url_kwarg)
        #global_org = core_models.Organization.objects.filter(name='Default')
        organization = self.kwargs['org_pk']
        deftype = DefinitionType.objects.filter(name='Best Practices')

        qs = super(DefaultCostModelValuesList, self).get_queryset()
        response = qs.filter(Q(organization=organization) | Q(defined_by=deftype))

        return response

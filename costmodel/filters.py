import rest_framework_filters as filters
from models import *


class DefinitionTypeFilter(filters.FilterSet):

    class Meta:
        model = DefinitionType
        fields = {
            'name': '__all__',
            'description': '__all__',
        }


class OMTypeFilter(filters.FilterSet):

    class Meta:
        model = OMType
        fields = {
            'name': '__all__',
            'description': '__all__',
        }


class ServiceTypeFilter(filters.FilterSet):

    class Meta:
        model = ServiceType
        fields = {
            'name': '__all__',
            'description': '__all__',
        }


class AssetTypeFilter(filters.FilterSet):

    class Meta:
        model = AssetType
        fields = {
            'name': '__all__',
            'description': '__all__',
        }


class ApplicableUnitFilter(filters.FilterSet):

    class Meta:
        model = ApplicableUnit
        fields = {
            'name': '__all__',
            'description': '__all__',
        }


class WarrantyTypeFilter(filters.FilterSet):

    class Meta:
        model = WarrantyType
        fields = {
            'name': '__all__',
            'description': '__all__',
            'years': '__all__',
        }


class ServiceProviderFilter(filters.FilterSet):

    class Meta:
        model = ServiceProvider
        fields = {
            'name': '__all__',
            'scope': '__all__',
            'qualifications': '__all__',
        }


class CountryFilter(filters.FilterSet):

    class Meta:
        model = Country
        fields = {
            'name': '__all__',
            'code': '__all__',
        }


class StateFilter(filters.FilterSet):
    country = filters.RelatedFilter(filterset=CountryFilter, queryset=Country.objects.all())

    class Meta:
        model = State
        fields = {
            'name': '__all__',
        }


class CityFilter(filters.FilterSet):
    state = filters.RelatedFilter(filterset=StateFilter, queryset=State.objects.all())

    class Meta:
        model = City
        fields = {
            'name': '__all__',
        }


class LaborRateFilter(filters.FilterSet):
    service_provider = filters.RelatedFilter(filterset=ServiceProviderFilter, queryset=ServiceProvider.objects.all())
    country = filters.RelatedFilter(filterset=CountryFilter, queryset=Country.objects.all())
    state = filters.RelatedFilter(filterset=StateFilter, queryset=State.objects.all())
    city = filters.RelatedFilter(filterset=CityFilter, queryset=City.objects.all())
    defined_by = filters.RelatedFilter(filterset=DefinitionTypeFilter, queryset=DefinitionType.objects.all())


    class Meta:
        model = LaborRate
        fields = {
            'name': '__all__',
            'description': '__all__',
            'rate_per_hour': '__all__',
            'overhead_multiplier': '__all__',
        }


class FailureDistributionTypeFilter(filters.FilterSet):

    class Meta:
        model = FailureDistributionType
        fields = {
            'name': '__all__',
            'description': '__all__',
        }


class FailureModeFilter(filters.FilterSet):

    class Meta:
        model = FailureMode
        fields = {
            'name': '__all__',
            'description': '__all__',
        }


class FailureModeValuesFilter(filters.FilterSet):
    asset_type = filters.RelatedFilter(filterset=AssetTypeFilter, queryset=AssetType.objects.all())
    failure_distribution = filters.RelatedFilter(filterset=FailureDistributionTypeFilter, queryset=FailureDistributionType.objects.all())
    failure_mode = filters.RelatedFilter(filterset=FailureModeFilter, queryset=FailureMode.objects.all())

    class Meta:
        model = FailureModeValues
        fields = {
            'name': '__all__',
            'description': '__all__',
            'failure_parameter_1': '__all__',
            'failure_parameter_2': '__all__',
            'failure_parameter_3': '__all__',
            'outage_time': '__all__',
        }


class PlantGroupFilter(filters.FilterSet):

    class Meta:
        model = PlantGroup
        fields = {
            'name': '__all__',
            'description': '__all__',
        }


class CostModelFilter(filters.FilterSet):
    defined_by = filters.RelatedFilter(filterset=DefinitionTypeFilter, queryset=DefinitionType.objects.all())

    class Meta:
        model = CostModel
        fields = {
            'name': '__all__',
            'description': '__all__',
            'id': '__all__'
        }


class ServiceFilter(filters.FilterSet):
    defined_by = filters.RelatedFilter(filterset=DefinitionTypeFilter, queryset=DefinitionType.objects.all())
    om_type = filters.RelatedFilter(filterset=OMTypeFilter, queryset=OMType.objects.all())
    service_type = filters.RelatedFilter(filterset=ServiceTypeFilter, queryset=ServiceType.objects.all())
    asset_type = filters.RelatedFilter(filterset=AssetTypeFilter, queryset=AssetType.objects.all())
    failure_mode = filters.RelatedFilter(filterset=FailureModeFilter, queryset=FailureMode.objects.all())

    class Meta:
        model = Service
        fields = {
            'name': '__all__',
            'description': '__all__',
        }


class ServiceValuesFilter(filters.FilterSet):
    defined_by = filters.RelatedFilter(filterset=DefinitionTypeFilter, queryset=DefinitionType.objects.all())
    service = filters.RelatedFilter(filterset=ServiceFilter, queryset=Service.objects.all())
    warranty_type = filters.RelatedFilter(filterset=WarrantyTypeFilter, queryset=WarrantyType.objects.all())
    applicable_unit = filters.RelatedFilter(filterset=ApplicableUnitFilter, queryset=ApplicableUnit.objects.all())
    service_provider = filters.RelatedFilter(filterset=ServiceProviderFilter, queryset=ServiceProvider.objects.all())
    labor_rate = filters.RelatedFilter(filterset=LaborRateFilter, queryset=LaborRate.objects.all())
    failure_distribution_type = filters.RelatedFilter(filterset=FailureDistributionTypeFilter, queryset=FailureDistributionType.objects.all())
    failure_mode_values = filters.RelatedFilter(filterset=FailureModeValuesFilter, queryset=FailureModeValues.objects.all())

    class Meta:
        model = ServiceValues
        fields = {
            'name': '__all__',
            'description': '__all__',
            'labor_warranty_covered': '__all__',
            'material_warranty_covered': '__all__',
            'mean_interval': '__all__',
            'shape_factor': '__all__',
            'number_of_units': '__all__',
            'labor_hours_per_unit': '__all__',
            'material_cost_per_unit': '__all__',
        }


class PlantReportsFilter(filters.FilterSet):

    class Meta:
        model = ReportQuery
        fields = {
            'report_name': '__all__',
            'report_type': '__all__',
            'task_id': '__all__',
            'result': '__all__',
            'status': '__all__',
        }
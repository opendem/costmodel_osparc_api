
from __future__ import unicode_literals

from django.db import migrations

from costmodel.models import (
    FailureMode, DefinitionType, LaborRate, Country,
	State, City, FailureModeValues, ServiceValues,
	Service, CostModel, PlantGroup
)

#from core.models import Organization

import csv

def get_data_from_csv(file_name):
    _file = open(file_name)

    lines = []

    for f in _file.readlines():
        try:
            f.encode('ascii')
            lines.append(f)
        except:
            # skipping the reco
            print f
    Reader = csv.reader(lines)
    Data = list(Reader)

    """
    #data not in range of ord(128) error occurs on the follwoing file

    if file_name == '/mnt/c/Users/David/Desktop/costmodel_osparc_api/static/state.csv' or file_name == '/mnt/c/Users/David/Desktop/costmodel_osparc_api/static/city.csv':
    new_data = Data
    else:
    new_data = []
    for dt in Data:
      updated_data = ["" if "\\N" in d else d for d in dt]
      new_data.append(updated_data)
    """
    return Data

def country_code():
    country_data = get_data_from_csv('/mnt/c/Users/David/Desktop/costmodel_osparc_api/static/county.csv')
    template = 'insert into costmodel_country (name, code) values ("{}","{}");'
    queries = [template.format(d[1], d[2]) for d in country_data]
    return queries

def state():
    state_data = get_data_from_csv('/mnt/c/Users/David/Desktop/costmodel_osparc_api/static/state.csv')
    template = 'INSERT INTO costmodel_state (name, country_id) VALUES ("{}","{}");'
    queries = [template.format(d[1], d[2]) for d in state_data]
    return queries

def city():
    city_data = get_data_from_csv('/mnt/c/Users/David/Desktop/costmodel_osparc_api/static/city.csv')
    template = 'INSERT INTO costmodel_city (name, state_id) VALUES ("{}","{}");'
    queries = [template.format(d[1], d[2]) for d in city_data]
    return queries

def failure_mode():
    failure_mode_data = get_data_from_csv('/mnt/c/Users/David/Desktop/costmodel_osparc_api/static/failuremode.csv')
    template = 'INSERT INTO costmodel_failuremode (name, description, organization_id) VALUES ("{}", "{}", "{}");'
    queries = [template.format(d[1], d[2], d[3]) for d in failure_mode_data]
    return queries

def definition_type():
    def_type_data = get_data_from_csv('/mnt/c/Users/David/Desktop/costmodel_osparc_api/static/definitiontype.csv')
    template = 'INSERT INTO costmodel_definitiontype (name, description) VALUES ("{}", "{}");'
    queries = [template.format(d[1], d[2]) for d in def_type_data]
    return queries

def labor_rate():
    labor_rate_data = get_data_from_csv('/mnt/c/Users/David/Desktop/costmodel_osparc_api/static/laborrate.csv')
    template = 'INSERT INTO costmodel_laborrate (name, description, rate_per_hour, overhead_multiplier, city_id, country_id, defined_by_id, organization_id, service_provider_id, state_id) VALUES ("{}", "{}", "{}", "{}", "{}", "{}", "{}", "{}", "{}", "{}");'
    queries = [template.format(d[1], d[2], d[3], d[4], d[5], d[6], d[7], d[8], d[9], d[10]) for d in labor_rate_data]
    return queries

def failure_mode_values():
    failure_mode_values_data = get_data_from_csv('/mnt/c/Users/David/Desktop/costmodel_osparc_api/static/failuremodevalues.csv')
    template = 'INSERT INTO costmodel_failuremodevalues (name, description, failure_parameter_1, failure_parameter_2, failure_parameter_3, outage_time, asset_type_id, failure_distribution_id, failure_mode_id, organization_id) VALUES ("{}", "{}", "{}", "{}", "{}", "{}", "{}", "{}", "{}", "{}");'
    queries = [template.format(d[1], d[2], d[3], d[4], d[5], d[6], d[7], d[8], d[9], d[10]) for d in failure_mode_values_data]
    return queries

def service():
    service_data = get_data_from_csv('/mnt/c/Users/David/Desktop/costmodel_osparc_api/static/service.csv')
    template = 'INSERT INTO costmodel_service (name, description, asset_type_id, default_values_id, defined_by_id, om_type_id, organization_id, service_type_id) VALUES ("{}", "{}", "{}", "{}", "{}", "{}", "{}", "{}");'
    queries = [template.format(d[1], d[2], d[3], d[4], d[5], d[6], d[7], d[8]) for d in service_data]
    return queries

def service_values():
    service_values_data = get_data_from_csv('/mnt/c/Users/David/Desktop/costmodel_osparc_api/static/servicevalues.csv')
    template = 'INSERT INTO costmodel_servicevalues (name, description, labor_warranty_covered, material_warranty_covered, mean_interval, shape_factor, number_of_units, labor_hours_per_unit, material_cost_per_unit, notes, reference, applicable_unit_id, defined_by_id, failure_distribution_type_id, labor_rate_id, organization_id, service_id, service_provider_id, warranty_type_id) VALUES ("{}", "{}", "{}", "{}", "{}", "{}", "{}", "{}", "{}", "{}", "{}", "{}", "{}", "{}", "{}", "{}", "{}", "{}", "{}");'
    queries = [template.format(d[1], d[2], d[3], d[4], d[5], d[6], d[7], d[8], d[9], d[10], d[11], d[12], d[13], d[14], d[15], d[16], d[17], d[18], d[19]) for d in service_values_data]
    return queries

def cost_model():
    costmodel_data = get_data_from_csv('/mnt/c/Users/David/Desktop/costmodel_osparc_api/static/costmodel_costmodel.csv')
    template = 'INSERT INTO costmodel_costmodel (name, description, defined_by_id, organization_id) VALUES ("{}", "{}", "{}", "{}");'
    queries = [template.format(d[1], d[2], d[3], d[4]) for d in costmodel_data]
    return queries

def plant_group():
    plantgroup_data = get_data_from_csv('/mnt/c/Users/David/Desktop/costmodel_osparc_api/static/plantgroup.csv')
    template = 'INSERT INTO costmodel_plantgroup (name, description, organization_id) VALUES ("{}", "{}", "{}");'
    queries = [template.format(d[1], d[2], d[3]) for d in plantgroup_data]
    return queries

class Migration(migrations.Migration):

    dependencies = [
        ('costmodel', '0004_merge_20170821_1459'),
    ]

    operations = [
#        migrations.RunSQL(country_code()),
#        migrations.RunSQL(state()),
#        migrations.RunSQL(city()),
        migrations.RunSQL(failure_mode()),
#        migrations.RunSQL(definition_type()),
#        migrations.RunSQL(labor_rate()),
#        migrations.RunSQL(failure_mode_values()),
#        migrations.RunSQL(service()),
#        migrations.RunSQL(cost_model()),
#        migrations.RunSQL(plant_group()),
#        migrations.RunSQL(service_values()),
    ]

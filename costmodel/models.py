from __future__ import unicode_literals
from django.db import models


class PlantGroup(models.Model):
    name = models.CharField(max_length=45)
    description = models.CharField(max_length=254, blank=True, null=True)
    organization = models.ForeignKey('core.Organization', on_delete=models.CASCADE)
    plant = models.ManyToManyField('core.Plant', blank=True, null=True)
    plant_group = models.ManyToManyField('PlantGroup', blank=True, null=True)

    def __str__(self):
        return self.name


class CostModel(models.Model):
    name = models.CharField(max_length=45)
    description = models.CharField(max_length=254, blank=True, null=True)
    service_values = models.ManyToManyField('ServiceValues', related_name='cost_model')
    organization = models.ForeignKey('core.Organization', related_name='cost_model')
    defined_by = models.ForeignKey('DefinitionType')

    def __str__(self):
        return self.name

class ReportQuery(models.Model):
    report_name = models.CharField(max_length=255)
    report_type = models.CharField(max_length=255)
    organization = models.ForeignKey('core.Organization', blank=True, null=True)
    task_id = models.CharField(max_length=200)
    result = models.TextField(max_length=10000, blank=True, null=True)
    status = models.CharField(max_length=255, default="PENDING")
    costmodel = models.IntegerField(blank=True, null=True)
    plant = models.IntegerField(blank=True, null=True)
    plant_group = models.IntegerField(blank=True, null=True)


class Service(models.Model):
    name = models.CharField(unique=True, max_length=254)
    description = models.CharField(max_length=312, blank=True, null=True)
    om_type = models.ForeignKey('OMType', models.SET_NULL, blank=True, null=True)
    service_type = models.ForeignKey('ServiceType', models.SET_NULL, blank=True, null=True)
    asset_type = models.ForeignKey('AssetType', models.SET_NULL, blank=True, null=True)
    default_values = models.ForeignKey('ServiceValues', models.SET_NULL, blank=True, null=True, related_name='+')
    organization = models.ForeignKey('core.Organization')
    failure_mode = models.ManyToManyField('FailureMode', blank=True, null=True)
    defined_by = models.ForeignKey('DefinitionType')

    def __str__(self):
        return self.name


class ServiceValues(models.Model):
    name = models.CharField(max_length=45)
    description = models.CharField(max_length=254, blank=True, null=True)
    service = models.ForeignKey('Service', on_delete=models.CASCADE, related_name='service_values')
    labor_warranty_covered = models.BooleanField()
    material_warranty_covered = models.BooleanField()
    warranty_type = models.ForeignKey('WarrantyType', models.SET_NULL, blank=True, null=True)
    applicable_unit = models.ForeignKey('ApplicableUnit', models.SET_NULL, blank=True, null=True)
    service_provider = models.ForeignKey('ServiceProvider', models.SET_NULL, blank=True, null=True)
    labor_rate = models.ForeignKey('LaborRate', models.SET_NULL, blank=True, null=True)
    mean_interval = models.FloatField()
    shape_factor = models.FloatField(blank=True, null=True)
    failure_distribution_type = models.ForeignKey('FailureDistributionType', models.SET_NULL, blank=True, null=True)
    number_of_units = models.CharField(max_length=254, blank=True, null=True)
    labor_hours_per_unit = models.CharField(max_length=254, blank=True, null=True)
    material_cost_per_unit = models.CharField(max_length=254, blank=True, null=True)
    notes = models.CharField(max_length=128, blank=True, null=True)
    reference = models.CharField(max_length=254, blank=True, null=True)
    organization = models.ForeignKey('core.Organization')
    failure_mode_values = models.ManyToManyField('FailureModeValues', blank=True, null=True)
    defined_by = models.ForeignKey('DefinitionType')

    def __str__(self):
        return self.name + " - " + self.service.name


class DefinitionType(models.Model):
    name = models.CharField(unique=True, max_length=45)
    description = models.CharField(max_length=128, blank=True, null=True)

    def __str__(self):
        return self.name


class OMType(models.Model):
    name = models.CharField(unique=True, max_length=45)
    description = models.CharField(max_length=254, blank=True, null=True)

    def __str__(self):
        return self.name


class ServiceType(models.Model):
    name = models.CharField(unique=True, max_length=45)
    description = models.CharField(max_length=254, blank=True, null=True)

    def __str__(self):
        return self.name


class AssetType(models.Model):
    name = models.CharField(unique=True, max_length=45)
    description = models.CharField(max_length=254, blank=True, null=True)

    def __str__(self):
        return self.name


class ApplicableUnit(models.Model):
    name = models.CharField(unique=True, max_length=45)
    description = models.CharField(max_length=254, blank=True, null=True)

    def __str__(self):
        return self.name


class WarrantyType(models.Model):
    name = models.CharField(unique=True, max_length=45)
    description = models.CharField(max_length=128, blank=True, null=True)
    years = models.IntegerField(blank=True, null=True)

    def __str__(self):
        return self.name


class ServiceProvider(models.Model):
    name = models.CharField(unique=True, max_length=45)
    scope = models.CharField(max_length=128, blank=True, null=True)
    qualifications = models.CharField(max_length=500, blank=True, null=True)

    def __str__(self):
        return self.name


class LaborRate(models.Model):
    name = models.CharField(max_length=150)
    description = models.CharField(max_length=254, blank=True, null=True)
    service_provider = models.ForeignKey('ServiceProvider', on_delete=models.CASCADE)
    rate_per_hour = models.FloatField()
    overhead_multiplier = models.FloatField()
    country = models.ForeignKey('Country', blank=True, null=True)
    state = models.ForeignKey('State', blank=True, null=True)
    city = models.ForeignKey('City', blank=True, null=True)
    organization = models.ForeignKey('core.Organization')
    defined_by = models.ForeignKey('DefinitionType')

    def __str__(self):
        return self.name + " - " + self.service_provider.name

    def _get_loaded_rate(self):
        try:
            return "%.2f" % float(self.rate_per_hour * self.overhead_multiplier)
        except TypeError:
            return 0

    loaded_rate_per_hour = property(_get_loaded_rate)

    class Meta:
        unique_together = (("name", "organization", "country", "state", "city", "service_provider"),)


class Country(models.Model):
    name = models.CharField(unique=True, max_length=150)
    code = models.CharField(max_length=3)

    def __str__(self):
        return self.name


class State(models.Model):
    name = models.CharField(max_length=128)
    country = models.ForeignKey('Country')

    def __str__(self):
        return self.name


class City(models.Model):
    name = models.CharField(max_length=128)
    state = models.ForeignKey('State')
    latitude = models.FloatField(default=36.744698)
    longitude = models.FloatField(default=-121.360946)

    def __str__(self):
        return self.name


class FailureDistributionType(models.Model):
    name = models.CharField(unique=True, max_length=45)
    description = models.CharField(max_length=254, blank=True, null=True)

    def __str__(self):
        return self.name


class FailureMode(models.Model):
    name = models.CharField(max_length=60)
    description = models.CharField(max_length=128, blank=True, null=True)
    organization = models.ForeignKey('core.Organization')

    def __str__(self):
        return self.name + " - " + self.description


class FailureModeValues(models.Model):
    name = models.CharField(max_length=60)
    description = models.CharField(max_length=128, blank=True, null=True)
    asset_type = models.ForeignKey('AssetType', models.SET_NULL, blank=True, null=True)
    failure_distribution = models.ForeignKey('FailureDistributionType', models.SET_NULL, blank=True, null=True)
    failure_parameter_1 = models.FloatField(blank=True, null=True)
    failure_parameter_2 = models.FloatField(blank=True, null=True)
    failure_parameter_3 = models.FloatField(blank=True, null=True)
    outage_time = models.FloatField(blank=True, null=True)
    failure_mode = models.ForeignKey(FailureMode, on_delete=models.CASCADE)
    organization = models.ForeignKey('core.Organization')

    def __str__(self):
        return self.name + " - " + self.failure_mode.name + self.failure_mode.description



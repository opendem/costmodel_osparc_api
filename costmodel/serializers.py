from rest_framework import serializers
from models import *
from drf_queryfields import QueryFieldsMixin


class PlantGroupSerializer(serializers.ModelSerializer):

    class Meta:
        model = PlantGroup
        fields = '__all__'


class CostModelSerializer(serializers.ModelSerializer):

    class Meta:
        model = CostModel
        fields = ('id', 'name', 'description', 'organization', 'defined_by', 'service_values', 'plant')


class ServiceSerializer(serializers.ModelSerializer):

    class Meta:
        model = Service
        fields = '__all__'


class ServiceValuesSerializer(QueryFieldsMixin, serializers.ModelSerializer):

    class Meta:
        model = ServiceValues
        fields = ('id', 'name', 'description', 'service', 'labor_warranty_covered', 'material_warranty_covered',
                  'warranty_type', 'applicable_unit', 'service_provider', 'labor_rate', 'mean_interval',
                  'shape_factor', 'failure_distribution_type', 'number_of_units', 'labor_hours_per_unit',
                  'material_cost_per_unit', 'notes', 'reference', 'organization', 'defined_by',
                  'failure_mode_values', 'cost_model')


class LaborRateSerializer(serializers.ModelSerializer):
    loaded_rate_per_hour = serializers.ReadOnlyField()

    class Meta:
        model = LaborRate
        fields = '__all__'


class DefinitionTypeSerializer(serializers.ModelSerializer):

    class Meta:
        model = DefinitionType
        fields = '__all__'


class OMTypeSerializer(serializers.ModelSerializer):

    class Meta:
        model = OMType
        fields = '__all__'


class ServiceTypeSerializer(serializers.ModelSerializer):

    class Meta:
        model = ServiceType
        fields = '__all__'


class AssetTypeSerializer(serializers.ModelSerializer):

    class Meta:
        model = AssetType
        fields = '__all__'


class ApplicableUnitSerializer(serializers.ModelSerializer):

    class Meta:
        model = ApplicableUnit
        fields = '__all__'


class WarrantyTypeSerializer(serializers.ModelSerializer):

    class Meta:
        model = WarrantyType
        fields = '__all__'


class ServiceProviderSerializer(serializers.ModelSerializer):

    class Meta:
        model = ServiceProvider
        fields = '__all__'


class FailureDistributionTypeSerializer(serializers.ModelSerializer):

    class Meta:
        model = FailureDistributionType
        fields = '__all__'


class CountrySerializer(serializers.ModelSerializer):

    class Meta:
        model = Country
        fields = '__all__'


class StateSerializer(serializers.ModelSerializer):

    class Meta:
        model = State
        fields = '__all__'


class CitySerializer(serializers.ModelSerializer):

    class Meta:
        model = City
        fields = '__all__'


class FailureModeSerializer(serializers.ModelSerializer):

    class Meta:
        model = FailureMode
        fields = '__all__'


class FailureModeValuesSerializer(serializers.ModelSerializer):

    class Meta:
        model = FailureModeValues
        fields = '__all__'


class PlantReportsSerializer(serializers.ModelSerializer):

    class Meta:
        model = ReportQuery
        fields = '__all__'
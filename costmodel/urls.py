from django.conf.urls import url
from . import views
from django.conf.urls import include


urlpatterns = [

    # Plant Groups, Cost Models, Services, Labor Rates, Failure Modes

    url(r'^organizations/(?P<org_pk>[0-9]+)/plant_groups/$', views.PlantGroupList.as_view()),
    url(r'^organizations/(?P<org_pk>[0-9]+)/plant_groups/(?P<pk>[0-9]+)/$', views.PlantGroupDetail.as_view()),

    url(r'^organizations/(?P<org_pk>[0-9]+)/cost_models/$', views.CostModelList.as_view(), name='costmodel_list'),
    url(r'^organizations/(?P<org_pk>[0-9]+)/cost_models/(?P<pk>[0-9]+)/$', views.CostModelDetail.as_view()),

    url(r'^organizations/(?P<org_pk>[0-9]+)/services/$', views.ServiceList.as_view(), name='service_list'),
    url(r'^organizations/(?P<org_pk>[0-9]+)/services/(?P<pk>[0-9]+)/$', views.ServiceDetail.as_view()),

    url(r'^organizations/(?P<org_pk>[0-9]+)/service_values/$', views.ServiceValuesList.as_view()),
    url(r'^organizations/(?P<org_pk>[0-9]+)/service_values/(?P<pk>[0-9]+)/$', views.ServiceValuesDetail.as_view()),

    url(r'^organizations/(?P<org_pk>[0-9]+)/labor_rates/$', views.LaborRateList.as_view(), name='labor_rate_list'),
    url(r'^organizations/(?P<org_pk>[0-9]+)/labor_rates/(?P<pk>[0-9]+)/$', views.LaborRateDetail.as_view()),

    url(r'^failure_modes/$', views.FailureModeList.as_view(), name='failure_mode_list'),
    url(r'^failure_modes/(?P<pk>[0-9]+)/$', views.FailureModeDetail.as_view()),

    url(r'^failure_mode_values/$', views.FailureModeValuesList.as_view(), name='failure_mode_values_list'),
    url(r'^failure_mode_values/(?P<pk>[0-9]+)/$', views.FailureModeValuesDetail.as_view()),

    # Aggregates

    url(r'^plant_group_count/$', views.plant_group_count),
    url(r'^plant_count/$', views.plant_count),
    url(r'^costmodel_count/$', views.costmodel_count),
    url(r'^service_count/$', views.service_count),

    url(r'^results/$', views.results, name='results'),
    url(r'^plant_group_results/$', views.plant_group_results, name='plant_group_results'),
    url(r'^organizations/(?P<org_pk>[0-9]+)/plant_reports/$', views.PlantReportsList.as_view()),
    url(r'^organizations/(?P<org_pk>[0-9]+)/plant_reports/(?P<pk>[0-9]+)/$', views.PlantReportsDetail.as_view()),
    url(r'^export/$', views.export, name='export'),

    # Enumerations

    url(r'^definition_types/$', views.DefinitionTypeList.as_view(), name='definition_type_list'),
    url(r'^definition_types/(?P<pk>[0-9]+)/$', views.DefinitionTypeDetail.as_view()),

    url(r'^asset_types/$', views.AssetTypeList.as_view()),
    url(r'^asset_types/(?P<pk>[0-9]+)/$', views.AssetTypeDetail.as_view()),

    url(r'^om_types/$', views.OMTypeList.as_view(), name='om_type_list'),
    url(r'^om_types/(?P<pk>[0-9]+)/$', views.OMTypeDetail.as_view()),

    url(r'^service_types/$', views.ServiceTypeList.as_view()),
    url(r'^service_types/(?P<pk>[0-9]+)/$', views.ServiceTypeDetail.as_view()),

    url(r'^countries/$', views.CountryList.as_view()),
    url(r'^countries/(?P<pk>[0-9]+)/$', views.CountryDetail.as_view()),

    url(r'^states/$', views.StateList.as_view()),
    url(r'^states/(?P<pk>[0-9]+)/$', views.StateDetail.as_view()),

    url(r'^cities/$', views.CityList.as_view()),
    url(r'^cities/(?P<pk>[0-9]+)/$', views.CityDetail.as_view()),

    url(r'^applicable_units/$', views.ApplicableUnitList.as_view(), name='applicable_unit_list'),
    url(r'^applicable_units/(?P<pk>[0-9]+)/$', views.ApplicableUnitDetail.as_view()),

    url(r'^failure_distribution_types/$', views.FailureDistributionTypeList.as_view(), name='failure_distributions_list'),
    url(r'^failure_distribution_types/(?P<pk>[0-9]+)/$', views.FailureDistributionTypeDetail.as_view()),

    url(r'^service_providers/$', views.ServiceProviderList.as_view(), name='service_provider_list'),
    url(r'^service_providers/(?P<pk>[0-9]+)/$', views.ServiceProviderDetail.as_view()),

    url(r'^warranty_types/$', views.WarrantyTypeList.as_view(), name='warranty_type_list'),
    url(r'^warranty_types/(?P<pk>[0-9]+)/$', views.WarrantyTypeDetail.as_view()),

    url(r'^organizations/(?P<org_pk>[0-9]+)/default-service-values/$', views.DefaultServiceList.as_view()),
    url(r'^organizations/(?P<org_pk>[0-9]+)/default-costmodel-values/$', views.DefaultCostModelValuesList.as_view()),

]

urlpatterns += [
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
]


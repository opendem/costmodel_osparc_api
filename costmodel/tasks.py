from celery import shared_task
from core import models as core_models
from osparc import models as osparc_models
from costmodel import models as costmodel_models
from osparc.mixins import (
    calculate_n_by_N, plants_in_group, merge_records_by,
    merge_list_of_records_by
)
from osparc.mixins import get_results as result_for_plant_group

import decimal
import json
import math
from collections import Counter
import numpy as np
from functools import reduce
from itertools import groupby
from operator import add, itemgetter

from django.db.models import Q

@shared_task
def create_plant_report(costmodel_id, plant_id, custom_query_id):
    total_labor_hours_per_year = []
    total_labor_cost = []
    total_matl_cost = []
    avg_annual_expense = []
    npv_life = []

    percentage_of_total_npv = []
    service_list = []
    service_instance_list = []
    service_id_list = []

    # Fetch the Plant and Cost Model
    costmodel = costmodel_models.CostModel.objects.get(id=costmodel_id)
    plant = core_models.Plant.objects.get(id=plant_id)
    plant_name = plant.name
    costmodel_name = costmodel.name

    # check_if report already exists and delete it, so we dont have 
    # multiple reports with the same name and same report
    check = costmodel_models.ReportQuery.objects.filter(
        report_name=plant.name + " - " + costmodel.name
    )

    for c in check:
        if c.status != "PENDING":
            c.delete()

    if plant.plant_finance is None:
        return {'status': 'error'}

    if plant.plant_equipment is None:
        return {'status': 'error'}

    for service_values in costmodel.service_values.all():
        service_id_list.append(service_values.id)
        service_list.append(service_values.service.name)
        service_instance_list.append(service_values.name)

        try:
            number_of_units = eval(service_values.number_of_units)
        except (ZeroDivisionError, TypeError, AttributeError):
            number_of_units = 0

        try:
            labor_hours_per_unit = eval(service_values.labor_hours_per_unit)
        except (ZeroDivisionError, TypeError, AttributeError):
            labor_hours_per_unit = 0

        try:
            material_cost_per_unit = eval(service_values.material_cost_per_unit)
        except (ZeroDivisionError, TypeError, AttributeError):
            material_cost_per_unit = 0

        # Calculate Total Labor Cost
        total_labor_cost.append(float(number_of_units) * float(labor_hours_per_unit) * float(service_values.labor_rate.rate_per_hour) * float(service_values.labor_rate.overhead_multiplier))

        # Calculate Total Material and Other Cost
        total_matl_cost.append(float(number_of_units) * float(material_cost_per_unit))

        # Calculate Total Labor Hours per Year
        total_labor_hours_per_year.append(float(number_of_units) * float(labor_hours_per_unit) / float(service_values.mean_interval))

    # Calculate Total Cost for Measure
    total_cost_for_measure = [x + y for x, y in zip(total_labor_cost, total_matl_cost)]
    d_total_cost_for_measure = ["%.2f" % v for v in total_cost_for_measure]

    total_matl_cost = [decimal.Decimal(x) for x in total_matl_cost]
    total_labor_cost = [decimal.Decimal(x) for x in total_labor_cost]
    d_total_labor_cost = ["%.2f" % v for v in total_labor_cost]
    d_total_labor_hours_per_year = ["%.2f" % v for v in total_labor_hours_per_year]

    # Calculate Annual Cash Flow
    columns = plant.plant_finance.analysis_period
    rows = costmodel.service_values.count()
    annual_cash_flow = [[0 for x in range(columns)] for y in range(rows)]
    d_annual_cash_flow = [[0 for x in range(columns)] for y in range(rows)]
    # npv_life = np.zeros(rows)

    warranty_years = 0

    # for i in range(rows):
    # for i in range(len(annual_cash_flow)) and service_values in costmodel.service_values.all():
    for i, k, l, m, service_values in map(None, range(len(annual_cash_flow)), total_cost_for_measure, total_labor_cost, total_matl_cost, costmodel.service_values.all()):
        # for j in range(columns):
        for j in range(len(annual_cash_flow[i])):

            # Determine the number of years covered by warranty
            if service_values.warranty_type.name == "Module (Product)":
                warranty_years = plant.plant_finance.module_warranty
            elif service_values.warranty_type.name == "Inverter":
                warranty_years = plant.plant_finance.inverter_warranty
            elif service_values.warranty_type.name == "EPC":
                warranty_years = plant.plant_finance.epc_warranty
            elif service_values.warranty_type.name == "Monitoring":
                warranty_years = plant.plant_finance.monitoring_warranty

            if j+1 > warranty_years:
                if service_values.failure_distribution_type.id == 1:
                    if service_values.mean_interval < 1.00:
                        # A
                        annual_cash_flow[i][j] = float((k/float(service_values.mean_interval)) * float(pow((1+(plant.plant_finance.inflation_rate/100)), j+1)))

                    else:
                        # B
                        annual_cash_flow[i][j] = (int((j+1)/service_values.mean_interval) - int((j+1-1)/service_values.mean_interval)) * float(k) * float(pow((1+(plant.plant_finance.inflation_rate/100)), (j+1)))

                else:
                    # WEIBULL.DIST(j, service_values.shape_factor, service_values.mean_interval, False)
                    x = j+1
                    a = service_values.shape_factor
                    b = service_values.mean_interval
                    weibull = float(a/pow(b, a)) * float(pow(x, a-1)) * float(math.exp(-(pow((x/b), a))))
                    # C
                    annual_cash_flow[i][j] = float(weibull * float(k) * float(pow((1+(plant.plant_finance.inflation_rate/100)), j+1)))

            else:
                if service_values.failure_distribution_type.id == 1:
                    if service_values.mean_interval < 1.00:
                        # D
                        annual_cash_flow[i][j] = float((float(m * (1-service_values.material_warranty_covered) + l * (1-service_values.labor_warranty_covered)) / service_values.mean_interval) * (pow((1+(plant.plant_finance.inflation_rate/100)), j+1)))
                    else:
                        # E
                        annual_cash_flow[i][j] = (int((j+1)/service_values.mean_interval) - int((j+1-1) / service_values.mean_interval)) * float(m * (1-service_values.material_warranty_covered) + l * (1-service_values.labor_warranty_covered)) * float(pow((1+(plant.plant_finance.inflation_rate/100)), j+1))

                else:
                    # WEIBULL.DIST(j, service_values.shape_factor, service_values.mean_interval, False)
                    x = j+1
                    a = service_values.shape_factor
                    b = service_values.mean_interval
                    weibull = float(a / pow(b, a)) * float(pow(x, a - 1)) * float(math.exp(-(pow((x / b), a))))
                    # F
                    annual_cash_flow[i][j] = float(weibull * float(m * (1-service_values.material_warranty_covered) + l * (1-service_values.labor_warranty_covered)) * float((pow((1+(plant.plant_finance.inflation_rate/100)), j+1))))

    npv = 0.00
    for i in range(len(annual_cash_flow)):
        # Calculate Average Annual Expense
        avg_annual_expense.append(sum(annual_cash_flow[i]) / len(annual_cash_flow[i]))
        d_avg_annual_expense = ["%.2f" % v for v in avg_annual_expense]

        values = [float(j) for j in (annual_cash_flow[i])]
        # Calculate NPV Project Life
        for key, value in enumerate(values):
            npv += (value / (pow((1 + float(plant.plant_finance.discount_rate/100)), key + 1)))
        npv_life.append(npv)
        npv = 0.00
        # npv_life.append(np.npv(float(plant.plant_finance.discount_rate/100), [float(j) for j in (annual_cash_flow[i])]))
        total_npv_life = sum(npv_life)
        d_total_npv_life = "%.2f" % total_npv_life
        d_npv_life = ["%.2f" % v for v in npv_life]

    sum_avg_annual_expense = sum(float(v) for v in avg_annual_expense)
    d_sum_avg_annual_expense = ["%.2f" % sum_avg_annual_expense]

    for i in range(len(npv_life)):
        # Calculate Percentage of Total NPV
        percentage_of_total_npv.append((npv_life[i]/reduce(lambda x, y: x + y, npv_life))*100)
        twodecimals_percentage_of_total_npv = ["%.2f" % v for v in percentage_of_total_npv]

    for i in range(len(annual_cash_flow)):
        d_annual_cash_flow[i] = ["%.2f" % v for v in annual_cash_flow[i]]
        # Calculate Sum of Annual Cash Flow
        sum_acf = [sum(x) for x in zip(*[[float(y) for y in x] for x in annual_cash_flow])]  # (*annual_cash_flow)]
        d_sum_acf = ["%.2f" % v for v in sum_acf]

        sum_annual_cash_flow = []
    for i, j in map(None, range(columns), d_sum_acf):
        sum_annual_cash_flow.append({'year': i+1, 'value': j})

    # Calculate NPV by O&M Type
    cost_per_year_by_om_type = Counter()
    for service_values, j in map(None, costmodel.service_values.all(), avg_annual_expense):
        cost_per_year_by_om_type[service_values.service.om_type.name] += float(j)

    npv_by_om_type = Counter()
    for service_values, j in map(None, costmodel.service_values.all(), npv_life):
        npv_by_om_type[service_values.service.om_type.name] += float(j)

    npv_by_activity = []

    for i, j in map(None, cost_per_year_by_om_type.items(), npv_by_om_type.items()):
        npv_by_activity.append(
             {'label': i[0], 'cost_per_year': "%.2f" %(i[1]), 'npv': "%.2f" % (j[1]),
              'npv_per_wp': "%.4f" % (j[1]/(plant.dc_rating*1000)),
              'value': "%.2f" % ((j[1]/total_npv_life)*100)})

    # Calculate NPV by Asset Type
    cost_per_year_by_om_category = Counter()
    for service_values, j in map(None, costmodel.service_values.all(), avg_annual_expense):
        cost_per_year_by_om_category[service_values.service.asset_type.name] += float(j)

    npv_by_om_category = Counter()
    for service_values, j in map(None, costmodel.service_values.all(), npv_life):
        npv_by_om_category[service_values.service.asset_type.name] += float(j)

    npv_life_by_om_category = []

    for i, j in map(None, cost_per_year_by_om_category.items(), npv_by_om_category.items()):
        npv_life_by_om_category.append(
             {'asset_type': i[0], 'cost_per_year': "%.2f" % (i[1]), 'npv': "%.2f" % (j[1]),
              'percentage_of_npv': "%.2f" % ((j[1] / total_npv_life)*100)})

    # Calculate NPV by Service Provider
    cost_per_year_by_sp = Counter()
    for service_values, j in map(None, costmodel.service_values.all(), avg_annual_expense):
        cost_per_year_by_sp[service_values.service_provider.name] += float(j)

    npv_by_sp = Counter()
    for service_values, j in map(None, costmodel.service_values.all(), npv_life):
        npv_by_sp[service_values.service_provider.name] += float(j)

    npv_life_by_sp = []

    for i, j in map(None, cost_per_year_by_sp.items(), npv_by_sp.items()):
        npv_life_by_sp.append(
             {'service_provider': i[0], 'cost_per_year': "%.2f" % (i[1]), 'npv': "%.2f" % (j[1]),
              'percentage_of_npv': "%.2f" % ((j[1] / total_npv_life)*100)})

    # Calculate Reserve Account
    columns = plant.plant_finance.analysis_period
    rows = costmodel.service_values.count()
    reserve_account = [[0 for x in range(columns)] for y in range(rows)]
    d_reserve_account = [[0 for x in range(columns)] for y in range(rows)]

    warranty_years = 0

    for i, k, l, m, service_values in map(None, range(len(reserve_account)), total_cost_for_measure, total_labor_cost, total_matl_cost, costmodel.service_values.all()):
        for j in range(len(reserve_account[i])):

            # Calculate No of Units
            try:
                number_of_units = eval(service_values.number_of_units)
            except (ZeroDivisionError, TypeError):
                number_of_units = 0

            # Determine the number of years covered by warranty
            if service_values.warranty_type.name == "Module (Product)":
                warranty_years = plant.plant_finance.module_warranty
            elif service_values.warranty_type.name == "Inverter":
                warranty_years = plant.plant_finance.inverter_warranty
            elif service_values.warranty_type.name == "EPC":
                warranty_years = plant.plant_finance.epc_warranty
            elif service_values.warranty_type.name == "Monitoring":
                warranty_years = plant.plant_finance.monitoring_warranty

            if j + 1 > warranty_years:
                if service_values.failure_distribution_type.id == 1:
                    if service_values.mean_interval < 1.00:
                        # A
                        reserve_account[i][j] = max(annual_cash_flow[i][j], (float((k / float(service_values.mean_interval)) * float(pow((1 + (plant.plant_finance.inflation_rate / 100)), j + 1)))))

                    else:
                        # B
                        reserve_account[i][j] = max(annual_cash_flow[i][j], (int((j+1) / service_values.mean_interval) - int((j+1-1) / service_values.mean_interval) * float(k) * float(pow((1 + (plant.plant_finance.inflation_rate / 100)), j + 1))))

                else:
                    # WEIBULL.DIST(j, service_values.shape_factor, service_values.mean_interval, False)
                    x = j + 1
                    a = service_values.shape_factor
                    b = service_values.mean_interval
                    weibull = float(a / pow(b, a)) * float(pow(x, a - 1)) * float(math.exp(-(pow((x / b), a))))

                    # n_by_N
                    n_by_N = calculate_n_by_N(number_of_units, weibull, plant.plant_finance.desired_confidence_that_reserve_covers_cost)

                    # New Reserve Account formula - Uses n_by_N_calculation
                    reserve_account[i][j] = max(annual_cash_flow[i][j], (n_by_N * float(k) * float(pow((1 + (plant.plant_finance.inflation_rate / 100)), j + 1))))

                   
            else:
                if service_values.failure_distribution_type.id == 1:
                    if service_values.mean_interval < 1.00:
                        # D
                        reserve_account[i][j] = max(annual_cash_flow[i][j], (float((float(m * (1 - service_values.material_warranty_covered) + l * (1 - service_values.labor_warranty_covered)) / service_values.mean_interval) * (pow((1 + (plant.plant_finance.inflation_rate / 100)), j + 1)))))
                    else:
                        # E
                        reserve_account[i][j] = max(annual_cash_flow[i][j], (float(float(int((j + 1) / service_values.mean_interval) - int((j + 1 - 1) / service_values.mean_interval)) * float(m * (1 - service_values.material_warranty_covered) + l * (1 - service_values.labor_warranty_covered)) * (pow((1 + (plant.plant_finance.inflation_rate / 100)), j + 1)))))

                else:
                    # WEIBULL.DIST(j, service_values.shape_factor, service_values.mean_interval, False)
                    x = j + 1
                    a = service_values.shape_factor
                    b = service_values.mean_interval
                    weibull = float(a / pow(b, a)) * float(pow(x, a - 1)) * float(math.exp(-(pow((x / b), a))))

                    # n_by_N
                    n_by_N = calculate_n_by_N(number_of_units, weibull, plant.plant_finance.desired_confidence_that_reserve_covers_cost)

                    # New Reserve Account formula
                    reserve_account[i][j] = max(annual_cash_flow[i][j], (n_by_N * float(m * (1 - service_values.material_warranty_covered) + l * (1 - service_values.labor_warranty_covered)) * float((pow((1 + (plant.plant_finance.inflation_rate / 100)), j + 1)))))

    for i in range(len(reserve_account)):
        d_reserve_account[i] = ["%.2f" % v for v in reserve_account[i]]
        # Calculate Sum of Reserve Account
        sum_ra = [sum(x) for x in zip(*[[float(y) for y in x] for x in reserve_account])]
        d_sum_ra = ["%.2f" % v for v in sum_ra]

        sum_reserve_account = []
    for i, j in map(None, range(columns), d_sum_ra):
        sum_reserve_account.append({'year': str(i + 1), 'value': j})

    annual_production = []
    for j in range(columns):
        annual_production.append({'year': j + 1,
                                  'annual_production_cash_flow': "%.2f" %(plant.dc_rating * plant.energy_yield*(pow((1-plant.plant_equipment.whole_system_degradation_rate), j+1))+0.0000000000001),
                                  'annual_production_reserve_account': "%.2f" %(plant.dc_rating * plant.energy_yield * (pow((1 - plant.plant_equipment.whole_system_degradation_rate), j + 1)) + 0.0000000000001)
                                  })

    annual_average = []
    for i, j in map(None, range(columns), sum_ra):
        annual_average.append({'year': str(i + 1),
                               'annual_average_cash_flow': "%.5f" %(j/(plant.dc_rating*plant.energy_yield*(pow((1-plant.plant_equipment.whole_system_degradation_rate), i+1))+0.0000000000001)),
                               'annual_average_reserve_account': "%.5f" %(j/(plant.dc_rating*plant.energy_yield*(pow((1-plant.plant_equipment.whole_system_degradation_rate), i+1))+0.0000000000001))
                               })

    ir = plant.plant_finance.inflation_rate / 100
    dr = plant.plant_finance.discount_rate / 100

    annualized_om_costs = total_npv_life / float(
        (1 + ir) / (dr - ir) * (1 - (pow(((1 + ir) / (1 + dr)), plant.plant_finance.analysis_period))))

    maximum_reserve_account = max([x['value'] for x in sum_reserve_account])

    # Calculate npv_annual_om_costs_per_kwh
    npv_annual_om_costs = 0.00
    annual_production_cash_flow = []
    for d in annual_production:
        annual_production_cash_flow.append(d['annual_production_cash_flow'])
    values = [float(j) for j in annual_production_cash_flow]

    for key, value in enumerate(values):
        npv_annual_om_costs += (value / (pow((1 + float(plant.plant_finance.discount_rate / 100)), key + 1)))

    final_results = [{'annualized_om_costs': "%.2f" % annualized_om_costs,
                      'annualized_unit_om_costs': "%.2f" % (annualized_om_costs / plant.dc_rating),
                      'maximum_reserve_account': maximum_reserve_account,
                      'npv_om_costs': d_total_npv_life,
                      'npv_per_wp': "%.4f" % (total_npv_life / (plant.dc_rating*1000)),
                      'npv_annual_om_costs_per_kwh': "%.5f" % (float(d_total_npv_life)/npv_annual_om_costs)}]

    # Calculate Failure Mode Analysis Data
    columns = plant.plant_finance.analysis_period
    rows = costmodel.service_values.count()
    failure_mode_analysis = [[0 for x in range(columns)] for y in range(rows)]

    dict_failure_mode_analysis = []

    for k, service_values, acf in map(None, total_cost_for_measure, costmodel.service_values.all(), annual_cash_flow):

        if service_values.failure_mode_values is not None:
            for j, lcoe in map(None, service_values.failure_mode_values.all(), annual_average):
                if j is not None:
                    cost_of_failure = j.outage_time * float(lcoe['annual_average_cash_flow']) * float(plant.plant_equipment.inverter_capacity)

                    if j.failure_distribution.id == 1:
                        expected_cost_of_failure = []
                        for year in range(plant.plant_finance.analysis_period):
                            expected_cost_of_failure.append(cost_of_failure * j.failure_parameter_1)
                    elif j.failure_distribution.id == 2:
                        expected_cost_of_failure = []
                        for year in range(plant.plant_finance.analysis_period):
                            x = year
                            a = j.failure_parameter_1
                            b = j.failure_parameter_2
                            weibull = 1 - (math.exp(-(pow((x / b), a))))
                            expected_cost_of_failure.append(weibull * cost_of_failure)
                    flag = []
                    for a, b in map(None, acf, expected_cost_of_failure):
                        if a < b:
                            flag.append(1)
                        else:
                            flag.append(0)
                    dict_failure_mode_analysis.append({'service_type_name': service_values.service.name,
                                                       'service_name': service_values.name,
                                                       'failure_mode': j.failure_mode.name + ' ' + j.failure_mode.description,
                                                       'cost_of_failure': "%.2f" %(cost_of_failure),
                                                       'expected_cost_of_failure': ["%.2f" % v for v in expected_cost_of_failure],
                                                       'annual_cash_flow': ["%.2f" % v for v in acf],
                                                       'flag': flag})

    result = {'plant_name': plant_name, 'costmodel_name': costmodel_name, 'service_id_list': service_id_list,
            'service_list': service_list, 'service_instance_list': service_instance_list,
            'total_labor_hours_per_year': d_total_labor_hours_per_year, 'total_labor_cost': d_total_labor_cost,
            'total_material_and_other_cost': total_matl_cost, 'total_cost_for_measure': d_total_cost_for_measure,
            'average_annual_expense': d_avg_annual_expense, 'sum_average_annual_expense': d_sum_avg_annual_expense,
            'npv_project_life': d_npv_life, 'sum_npv_project_life': d_total_npv_life,
            'percentage_of_total_npv': twodecimals_percentage_of_total_npv,
            'annual_cash_flow': d_annual_cash_flow, 'sum_annual_cash_flow': sum_annual_cash_flow,
            'npv_by_activity': npv_by_activity, 'npv_by_om_category': npv_life_by_om_category,
            'npv_by_service_provider': npv_life_by_sp, 'final_results': final_results,
            'reserve_account': d_reserve_account, 'sum_reserve_account': sum_reserve_account,
            'annual_production': annual_production, 'annual_average': annual_average,
            'dict_failure_mode_analysis': dict_failure_mode_analysis
            }

    result_for_rq = result

    total_mc = [str(float(r)) for r in result['total_material_and_other_cost']]
    result_for_rq.update({'total_material_and_other_cost': total_mc })

    update_rq_result = costmodel_models.ReportQuery.objects.filter(
    	id=custom_query_id
    ).update(result=json.dumps(result_for_rq), status="SUCCESS")

    return result

@shared_task
def create_plant_group_report(plant_group_id, custom_query_id):

    npv_per_wp = 0
    npv_om_costs = 0
    npv_by_activity = []
    plant_names = []

    plant_group = costmodel_models.PlantGroup.objects.get(id=plant_group_id)

    # check_if report already exists and delete it, so we dont have 
    # multiple reports with the same name and same report
    check = costmodel_models.ReportQuery.objects.filter(
        report_name=plant_group.name
    )

    for c in check:
        if c.status != "PENDING":
            c.delete()

    plant_list = plants_in_group(plant_group_id)

    for plant in plant_list:
        plant_names.append(plant.name)
        if plant.active_cost_model:
            plant_id = int(plant.id)
            costmodel_id = int(plant.active_cost_model.id)

            data = result_for_plant_group(plant_id, costmodel_id)

            if data:
                final_results = data['final_results']
                npv_per_wp += float(final_results[0]['npv_per_wp'])
                npv_om_costs += float(final_results[0]['npv_om_costs'])

                merger = merge_list_of_records_by('label', add)
                npv_by_activity = merger(npv_by_activity + data['npv_by_activity'])


    result = {
        'plant_group_name': plant_group.name,
        'number_of_plants': len(plant_list),
        'plant_names': plant_names,
        'npv_per_wp': npv_per_wp,
        'npv_om_costs': npv_om_costs,
        'npv_by_activity': npv_by_activity
    }

    # can check if report already exists wiht the plant_group.name
    if result['npv_by_activity']:
        update_rq_result = costmodel_models.ReportQuery.objects.filter(
            id=custom_query_id
        ).update(result=json.dumps(result), status="SUCCESS")
    else:
        update_rq_result = costmodel_models.ReportQuery.objects.filter(
            id=custom_query_id
        ).update(status="Report could not be created due to no active costmodel associated with the plants in group.")

    return result
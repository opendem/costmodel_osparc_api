# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2017-10-20 16:56
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('costmodel', '0005_auto_20171020_1637'),
    ]

    operations = [
        migrations.AlterField(
            model_name='city',
            name='latitude',
            field=models.FloatField(default=0),
        ),
        migrations.AlterField(
            model_name='city',
            name='longitude',
            field=models.FloatField(default=0),
        ),
    ]

# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations

from costmodel.models import (
    OMType, ServiceType, AssetType, ApplicableUnit,
    WarrantyType, ServiceProvider, FailureDistributionType,
)

def forwards_func(apps, schema_editor):
    ## For OMType ##
    om_type_list = [
        'Administrative', 'Corrective', 'Preventative'
    ]

    OMType = apps.get_model("costmodel", "OMType")

    om_type_instance_list = [
        OMType(name=om, description=om) for om in om_type_list
    ]

    OMType.objects.bulk_create(om_type_instance_list)

    ## For ServiceType ##
    service_type_list = [
        'Cleaning', 'Emergency Response', 'Inspection',
        'Management', 'Repair', 'General Maintenance', 'Testing'
    ]

    ServiceType = apps.get_model("costmodel", "ServiceType")

    service_type_instance_list = [
        ServiceType(name=st, description=st) for st in service_type_list
    ]

    ServiceType.objects.bulk_create(service_type_instance_list)

    ## For AssetType ##
    asset_type_list = [
        'Asset Management', 'AC Wiring', 'DC Wiring',
        'Documents', 'Electrical', 'Mechanical', 'Inverter',
        'Meter', 'Monitoring System', 'Mounting Structure',
        'PV Module', 'Roof', 'Tracker', 'Transformer', 'PV System'
    ]

    AssetType = apps.get_model("costmodel", "AssetType")

    asset_type_instance_list = [
        AssetType(name=at, description=at) for at in asset_type_list
    ]

    AssetType.objects.bulk_create(asset_type_instance_list)

    ## For ApplicableUnit ##
    applicable_unit_list = [
        'System', 'Inverter', 'Site', 'Disconnect Box',
        'Combiner Box', 'Rail/ Fastener', 'PV Module', 'Structure',
        'NCU', 'Connection', 'Strings', 'Acre', 'Weather Station',
        'Controller', 'Motor', 'Transformer', 'Driveshaft', 'Block',
        'Slew Gear', 'Row', 'Percent of Initial Cost', 'MW'
    ]

    ApplicableUnit = apps.get_model("costmodel", "ApplicableUnit")

    applicable_unit_instance_list = [
        ApplicableUnit(name=au, description=au) for au in applicable_unit_list
    ]

    ApplicableUnit.objects.bulk_create(applicable_unit_instance_list)

    ## For WarrantyType ##
    warranty_type_list = [
        'EPC', 'Inverter', 'Module (Product)',
        'Monitoring', 'N/A'
    ]

    WarrantyType = apps.get_model("costmodel", "WarrantyType")

    warranty_type_instance_list = [
        WarrantyType(name=wt, description=wt, years=0) for wt in warranty_type_list
    ]

    WarrantyType.objects.bulk_create(warranty_type_instance_list)

    ## For ServiceProvider ##
    service_provider_list = [
        'Administrator', 'Designer', 'Cleaner', 'Mower/ Trimmer',
        'Pest control', 'Roofer', 'Structural engineer', 'Mechanic',
        'Master electrician', 'Journeyman electrician', 'Network/IT/SCADA',
        'Inspector', 'Inverter specialist', 'PV module/array Specialist',
        'Utilities locator'
    ]
    ServiceProvider = apps.get_model("costmodel", "ServiceProvider")
    service_provider_instance_list = [
        ServiceProvider(name=sp, scope=sp, qualifications=sp) for sp in service_provider_list
    ]
    ServiceProvider.objects.bulk_create(service_provider_instance_list)

    ## For FailureDistributionType ##
    failure_dist_list = [
        'Interval', 'Weibull', 'Log Normal',
    ]

    FailureDistributionType = apps.get_model("costmodel", "FailureDistributionType")

    failure_dist_instance_list = [
        FailureDistributionType(name=fd, description=fd) for fd in failure_dist_list
    ]

    FailureDistributionType.objects.bulk_create(failure_dist_instance_list)  

def reverse_func(apps, schema_editor):
    ## For OMType ##
    OMType = apps.get_model("costmodel", "OMType")
    om_type_list = [
        'Administrative', 'Corrective', 'Preventative'
    ]

    for om in omm_type_list:
        OMType.objects.filter(name=om, description=om).delete()

    ## For ServiceType ##
    service_type_list = [
        'Cleaning', 'Emergency Response', 'Inspection',
        'Management', 'Repair', 'General Maintenance', 'Testing'
    ]

    ServiceType = apps.get_model("costmodel", "ServiceType")
    for st in service_type_list:
        ServiceType.objects.filter(name=st, description=st).delete()

    ## For AssetType ##
    AssetType = apps.get_model("costmodel", "AssetType")
    asset_type_list = [
        'Asset Management', 'AC Wiring', 'DC Wiring',
        'Documents', 'Electrical', 'Mechanical', 'Inverter',
        'Meter', 'Monitoring System', 'Mounting Structure',
        'PV Module', 'Roof', 'Tracker', 'Transformer', 'PV System'
    ]
    for at in asset_type_list:
        AssetType.objects.filter(name=at, description=at).delete()

    ## For ApplicableUnit ##
    ApplicableUnit = apps.get_model("costmodel", "ApplicableUnit")
    applicable_unit_list = [
        'System', 'Inverter', 'Site', 'Disconnect Box',
        'Combiner Box', 'Rail/ Fastener', 'PV Module', 'Structure',
        'NCU', 'Connection', 'Strings', 'Acre', 'Weather Station',
        'Controller', 'Motor', 'Transformer', 'Driveshaft', 'Block',
        'Slew Gear', 'Row', 'Percent of Initial Cost', 'MW'
    ]
    for au in applicable_unit_list:
        ApplicableUnit.objects.filter(name=au, description=au).delete()

    ## For WarrantyType ##
    WarrantyType = apps.get_model("costmodel", "WarrantyType")
    warranty_type_list = [
        'EPC', 'Inverter', 'Module (Product)',
        'Monitoring', 'N/A'
    ]
    for wt in warranty_type_list:
        WarrantyType.objects.filter(name=wt, description=wt, years=0).delete()

    ## For ServiceProvider ##
    ServiceProvider = apps.get_model("costmodel", "ServiceProvider")
    service_provider_list = [
        'Administrator', 'Designer', 'Cleaner', 'Mower/ Trimmer',
        'Pest control', 'Roofer', 'Structural engineer', 'Mechanic',
        'Master electrician', 'Journeyman electrician', 'Network/IT/SCADA',
        'Inspector', 'Inverter specialist', 'PV module/array Specialist',
        'Utilities locator'
    ]
    for sp in service_provider_list:
        ServiceProvider.objects.filter(name=sp, scope=sp, qualifications=sp).delete()

    ## For FailureDistributionType ##
    FailureDistributionType = apps.get_model("costmodel", "FailureDistributionType")
    failure_dist_list = [
        'Interval', 'Weibull', 'Log Normal',
    ]
    for fd in failure_dist_list:
        FailureDistributionType.objects.filter(name=fd, description=fd).delete()


class Migration(migrations.Migration):

    dependencies = [
        ('costmodel', '0001_initial'),
    ]

    operations = [
        migrations.RunPython(forwards_func, reverse_func),
    ]
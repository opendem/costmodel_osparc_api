import os

from celery import Celery


PROJECT_NAME = os.path.basename(os.getcwd())


os.environ.setdefault('DJANGO_SETTINGS_MODULE', '{}.{}'.format(PROJECT_NAME, 'settings'))

app = Celery(os.path.basename(os.getcwd()))
app.config_from_object('django.conf:settings')

app.autodiscover_tasks()


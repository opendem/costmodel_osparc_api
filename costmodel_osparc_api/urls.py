"""costmodel_osparc_api URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import include, url
from django.contrib import admin
from django.contrib.auth.decorators import login_required, permission_required, user_passes_test


def has_license(user, version):
    print("user.organization.osparc_license: "+user.organization.osparc_license)
    if user.organization.osparc_license:
        return True
    return False

urlpatterns = [
    url(r'^admin/', admin.site.urls),

    url(r'^api/(?P<version>(v1))/', include('core.urls')),
    url(r'^api/(?P<version>(v1))/', include('costmodel.urls')),
    url(r'^api/(?P<version>(v1))/', include('osparc.urls')),
    # url(r'^cmapi/(?P<version>(v1))/', permission_required(lambda u: u.organization.cost_model_license, include('costmodel.urls'))),
    # url(r'^api/', permission_required(has_license, include('osparc.urls'))),

]

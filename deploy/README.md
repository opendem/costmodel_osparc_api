# Production Servers Provisioning

 * uses ansible to provision one or multliple servers
 * runs provision remotely from a control machine
 * allows easy update of db and repository credentials using `group_vars/all`.

Follow [this guide](http://docs.ansible.com/ansible/latest/intro_installation.html) to install [ansible]() on your control machine.


### Update hosts

This directory contains a `hosts` file containing host names of the server(s) that you may want to provision.

Since ansible uses `ssh` to access remote servers and remotely perform commands on them. It is recommended that you add ssh configuration to your `.ssh/config` file.

Below is a sample host configuration for a virtual machine.

```shell
Host default
  HostName 127.0.0.1
  User ubuntu
  Port 2222
  UserKnownHostsFile /dev/null
  StrictHostKeyChecking no
  PasswordAuthentication no
  IdentityFile /home/user/costmodel_osparc_api/vagrant/.vagrant/machines/default/virtualbox/private_key
  IdentitiesOnly yes
  LogLevel FATAL
```

For an ec2 instance, a very basic host configuration may look as follows:
```shell
Host sunspec_staging
     HostName 18.220.143.240
     User ubuntu
     IdentityFile /home/user/ec2-Server-Key.pem
```

After having updated `~/.ssh/config` you can run following commands to ensure everything is working correctly.

```shell
ssh <host>
```

In our examples, we've defined two hosts `default` and `sunspec_staging`. So, command for `sunspec_staging` would look like:

```shell
ssh sunspec_staging
```

`hosts` must use the same names as defined next to `Host` directive in the `~/.ssh/config` file.

### Database and Code Repository Credentials

Database and code repository credentials must be updated in `group_vars/all` file before running the playbook.


### Provisioning

To provision the servers, run following command while in `deploy` working directory:

```shell
ansible-playbook site.yml
```

The provisioning will be interrupted if an error occurs and the debug message will include details to further debug and fix the errors.

If there are no errors your server will be ready to serve requests!

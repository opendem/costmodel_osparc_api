# Asset Performance Suite

The project is divided in two parts.

1. [Web Service](https://github.com/sunspec/costmodel_osparc_api) - this project.
2. [Web Application](https://github.com/sunspec/costmodel_osparc_app).

```note
You should have access to both repositories given above in order to setup environment for this AP suite.
```

For more information on the project, refer to the design documents as given below:

* [PV CostModel]()
* [oSPARC]()



### Web Service

Web service exposes RESTful interfaces to create and manage resources supported by AP Suite.

It is built as a Django project and has three django apps:

1. core - includes models and interfaces common to both costmodel's and osparc's design scope.
2. costmodel - includes models and interfaces related only to PV CostModel. 
3. osparc - includes models and interfaces related only to oSPARC.

#### Database

Webservice uses Amazon redshift - amazon powered mysql based hosted database solution for its database in the production setup.

#### Background Task Processing

The webservice schedules background tasks for long running task like import wizards and report generations. It uses `celery` with `rabbitmq` as the broker to run and manage background tasks.


### Web Application

Web application is also a Django project and similarly structured with three django apps - one each for `core`, `costmodel` and `osparc` feature sets.

No new models are defined on the Web Application and it primarily is the front-end web interface built on top of the web service.


### Local Environment Setup

The easiest way to setup a local environment is to run a Vagrant machine as described [here](https://github.com/sunspec/costmodel_osparc_api/blob/master/vagrant/README.md) and provision it with ansible following the directions [here](https://github.com/sunspec/costmodel_osparc_api/blob/master/deploy/README.md).

### Security

* An nginx reverse proxy server sits at the top and delegates requests to gunicorn web service and web app servers.
* fail2ban filters clients resulting in 404 http responses and blacklist them if they exceed five 404 response within one minute time.

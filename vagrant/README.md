# Vagrant Virtual Machine Setup

Usage:

 * Replicate production environment on a vagrant vm on your local machine.
 * Verify/validate playbooks
 * configuration testing and debugging

Complete [Vagrant](https://www.vagrantup.com/docs/installation/) installation before proceeding with the following steps.

Also, install [VirtualBox](https://www.virtualbox.org/) as Vagrant uses it to create virtual machines. `v.5.0` is the preferred version.


### Configure Vagrant machine

Once VirtualBox and Vagrant are installed successfully, run the following command to download the [ubuntu 16.04]() image.

```shell
vagrant box add ubuntu/xenial64
```

Change `directory` to the `vagrant` folder located at [project root](https://github.com/sunspec/costmodel_osparc_api) and run following command to boot the vm.

```shell
vagrant up
```

Run the following command to login to the booted vm.

```shell
vagrant ssh
```

That's it! your vm is up and running and you just verified it.

### Update ssh-config

On the host machine, run the following command to add vm host to the user's ssh configuration.

```shell
vagrant ssh-config >> ~/.ssh/config
```

Output of `vagrant ssh-config` looks as follows:

```shell
Host default
  HostName 127.0.0.1
  User ubuntu
  Port 2222
  UserKnownHostsFile /dev/null
  StrictHostKeyChecking no
  PasswordAuthentication no
  IdentityFile /home/user/costmodel_osparc_api/vagrant/.vagrant/machines/default/virtualbox/private_key
  IdentitiesOnly yes
  LogLevel FATAL
```

### Provision VM with ansible

To provision the VM with ansible, follow directions [here](https://github.com/sunspec/costmodel_osparc_api/blob/master/deploy/README.md).

DBUSER=root
DBHOST=localhost
DBNAME=costmodel_osparc
FIXTUREFILE=fixtures/db.json

CREATE_USER_COMMAND="from core.models import User;\
	User.objects.filter(email='foo@bar.com').delete();\
	User.objects.create_superuser('foo@bar.com', 'test')"


staging: new_database migrate create_superuser
	@echo "\033[1;32mStaging environment provisioned successfully!\033[0m"


new_database: del_database
	@echo "Created database $(DBNAME)"
	@mysql -u$(DBUSER) -p$(DBPASS) -h$(DBHOST) \
		--execute "CREATE DATABASE costmodel_osparc;"


del_database:
	@echo "\033[1;31mDropped database $(DBNAME)"
	@mysql -u$(DBUSER) -p$(DBPASS) -h$(DBHOST) \
		--execute "DROP DATABASE costmodel_osparc;"


make_migrations:
	@echo "\033[1;34mMaking migrations"
	python manage.py makemigrations


migrate: make_migrations
	@echo "\033[1;34mApplying migrations"
	python manage.py migrate


prune_database:
	@mysql -u$(DBUSER) -p$(DBPASS) -h$(DBHOST) $(DBNAME) --execute "\
		DELETE FROM costmodel_applicableunit;\
		DELETE FROM core_pvmarketsector;\
		DELETE FROM core_moduletype;\
	"


load_data_into_app: prune_database
	python manage.py loaddata $(FIXTUREFILE)


create_superuser: load_data_into_app
	@echo $(CREATE_USER_COMMAND) | python manage.py shell
	@echo "\033[1;34mCreated user: foo@bar.com pass: test successfully."


kill_old_processes:
	@echo "\033[1;31mKilling already running processes"
	@echo ps axf | tr -s ' ' | cut -d ' ' -f1,6- | grep "gunicorn costmodel_osparc" | \
	cut -d ' ' -f1 | while read line; do echo "Killing $line"; kill -9 $line; done


clean:
	find . -name "*.pyc" -delete;
	@echo "Removed all *pyc files"

from __future__ import unicode_literals
from django.db import models
from django.utils import timezone


class UploadActivity(models.Model):
    plant_upload_time = models.DateTimeField(auto_now_add=True)
    most_recent_time_series_upload_time = models.DateTimeField(blank=True, null=True)
    status = models.CharField(max_length=16)
    error_detail = models.CharField(max_length=1024, blank=True, null=True)


# Production Statistics pertaining to a single plant over a specific period (often its lifetime)
# Used in the construction of instances of ReportRuns
class PlantReport(models.Model):
    record_status = models.IntegerField(default=9)           # RECORD_STATUS_RECALCULATE
    create_time = models.DateTimeField(auto_now_add=True)    # time the report was created
    sample_interval = models.CharField(max_length=64, blank=True, null=True)
    first_measurement_date = models.DateField(blank=True, null=True)
    last_measurement_date = models.DateField(blank=True, null=True)
    monthly_yield = models.FloatField(blank=True, null=True)    # production yield kWh/kWdc
    performance_ratio = models.FloatField(blank=True, null=True)    # performance ratio yf/yr
    storage_state_of_health = models.FloatField(blank=True, null=True)   # storage state of health


class PlantTimeSeries(models.Model):
    time_stamp = models.DateTimeField()
    sample_interval = models.IntegerField()
    WH_DIFF = models.FloatField()           # kWh since last entry
    GHI_DIFF = models.FloatField(blank=True, null=True)
    TMPAMB_AVG = models.FloatField(blank=True, null=True)
    # from PVArrayTimeSeries
    HPOA_DIFF = models.FloatField(blank=True, null=True)
    plant = models.ForeignKey('core.Plant', on_delete=models.CASCADE, blank=True, null=True)
    record_status = models.IntegerField(default=1)  # RECORD_STATUS_ACTIVE


class Total(models.Model):
    dc_rating = models.FloatField(blank=True, null=True)
    storage_original_capacity = models.FloatField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'osparc_total'


#  =========   REPORTS   ===========

# Custom Query Report definition
# The database query used to generate the result is built on the fly from this definition
class ReportDefinition(models.Model):
    name = models.CharField(max_length=45)
    description = models.CharField(max_length=254, blank=True, null=True)
    observation_start_date = models.DateField(blank=True, null=True)  # time plant observation started
    observation_end_date = models.DateField(blank=True, null=True)  # time plant observation ended
    plant_filter_attribute = models.CharField(max_length=254, blank=True, null=True)
    plant_filter_operation = models.CharField(max_length=254, blank=True, null=True)
    plant_filter_value = models.CharField(max_length=254, blank=True, null=True)
    organization = models.ForeignKey('core.Organization', on_delete=models.CASCADE)

    def __str__(self):
        return self.name


# The results of a run of a query
class ReportRun(models.Model):
    status = models.IntegerField(default=2)  # 1=ready, 2=pending, 5=processing, 6=failed, 9=empty
    report_definition = models.ForeignKey(ReportDefinition)
    run_submit_time = models.DateTimeField(auto_now_add=True)  # time user ordered the report
    run_start_time = models.DateTimeField(blank=True, null=True)  # time report preparation actually began
    run_complete_time = models.DateTimeField(blank=True, null=True)  # time report preparation actually completed
    first_measurement_date = models.DateField(blank=True, null=True)  # time plant observation started
    last_measurement_date = models.DateField(blank=True, null=True)  # time plant observation ended
    number_of_measurements = models.IntegerField(blank=True, null=True)
    number_of_plants = models.IntegerField(blank=True, null=True)
    total_dc_capacity = models.FloatField(blank=True, null=True)
    total_storage_capacity = models.FloatField(blank=True, null=True)
    organization = models.ForeignKey('core.Organization', on_delete=models.CASCADE)


class CustomQuery(models.Model):
    report_name = models.CharField(max_length=145)
    start_date = models.DateField(blank=True, null=True)
    end_date = models.DateField(blank=True, null=True)
    task_id = models.CharField(max_length=200)
    date = models.DateTimeField(default=timezone.now)
    organization = models.ForeignKey('core.Organization', blank=True, null=True)
    plants = models.ManyToManyField('core.Plant')
    timeseries = models.ManyToManyField('PlantTimeSeries')
    kpi_result = models.TextField(max_length=10000, blank=True, null=True)
    api_query = models.CharField(max_length=10000, blank=True, null=True)
    kpi_status = models.CharField(max_length=255, default="PENDING")


class KPI(models.Model):
    name = models.CharField(max_length=254, blank=True, null=True)
    report_run = models.ForeignKey(ReportRun, blank=True, null=True)
    number_of_plants = models.IntegerField(blank=True, null=True)
    sample_interval = models.CharField(max_length=64, default='monthly')
    first_day = models.DateField(blank=True, null=True)
    last_day = models.DateField(blank=True, null=True)
    mean = models.FloatField(blank=True, null=True)
    median = models.FloatField(blank=True, null=True)
    minimum = models.FloatField(blank=True, null=True)
    maximum = models.FloatField(blank=True, null=True)


class WeatherData(models.Model):
    start_time = models.DateTimeField(blank=True, null=True)
    observation_time = models.DateTimeField(blank=True, null=True)
    end_time = models.DateTimeField(blank=True, null=True)
    global_horizontal_irradiance = models.FloatField(blank=True, null=True)
    direct_normal_irradiance = models.FloatField(blank=True, null=True)
    diffuse_horizontal_irradiance = models.FloatField(blank=True, null=True)
    irradiance_observation_type = models.CharField(max_length=255, blank=True, null=True)
    ambient_temperature = models.FloatField(blank=True, null=True)
    ambient_temperature_observation_type = models.CharField(max_length=255, blank=True, null=True)
    wind_speed = models.FloatField(blank=True, null=True)
    wind_speed_observation_type = models.CharField(max_length=255, blank=True, null=True)
    relative_humidity = models.FloatField(blank=True, null=True)
    liquid_precipitation = models.FloatField(blank=True, null=True)
    solid_precipitation = models.FloatField(blank=True, null=True)
    snow_depth = models.FloatField(blank=True, null=True)
    plant = models.ForeignKey('core.Plant', on_delete=models.CASCADE, blank=True, null=True)


class SingletonModel(models.Model):

    class Meta:
        abstract = True

    def save(self, *args, **kwds):
        self.pk = 1
        super(SingletonModel, self).save(*args, **kwds)

    def delete(self, *args, **kwds):
        # prevents from deleting the object accidently
        pass

    @classmethod
    def load(cls):
        obj, created = cls.objects.get_or_create(pk=1)
        return obj


class OsparcDashboard(SingletonModel):
    dashboard_objects = models.TextField()

#to what extent can we get insolation data
#how are we gonnna generate the data that we need for osparc
#monthly for osparc

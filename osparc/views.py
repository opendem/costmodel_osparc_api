import collections
import datetime
import json
import dateparser

from django.db import connection
from django.db.models import Q, Sum, Count
from celery.result import AsyncResult
from rest_framework import viewsets
from rest_framework import response, schemas
from rest_framework import generics
from rest_framework.decorators import api_view, renderer_classes
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework_swagger.renderers import OpenAPIRenderer, SwaggerUIRenderer
from serializers import *
from .mixins import KPIs
from core import models as core_models
from core import serializers as core_models_serializers
from core import filters as core_models_filters
from costmodel import models as cost_models
from filters import *
from core.mixins import DepthSerializerMixin
from core.permissions import *
from osparc.tasks import calculate_kpis, calculate_dashboard_kpis, create_solar_anywhere_data
from django_celery_results.models import TaskResult
from django.core.exceptions import ObjectDoesNotExist
from core.pagination import DefaultPageNumberPagination


class UploadActivityList(DepthSerializerMixin, generics.ListCreateAPIView):
    queryset = UploadActivity.objects.all()
    serializer_class = UploadActivitySerializer
    filter_class = UploadActivityFilter
    permission_classes = (permissions.IsAuthenticated, UserPermissions, HasOsparcLicense)


class UploadActivityDetail(DepthSerializerMixin, generics.RetrieveUpdateDestroyAPIView):
    queryset = UploadActivity.objects.all()
    serializer_class = UploadActivitySerializer
    permission_classes = (permissions.IsAuthenticated, UserPermissions, HasOsparcLicense)


class PlantTimeSeriesList(DepthSerializerMixin, generics.ListCreateAPIView):
    queryset = PlantTimeSeries.objects.all()
    serializer_class = PlantTimeSeriesSerializer
    filter_class = PlantTimeSeriesFilter
    permission_classes = (permissions.IsAuthenticated, DataProviderPermissions, HasOsparcLicense)
    #pagination_class = DefaultPageNumberPagination

    def get_queryset(self):
        # org_pk = self.kwargs['org_pk']
        plant_pk = self.kwargs['plant_pk']
        queryset = self.queryset.filter(plant=plant_pk)
        return queryset

    # override create in order to update the uploadActivity entry
    def perform_create(self, serializer):

        instance = serializer.save()

        # Find the plant associated with this timeseries element.
        # If the timeseries element's plant attribute is set, it is the db id
        # of the plant. If not, then it is specified by the timeseries element's uuid.
        # That is the case of importing from oSPARC v1, where the timeseries' plant
        # attribute is not imported because the db id of the plant is likely different
        # in oSPARC v2.
        # When a new timeseries element is added, the plant's PlantReport must be
        # re-generated (since there is new data). We invalidate it here if it exists,
        # and it is recalculated asynchronously.

        try:
            if instance.plant is not None and instance.plant > 0:
                plant = core_models.Plant.objects.get(id=instance.plant_id)
            else:
                plant = core_models.Plant.objects.get(uuid=instance.plant_uuid)

                instance.plant = plant
                if serializer.is_valid():
                    serializer.save()
                else:
                    print serializer.errors

            # here, we have identified the plant

            # (1) update the upload activity
            try:
                ua = UploadActivity.objects.get(id=plant.upload_activity_id)
                ua.most_recent_time_series_upload_time = datetime.datetime.now()
                ua.status = 'success'
                ua.save()

            except:
                print "ERROR updating UA"

            # (2) invalidate the plant report
            try:
                report = plant.plant_report
                report.record_status = 9  # invalid
                newser = PlantReportSerializer(report, data=report.__dict__)
                if newser.is_valid():
                    newser.save()
                else:
                    print "ERROR invalidating PlantReport: errors:", newser.errors
            except:
                print "ERROR invalidating PlantReport"

        except:
            print "Unable to locate plant for time series %s" % instance.timestamp
            return


class PlantTimeSeriesDetail(DepthSerializerMixin, generics.RetrieveUpdateDestroyAPIView):
    queryset = PlantTimeSeries.objects.all()
    serializer_class = PlantTimeSeriesSerializer
    permission_classes = (permissions.IsAuthenticated, HasOsparcLicense, DataProviderPermissions)

    def get_queryset(self):
        # org_pk = self.kwargs['org_pk']
        plant_pk = self.kwargs['plant_pk']
        queryset = self.queryset.filter(plant=plant_pk)
        return queryset


# class PlantTimeSeriesViewSet(viewsets.ModelViewSet):
#     queryset = PlantTimeSeries.objects.all()
#     serializer_class = PlantTimeSeriesSerializer
#
#     # override create in order to update the uploadActivity entry
#     def perform_create(self, serializer):
#
#         instance = serializer.save()
#
#         # Find the plant associated with this timeseries element.
#         # If the timeseries element's plant attribute is set, it is the db id
#         # of the plant. If not, then it is specified by the timeseries element's uuid.
#         # That is the case of importing from oSPARC v1, where the timeseries' plant
#         # attribute is not imported because the db id of the plant is likely different
#         # in oSPARC v2.
#         # When a new timeseries element is added, the plant's PlantReport must be
#         # re-generated (since there is new data). We invalidate it here if it exists,
#         # and it is recalculated asynchronously.
#
#         try:
#             if instance.plant != None and instance.plant > 0:
#                 plant = core_models.Plant.objects.get(id=instance.plant_id)
#             else:
#                 plant = core_models.Plant.objects.get(uuid=instance.plantUUID)
#
#                 instance.plant = plant
#                 if serializer.is_valid():
#                     serializer.save()
#                 else:
#                     print serializer.errors
#
#             # here, we have identified the plant
#
#             # (1) update the uploadactivity
#             try:
#                 ua = UploadActivity.objects.get(id=plant.uploadactivity_id)
#                 ua.most_recent_time_series_upload_time = datetime.datetime.now()
#                 ua.status = 'success'
#                 ua.save()
#
#             except:
#                 print "ERROR updating UA"
#
#             # (2) invalidate the plantreport
#             try:
#                 report = plant.plantreport
#                 report.record_status = 9  # invalid
#                 newser = PlantReportSerializer(report, data=report.__dict__)
#                 if newser.is_valid():
#                     newser.save()
#                 else:
#                     print "ERROR invalidating PlantReport: errors:", newser.errors
#             except:
#                 print "ERROR invalidating PlantReport"
#
#         except:
#             print "Unable to locate plant for timeseries %s" % instance.timestamp
#             return


class KPIViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = KPI.objects.all()
    serializer_class = KPISerializer
    filter_class = KPIFilter
    permission_classes = (permissions.IsAuthenticated, HasOsparcLicense, UserPermissions)


class KPIView(generics.ListAPIView):
    filter_class = KPIFilter
    permission_classes = (permissions.IsAuthenticated, HasOsparcLicense, UserPermissions)

    def list(self, request):
        queryset = KPI.objects.all()
        serializer = KPISerializer(queryset, many=True)
        return Response(serializer.data)


class PlantReportDetail(DepthSerializerMixin, generics.RetrieveUpdateDestroyAPIView):
    queryset = PlantReport.objects.all()
    serializer_class = PlantReportSerializer
    permission_classes = (permissions.IsAuthenticated, HasOsparcLicense, UserPermissions)


# queries & reports
class ReportDefinitionList(DepthSerializerMixin, generics.ListCreateAPIView):
    queryset = ReportDefinition.objects.all()
    serializer_class = ReportDefinitionSerializer
    permission_classes = (permissions.IsAuthenticated, HasOsparcLicense, UserPermissions)
    filter_class = ReportDefinitionFilter

    def get_queryset(self):
        org_pk = self.kwargs['org_pk']
        return self.queryset.filter(organization=org_pk)

    # Start here Tuesday, create post method for data


class ReportDefinitionDetail(DepthSerializerMixin, generics.RetrieveUpdateDestroyAPIView):
    queryset = ReportDefinition.objects.all()
    serializer_class = ReportDefinitionSerializer
    permission_classes = (permissions.IsAuthenticated, HasOsparcLicense, UserPermissions)

    def get_queryset(self):
        org_pk = self.kwargs['org_pk']
        return self.queryset.filter(organization=org_pk)


class ReportRunList(DepthSerializerMixin, generics.ListCreateAPIView):
    queryset = ReportRun.objects.all()
    serializer_class = ReportRunSerializer
    filter_class = ReportRunFilter
    permission_classes = (permissions.IsAuthenticated, HasOsparcLicense, UserPermissions)

    def get_queryset(self):
        org_pk = self.kwargs['org_pk']
        return self.queryset.filter(organization=org_pk)


class ReportRunDetail(DepthSerializerMixin, generics.RetrieveUpdateDestroyAPIView):
    queryset = ReportRun.objects.all()
    serializer_class = ReportRunSerializer
    permission_classes = (permissions.IsAuthenticated, HasOsparcLicense, UserPermissions)

    def get_queryset(self):
        org_pk = self.kwargs['org_pk']
        return self.queryset.filter(organization=org_pk)


class ListPlantsForGeoChart(APIView):
    permission_classes = (
        permissions.IsAuthenticated, HasOsparcLicense, UserPermissions
    )

    def get(self, request, format=None, **kwargs):
        plants = core_models.Plant.objects.all()
        states = collections.Counter([p.state.name for p in plants])
        data = [{"state": k, "plantCount": v} for k, v in states.items()]
        return Response(data)


# stats
class AggregatesView(APIView):
    permission_classes = (permissions.IsAuthenticated, HasOsparcLicense, UserPermissions)
    def plantsByState(self, plants, version):
        result = collections.defaultdict(int)
        for plant in plants:
            result[plant.state] += 1
        return result

    def plantsByYear(self, plants, version):
        result = collections.defaultdict(int)
        for plant in plants:
            adate = plant.activationdate
            result[adate.year] += 1
        return result

    def plantsByYearAndDcrating(self, plants, version):
        # there are 12 years and 5 dcrating buckets
        result = collections.defaultdict(dict)
        result['2007']['<10kW'] = 0
        result['2007']['10-100kW'] = 0
        result['2007']['100kW-1MW'] = 0
        result['2007']['1-10MW'] = 0
        result['2007']['>10MW'] = 0
        result['2008']['<10kW'] = 0
        result['2008']['10-100kW'] = 0
        result['2008']['100kW-1MW'] = 0
        result['2008']['1-10MW'] = 0
        result['2008']['>10MW'] = 0
        result['2009']['<10kW'] = 0
        result['2009']['10-100kW'] = 0
        result['2009']['100kW-1MW'] = 0
        result['2009']['1-10MW'] = 0
        result['2009']['>10MW'] = 0
        result['2010']['<10kW'] = 0
        result['2010']['10-100kW'] = 0
        result['2010']['100kW-1MW'] = 0
        result['2010']['1-10MW'] = 0
        result['2010']['>10MW'] = 0
        result['2011']['<10kW'] = 0
        result['2011']['10-100kW'] = 0
        result['2011']['100kW-1MW'] = 0
        result['2011']['1-10MW'] = 0
        result['2011']['>10MW'] = 0
        result['2012']['<10kW'] = 0
        result['2012']['10-100kW'] = 0
        result['2012']['100kW-1MW'] = 0
        result['2012']['1-10MW'] = 0
        result['2012']['>10MW'] = 0
        result['2013']['<10kW'] = 0
        result['2013']['10-100kW'] = 0
        result['2013']['100kW-1MW'] = 0
        result['2013']['1-10MW'] = 0
        result['2013']['>10MW'] = 0
        result['2014']['<10kW'] = 0
        result['2014']['10-100kW'] = 0
        result['2014']['100kW-1MW'] = 0
        result['2014']['1-10MW'] = 0
        result['2014']['>10MW'] = 0
        result['2015']['<10kW'] = 0
        result['2015']['10-100kW'] = 0
        result['2015']['100kW-1MW'] = 0
        result['2015']['1-10MW'] = 0
        result['2015']['>10MW'] = 0
        result['2016']['<10kW'] = 0
        result['2016']['10-100kW'] = 0
        result['2016']['100kW-1MW'] = 0
        result['2016']['1-10MW'] = 0
        result['2016']['>10MW'] = 0
        result['2017']['<10kW'] = 0
        result['2017']['10-100kW'] = 0
        result['2017']['100kW-1MW'] = 0
        result['2017']['1-10MW'] = 0
        result['2017']['>10MW'] = 0
        result['2018']['<10kW'] = 0
        result['2018']['10-100kW'] = 0
        result['2018']['100kW-1MW'] = 0
        result['2018']['1-10MW'] = 0
        result['2018']['>10MW'] = 0
        for plant in plants:
            if plant.dcrating is not None:
                adate = plant.activationdate
                for yearBucket in [2007, 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015, 2016, 2017, 2018]:
                    if adate.year == yearBucket:
                        # for rangeBucket in [[0,10],[10,100],[100,1000],[1000,10000],[10000,sys.maxint]:
                        if plant.dcrating / 1000 < 10:
                            result[str(yearBucket)]['<10kW'] += 1
                        elif plant.dcrating / 1000 < 100:
                            result[str(yearBucket)]['10-100kW'] += 1
                        elif plant.dcrating / 1000 < 1000:
                            result[str(yearBucket)]['100kW-1MW'] += 1
                        elif plant.dcrating / 1000 < 10000:
                            result[str(yearBucket)]['1-10MW'] += 1
                        else:
                            result[str(yearBucket)]['>10MW'] += 1
        return result

    def totals(self, version):
        result = collections.defaultdict(dict)

        result['count'] = core_models.Plant.objects.count()

        # osparc_total is a view containing the sums of dcrating and storageorigcapacity.
        # The following abomination is required to read views. As if that were not abominable
        # enough, you have to create the view manually, separate from makemigrations.
        # There's a good chance I'll replace this with a table that I populate in code whenever
        # a plant is added..
        cursor = connection.cursor()
        sql_string = 'SELECT * FROM osparc_total'
        cursor.execute(sql_string)
        dbRes = cursor.fetchall()
        result['dc_rating'] = dbRes[0][0]
        result['storage_capacity'] = dbRes[0][1]

        return result

    def get(self, request, version, format=None):

        #  count, dcrating
        result = dict(AggregatesView.totals(self, version))

        #  kpis
        result['kpis'] = KPISerializer(KPI.objects.all(), context={'request': request}, many=True).data

        # wacky groupings if needed

        queries = dict(request.query_params.iterlists())

        if 'by' in request.query_params:
            year = False
            dc = False
            state = False
            by = queries['by']
            if 'year' in by:
                year = True
            if 'dcrating' in by:
                dc = True
            if 'state' in by:
                state = True

            print("year=%s,dc=%s,state=%s" % (year, dc, state))

            if year is False and dc is False and state is False:
                return Response(
                    {"I don't understand the query string": dict(request.query_params.iterlists()).keys()[0]})

            plants = core_models.Plant.objects.all()

            if state is True:
                result['bystate'] = AggregatesView.plantsByState(self, plants)

            if year is True and dc is False:
                result['byyear'] = AggregatesView.plantsByYear(self, plants)

            if year is True and dc is True:
                result['byyearanddcrating'] = AggregatesView.plantsByYearAndDcrating(self, plants)

        return Response(result)


# KPIs
class KPIsView(APIView):
    permission_classes = (permissions.IsAuthenticated, HasOsparcLicense, UserPermissions)

    def create(self, validated_data, version):
        return KPI.objects.create(**validated_data)

    def get(self, request, version, format=None):

        def create_task():
            cq = CustomQuery.objects.create(
                report_name="Osparc Dashboard Custom Query"
            )
            task_id = calculate_dashboard_kpis.delay(cq.id)

            cq.task_id = task_id
            cq.save()

            return Response({'status': 'Scheduled KPI calculation task.'})

        try:
            cq = CustomQuery.objects.get(report_name="Osparc Dashboard Custom Query")
            if not cq.kpi_result:
                if 'failed' in AsyncResult(cq.task_id).state.lower():
                    cq.delete()
                    return create_task()
                else:
                    return Response({'status': 'Task already executed - pending result'})
            else:
                return Response({'result': cq.kpi_result, 'status': cq.kpi_status})

        except ObjectDoesNotExist:
            return create_task()


class KPIsSinglePlantView(APIView):
    permission_classes = (
        permissions.IsAuthenticated, HasOsparcLicense, UserPermissions
    )

    def get(self, request, plant_pk, version, format=None):
        plant_id = plant_pk

        plants = []
        plant = core_models.Plant.objects.get(id=plant_id)
        plants.append(plant)
        timeseries = PlantTimeSeries.objects.filter(plant=plant.id)
        mixin = KPIs()
        kpis = mixin.calculateKPIs(plants, timeseries)

        return Response(kpis)


class KPIsMultiplePlantSelection(APIView):
    permission_classes = (
        permissions.IsAuthenticated, HasOsparcLicense, UserPermissions
    )

    def post(self, request, version, format=None):

        # filter plants by given criteria
        plant_filters = json.loads(request.POST['plant_filters'])
        for k, v in plant_filters.items():
            if not v:
                plant_filters.pop(k)
            try:
                plant_filters[k] = int(v)  # cast integers (foreign keys)
            except:
                pass

        plants_qs = core_models.Plant.objects.filter(**plant_filters)
        if not plants_qs:
            return Response({
                'status': 'No plants match your selection criteria'
                          ', please try a different query.'
            })

        timeseries_filters = {
            'time_stamp__gte': request.POST['start_time'],
            'time_stamp__lte': request.POST['end_time'],
            'plant__in': plants_qs
        }

        timeseries_qs = PlantTimeSeries.objects.filter(**timeseries_filters)
        timeseries_filters.pop('plant__in')

        if not timeseries_qs:
            return Response({
                'status': 'Matched plants do not have any timeseries '
                          'information. Please try a different query.'
            })

        cq_id = request.POST.get('cq_id', False)
        if cq_id:
            cq = CustomQuery.objects.get(id=cq_id)
            cq.kpi_status = 'PENDING'
            cq.kpi_result = ''
            cq.date = datetime.datetime.utcnow()
        else:
            cq = CustomQuery.objects.create(
                report_name=request.POST['report_name'],
                start_date=dateparser.parse(request.POST['start_time']).date(),
                end_date=dateparser.parse(request.POST['end_time']).date(),
                organization=core_models.Organization.objects.get(pk=request.POST['org_id']),
                api_query=json.dumps(plant_filters),
                kpi_result="",
            )

        task_id = calculate_kpis.delay(
            [], [], request.user.email,
            cq.id, request.POST.get('email_desired'),
            plant_filters=plant_filters, timeseries_filters=timeseries_filters
        )

        cq.task_id = task_id
        cq.save()

        return Response({
            'status': 'Request successfully scheduled, we will notify you'
                      ' by email once it is finished.'
        })


class WeatherDataList(DepthSerializerMixin, generics.ListCreateAPIView):
    queryset = WeatherData.objects.all()
    serializer_class = WeatherDataSerializer
    filter_class = WeatherDataFilter
    permission_classes = (permissions.IsAuthenticated, DataProviderPermissions, HasOsparcLicense)

    def get_queryset(self):
        # org_pk = self.kwargs['org_pk']
        plant_pk = self.kwargs['plant_pk']
        queryset = self.queryset.filter(plant=plant_pk)
        return queryset


    def perform_create(self, serializer):

        instance = serializer.save()
        try:
            if instance.plant is not None and instance.plant > 0:
                plant = core_models.Plant.objects.get(id=instance.plant_id)
            else:
                plant = core_models.Plant.objects.get(uuid=instance.plant_uuid)

                instance.plant = plant
                if serializer.is_valid():
                    serializer.save()
                else:
                    print serializer.errors

        except:
            print "Unable to locate plant for weather data %s" % instance.timestamp
            return


class CustomQueryList(DepthSerializerMixin, generics.ListCreateAPIView):
    queryset = CustomQuery.objects.all()
    serializer_class = CustomQuerySerializer
    filter_class = CustomQueryFilter
    permission_classes = (
        permissions.IsAuthenticated, DataProviderPermissions, HasOsparcLicense
    )

    def get_queryset(self):
        org_pk = self.kwargs['org_pk']
        #pk = self.kwargs['pk']
        queryset = self.queryset.filter(organization=org_pk)
        return queryset


    def perform_create(self, serializer):

        instance = serializer.save()
        try:
            if instance.organization is not None and instance.organization > 0:
                organization = core_models.Organization.objects.get(
                    id=instance.organization_id
                )
            else:
                organization = core_models.Organization.objects.get(name=instance.organization_name)

                instance.organization = organization
                if serializer.is_valid():
                    serializer.save()
                else:
                    print serializer.errors

        except:
            print "Unable to locate organization for custom query create %s" % instance.timestamp
            return


class CustomQueryDetail(DepthSerializerMixin, generics.RetrieveUpdateDestroyAPIView):
    queryset = CustomQuery.objects.all()
    serializer_class = CustomQuerySerializer
    filter_class = CustomQueryFilter
    permission_classes = (
        permissions.IsAuthenticated, DataProviderPermissions, HasOsparcLicense
    )

    def get_queryset(self):
        pk = self.kwargs['pk']
        org_pk = self.kwargs['org_pk']
        queryset = self.queryset.filter(organization=org_pk, id=pk)
        return queryset


class AllPlantsList(DepthSerializerMixin, generics.ListCreateAPIView):

    queryset = core_models.Plant.objects.all()
    serializer_class = core_models_serializers.PlantSerializer
    permission_classes = (
        permissions.IsAuthenticated, UserPermissions
    )
    filter_class = core_models_filters.PlantFilter
    def get_queryset(self):
        queryset = core_models.Plant.objects.all()
        return queryset

@api_view()
def query_task_status(request, version, id):
    try:
        t = TaskResult.objects.get(task_id=id)
        status = t.status
        result = t.result
    except TaskResult.DoesNotExist:
        status = 'PENDING'
        result = {}

    return response.Response(
        {'id': id, 'status': status, 'result': result}
    )


@api_view()
def dashboard_stats(request, version):
    organization_id = 1
    # params from request
    # organization_id = request.GET['org_id']

    # *
    # total plant counts by all/org
    # *

    total_plant_count_all = core_models.Plant.objects.count()
    total_plant_count_org = core_models.Plant.objects.filter(
        Q(organization_id=organization_id)).count()

    # *
    # total dc_rating by all/org
    # *

    total_dc_sum_all = core_models.Plant.objects.all().aggregate(Sum('dc_rating'))
    total_dc_sum_org = core_models.Plant.objects.filter(
        Q(organization_id=organization_id)).aggregate(Sum('dc_rating'))

    # *
    # plants count by state
    # *

    # hardcoding it for U.S.
    pcxs_ann = Count('plant', filter=(Q(country__id__eq=231)))

    plants_count_by_state = [
        [st.name, st.num_plants]
        for st in cost_models.State.objects.annotate(num_plants=pcxs_ann)
    ]

    # *
    # plants count by dc rating
    # *

    def dc_range(start, end, x=10):
        """Returns members of geometric sequence between start and end limits.
           x is configurable.
        """
        yield start
        while start < end:
            start = start * x
            yield start

    year_range = range(2000, datetime.datetime.now().year + 1)
    dc_range_ = list(dc_range(10000, 100000000))

    year_dc_breakup = []

    for year in year_range:
        row = [year]
        year_filter = Q(activation_date__year=year)
        for dc in dc_range_:
            dc_filter = Q(dc_rating__lt=dc)
            row.append(
                core_models.Plant.objects.filter(
                    year_filter & dc_filter
                ).count()
            )
        year_dc_breakup.append(row)

    return Response({
        'year_dc_breakup': year_dc_breakup,
        'plants_count_by_state': plants_count_by_state,
        'total_plant_count_all': total_plant_count_all,
        'total_plant_count_org': total_plant_count_org,
        'total_dc_sum_all': total_dc_sum_all,
        'total_dc_sum_org': total_dc_sum_org,
    })


class SolarAnywhereWeatherData(APIView):
    permission_classes = (
        permissions.IsAuthenticated, HasOsparcLicense, UserPermissions
    )

    def post(self, request, version, plant_pk, format=None):
        plant_id = plant_pk

        weather_data = request.POST.get('weather_data')
        task = create_solar_anywhere_data.delay(plant_id, weather_data)

        return Response({'status': 'Successfully scheduled task'})


# swagger
@api_view()
@renderer_classes([OpenAPIRenderer, SwaggerUIRenderer])
def schema_view(request, version):
    generator = schemas.SchemaGenerator(title='oSPARC API')
    return response.Response(generator.get_schema(request=request))

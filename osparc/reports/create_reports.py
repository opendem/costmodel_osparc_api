#!/usr/bin/python

# run periodically to create custom reports.
# reads reportrun table and if records are found in the pending state, 
# runs the report and updates the table with results and status.
# Sets status to processing when it starts, ready when it finishes.

import datetime
import time
import collections
from kpis import KPIs
from dbwrapper import DbWrapper

try:
    dbwrapper = DbWrapper()
    # get the reportruns. Those with status==pending will be processed.
    runs = DbWrapper.readRun(dbwrapper)
    for run in runs:
        # determine whether to process this reportrun - do so if its status is pending
        status = run[1]
        if status == 2:		# pending
            try:
                runid = run[0]
                now = datetime.datetime.now()
                print("starting report %d at %s" % (runid, now))

                # let the system know we're working on this run
                DbWrapper.updateRunStatus(dbwrapper, runid, now, 5)
                # get the definition, which contans the plant filters
                defId = run[11]
                defi = DbWrapper.readDef(dbwrapper, defId)

                # use the plant filter from definition to get the set of plants
                #  participating in the report
                attr = defi[5]
                op = defi[6]
                value = defi[7]
                plants = DbWrapper.getPlants(dbwrapper, attr, op, value)
                total_dc_capacity = 0
                total_storage_capacity = 0

                for plant in plants:
                    if plant.dc_rating is not None:
                        total_dc_capacity += plant.dc_rating
                    if plant.storage_original_capacity is not None:
                        total_storage_capacity += plant.storage_original_capacity

                # use the time filter from the reportrun, and the set of plants
                #  retrieved above, to get the set of timeseries elements to be used
                #  to calculate the kpis for the report
                startTime = datetime.datetime.combine(defi[3], datetime.time.min)
                endTime = datetime.datetime.combine(defi[4], datetime.time.max)
                timeseries = DbWrapper.getTimeSeries(dbwrapper, plants, startTime, endTime)

                print "creating report with %d plants, %d timeseries elements" % (len(plants), len(timeseries))

                # The results are saved as a summary in the reportrun table, and a set of
                #  kpis, in the kpi table. Kpi table entries have a foreign key to the reportrun table
                #
                #  calculate and save the KPIs
                kpiObj = KPIs()
                plantKpis = KPIs.calculatePlantKPIs(kpiObj, plants)
                if plantKpis is not None:
                    DbWrapper.saveKpi(dbwrapper, runid, plantKpis['dc_rating'])
                    DbWrapper.saveKpi(dbwrapper, runid, plantKpis['storage_capacity'])
                    DbWrapper.saveKpi(dbwrapper, runid, plantKpis['storage_state_of_health'])
                timeseriesKpis = kpiObj.calculateTimeseriesKPIs(plants, timeseries)
                if timeseriesKpis is not None:
                    DbWrapper.saveKpi(dbwrapper, runid, timeseriesKpis['monthly_insolation'])
                    DbWrapper.saveKpi(dbwrapper, runid, timeseriesKpis['monthly_generated_energy'])
                    DbWrapper.saveKpi(dbwrapper, runid, timeseriesKpis['monthly_yield'])
                    DbWrapper.saveKpi(dbwrapper, runid, timeseriesKpis['performance_ratio'])

                # update the reportrun table with the summary of the run...
                summary = collections.defaultdict(int)
                summary["number_of_plants"] = len(plants)
                summary['total_dc_capacity'] = total_dc_capacity
                summary["number_of_measurements"] = len(timeseries)
                summary["first_measurement_date"] = defi[3]	 # note: get this from timeseries instead!
                summary["last_measurement_date"] = defi[4]	 # ditto
                summary['total_storage_capacity'] = total_storage_capacity
                DbWrapper.updateRunSummary(dbwrapper, runid, summary)

                # ...and the status, indicating that it's ready to be viewed
                now = datetime.datetime.now()
                DbWrapper.updateRunStatus(dbwrapper, runid, now, 1)

                print("finished report %d at %s" % (runid, datetime.datetime.now()))
            except Exception as e:
                print "ERROR processing run", runid + e.message, "exception type ", type(e)

except Exception as e:
    print "ERROR processing runs:", e.message, "exception type ", type(e)

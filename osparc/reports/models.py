# XXX TBD TODO Heinosity Alert:
# This, i.e. the script that runs reports, uses models that are very similar, but different,
# from the models used in the django web services.
# They should be combined; meanwhile, tread carefully!

class Plant:
    def __init__(self,id,actdate,dc_rating,storage_original_capacity,storage_current_capacity):
        self.id = id
        self.activation_date = actdate
        self.dc_rating = dc_rating
        self.storage_original_capacity = storage_original_capacity
        self.storage_current_capacity = storage_current_capacity

class PlantTimeSeries:
    def __init__(self,id,time_stamp,sample_interval,WH_DIFF,GHI_DIFF,TMPAMB_AVG,HPOA_DIFF,plant_id):
        self.time_stamp = time_stamp
        self.sample_interval = sample_interval
        self.WH_DIFF = WH_DIFF
        self.GHI_DIFF = GHI_DIFF
        self.TMPAMB_AVG = TMPAMB_AVG
        self.HPOA_DIFF = HPOA_DIFF
        self.plant_id = plant_id    # NB this is the id, not a foreign key to the plant (as in the web services version)

class KpiTimeseriesElement:
    def __init__(self,plantId,time_stamp,numerator,denominator):
        # print "KpiTimeseriesElement init: ",plantId,time_stamp,numerator,denominator
        self.plantId = plantId
        self.time_stamp = time_stamp
        self.value = numerator/denominator


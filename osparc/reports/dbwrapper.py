#!/usr/bin/python

# manage the database tables

import MySQLdb
import models

# XXX TBD TODO Heinosity Alert:
# This, i.e. the script that runs reports, uses models that are very similar, but different,
# from the models used in the django web services.
# They should be combined; meanwhile, tread carefully!


class DbWrapper(object):
    dbHost = ""
    dbUser = ""
    dbPwrd = ""
    dbDb = "osparc"

    def __init__(self):
        self.dbHost = "localhost"
        self.dbUser = "root"
        self.dbPwrd = "121"
        self.dbDb = "costmodel_osparc"

    def readRun(self):
        try:
            db = MySQLdb.connect(self.dbHost,self.dbUser,self.dbPwrd,self.dbDb)
            cursor = db.cursor()
            cursor.execute("select * from osparc_reportrun")
            runs = cursor.fetchall()
            db.close()
            return runs
        except Exception as e:
            print "ERROR reading reportrun:", e.message, "exception type ", type(e)

    def updateRunStatus(self, id, time, status):
        try:
            db = MySQLdb.connect(self.dbHost, self.dbUser, self.dbPwrd, self.dbDb)
            cursor = db.cursor()
            if status == 5:	# changing to processing
                query = "update osparc_reportrun set status=%d,run_start_time='%s' where id=%d" % (status, time, id)
            elif status == 1: # changing to ready
                query = "update osparc_reportrun set status=%d,run_complete_time='%s' where id=%d" % (status, time, id)
            else:
                return 
            cursor.execute(query)
            db.commit()
            db.close()
        except:
            print "ERROR updating runstatus"
            
    def updateRunSummary(self, runid, summary):
        try:
            db = MySQLdb.connect(self.dbHost,self.dbUser,self.dbPwrd,self.dbDb)
            cursor = db.cursor()
            query = "update osparc_reportrun set number_of_plants=%d,total_dc_capacity=%d,total_storage_capacity=%d," \
                    "number_of_measurements=%d,first_measurement_date='%s',last_measurement_date='%s'" \
                    "where id=%d" %\
                    (
                        summary["number_of_plants"],summary["total_dc_capacity"],summary["total_storage_capacity"],
                        summary["number_of_measurements"],summary["first_measurement_date"],
                        summary["last_measurement_date"], runid
                    )
            cursor.execute(query)
            db.commit()
            db.close()
        except:
            print "ERROR updating runsummary"

    def readDef(self,defId):
        try:
            query = "select * from osparc_reportdefinition where id=%d" %defId
            db = MySQLdb.connect(self.dbHost, self.dbUser, self.dbPwrd, self.dbDb)
            cursor = db.cursor()
            cursor.execute(query)
            defi = cursor.fetchone()
            db.close()
            return defi
        except:
            print "ERROR reading defs"

    def getPlants(self,attr,op,value):
        try:
            print(attr,op,value)
            if attr != "":
                query = "select id,activation_date,dc_rating,storage_original_capacity,storage_current_capacity " \
                        "from core_plant where %s %s '%s'" % (attr,op,value)
            else:
                query = "select id,activation_date,dc_rating,storage_original_capacity,storage_current_capacity " \
                        "from core_plant"
            print "executing: ", query
            db = MySQLdb.connect(self.dbHost, self.dbUser, self.dbPwrd, self.dbDb)
            cursor = db.cursor()
            cursor.execute(query)
            plantArrays = cursor.fetchall()
            print(plantArrays)
            db.close()
            # look, daddy's own little ORM!
            plants = list()
            for pArray in plantArrays:
                plant = models.Plant(pArray[0], pArray[1], pArray[2], pArray[3], pArray[4])
                plants.append(plant)
            return plants
        except:
            print "ERROR getting plants"

    def getTimeSeries(self,plants,startTime,endTime):
        try:
            plantIds = list()
            for plant in plants:
                plantIds.append(str(plant.id))
            plantIdsStr = ','.join(plantIds)
            query = "select * from osparc_planttimeseries where plant_id in (%s) and time_stamp between '%s' and '%s'" \
                    % (plantIdsStr,startTime,endTime)
            print(query)
            db = MySQLdb.connect(self.dbHost,self.dbUser,self.dbPwrd,self.dbDb)
            cursor = db.cursor()
            cursor.execute(query)
            resultArrays = cursor.fetchall()
            print(resultArrays)
            db.close()
            # look, daddy's own little ORM!
            results = list()
            for eArray in resultArrays:
                entry = models.PlantTimeSeries(eArray[0], eArray[1], eArray[2], eArray[3], eArray[4], eArray[5],
                                               eArray[6], eArray[8])

                # (eArray[7] is plantUUID, eArray[8] is recordStatus)
                # print entry.timestamp,entry.plant
                results.append(entry)
            return results
        except Exception as e:
            print "ERROR getting timeseries", e.message, "exception type ", type(e)

    # save a kpi set associated with a reportrun
    def saveKpi(self, runId, kpi):
        try:
            # there must be only one (reportrun,kpi) tuple per reportrun and kpi
            # (if one doesn't exist, query has no effect)
            query1 = "delete from osparc_kpi where name='%s' and report_run_id=%d" % (kpi["name"], runId)
            print(query1)
            query = "insert into osparc_kpi " \
                    "(name,number_of_plants,first_day,last_day,mean,median,minimum,maximum,sample_interval,report_run_id) " \
                    "values ('%s',%d,'%s','%s',%.3f,%.3f,%.3f,%.3f,'%s',%d)" \
                    %(kpi["name"],kpi["plants"],kpi["first_day"],kpi["last_day"],kpi["mean"],kpi["median"],kpi["minimum"],
                      kpi["maximum"],kpi["sample_interval"],runId)
            print(query)
            db = MySQLdb.connect(self.dbHost,self.dbUser,self.dbPwrd,self.dbDb)
            cursor = db.cursor()
            cursor.execute(query1)
            cursor.execute(query)
            db.commit()
            db.close()
        except Exception as e:
            print "ERROR saving kpis", e.message, "exception type ", type(e)

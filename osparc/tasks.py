from .mixins import KPIs
from celery import shared_task
from django.core.mail import send_mail
from django.conf import settings
from django.core.mail import EmailMultiAlternatives
from django.template.loader import render_to_string
from django.core import serializers as d_serializers
from bs4 import BeautifulSoup

from core import models as core_models
from osparc import models as osparc_models

import json


def serialize_to_dict(model_instance):
    return json.loads(d_serializers.serialize('json', [model_instance]))[0]


@shared_task
def calculate_kpis(
        plants_qs, timeseries_qs, user_email, custom_query_id, email_desired,
        plant_filters={}, timeseries_filters={}
    ):
    if plant_filters and timeseries_filters:
        plants_qs = core_models.Plant.objects.filter(**plant_filters)

        timeseries_filters['plant__in'] = plants_qs
        timeseries_qs  = osparc_models.PlantTimeSeries.objects.filter(**timeseries_filters)

    kpi = KPIs()
    result = kpi.calculateKPIs(plants_qs, timeseries_qs)

    kpi_result = result
    keys = [
        'monthly_insolation', 'storage_capacity', 'monthly_yield', 'dc_rating',
        'storage_state_of_health', 'monthly_generated_energy', 'performance_ratio'
    ]

    for key in keys:
        try:
            kpi_result[key]['first_day'] = str(result[key]['first_day'])
            kpi_result[key]['last_day'] = str(result[key]['last_day'])
        except:
            # ignore errors
            pass

    cq = osparc_models.CustomQuery.objects.get(pk=custom_query_id)
    cq.kpi_result = json.dumps(kpi_result)
    cq.kpi_status = "SUCCESS"
    cq.plants = plants_qs
    cq.timeseries = timeseries_qs
    cq.save()

    if email_desired == 'on':
        data = {
            'user_email': user_email,
            'domain': settings.SUNSPEC_DOMAIN,
            'custom_query_id': custom_query_id,
        }
        email_template_name = 'kpi_report_email.html'
        email_template = render_to_string(email_template_name, data)
        send_mail(
            'KPI report for APSuite',
            'KPI report for APSuite',
            settings.DEFAULT_FROM_EMAIL,
            [user_email],
            html_message=email_template,
            fail_silently=False,
        )

    return result


@shared_task
def calculate_dashboard_kpis(custom_query_id):
    plants_qs = core_models.Plant.objects.all()
    timeseries_qs = osparc_models.PlantTimeSeries.objects.all()

    kpi = KPIs()
    result = kpi.calculateKPIs(plants_qs, timeseries_qs)

    kpi_result = result
    kpi_result['monthly_insolation']['first_day'] = str(result['monthly_insolation']['first_day'])
    kpi_result['monthly_insolation']['last_day'] = str(result['monthly_insolation']['last_day'])
    kpi_result['storage_capacity']['first_day'] = str(result['storage_capacity']['first_day'])
    kpi_result['storage_capacity']['last_day'] = str(result['storage_capacity']['last_day'])
    kpi_result['monthly_yield']['first_day'] = str(result['monthly_yield']['first_day'])
    kpi_result['monthly_yield']['last_day'] = str(result['monthly_yield']['last_day'])
    kpi_result['dc_rating']['first_day'] = str(result['dc_rating']['first_day'])
    kpi_result['dc_rating']['last_day'] = str(result['dc_rating']['last_day'])    
    kpi_result['storage_state_of_health']['first_day'] = str(result['storage_state_of_health']['first_day'])
    kpi_result['storage_state_of_health']['last_day'] = str(result['storage_state_of_health']['last_day']) 
    kpi_result['monthly_generated_energy']['first_day'] = str(result['monthly_generated_energy']['first_day'])
    kpi_result['monthly_generated_energy']['last_day'] = str(result['monthly_generated_energy']['last_day'])
    kpi_result['performance_ratio']['first_day'] = str(result['performance_ratio']['first_day'])
    kpi_result['performance_ratio']['last_day'] = str(result['performance_ratio']['last_day']) 

    update_query_result = osparc_models.CustomQuery.objects.filter(
        id=custom_query_id
    ).update(kpi_result=json.dumps(kpi_result), kpi_status="SUCCESS")

    return result

@shared_task
def create_solar_anywhere_data(plant_id, solar_anywhere_data):
    plant = core_models.Plant.objects.get(id=plant_id)
    soup = BeautifulSoup(solar_anywhere_data, "html.parser")
    data_periods = soup.find_all('weatherdataperiods')
    for data in data_periods:
        data_period = soup.find_all('weatherdataperiod')
        for period in data_period:
            data = {
                'start_time': period['starttime'],
                'observation_time': period['observationtime'],
                'end_time': period['endtime'],
                'global_horizontal_irradiance':period.get('globalhorizontalirradiance_wattspermetersquared') if period.get('globalhorizontalirradiance_wattspermetersquared') else None,
                'direct_normal_irradiance': period.get('directnormalirradiance_wattspermetersquared') if period.get('directnormalirradiance_wattspermetersquared') else None,
                'diffuse_horizontal_irradiance': period.get('diffusehorizontalirradiance_wattspermetersquared') if period.get('diffusehorizontalirradiance_wattspermetersquared') else None,
                'irradiance_observation_type': period.get('irradianceobservationtype') if period.get('irradianceobservationtype') else None,
                'ambient_temperature': period.get('ambienttemperature_degreesc') if period.get('ambienttemperature_degreesc') else None,
                'ambient_temperature_observation_type': period.get('ambienttemperatureobservationtype') if period.get('ambienttemperatureobservationtype') else None,
                'wind_speed': period.get('windspeed_meterspersecond') if period.get('windspeed_meterspersecond') else None,
                'wind_speed_observation_type': period.get('windspeedobservationtype') if period.get('windspeedobservationtype') else None,
                'relative_humidity': period.get('relativehumidity_percent') if period.get('relativehumidity_percent') else None,
                'liquid_precipitation': period.get('liquidprecipitation_kilogramspermetersquared') if period.get('liquidprecipitation_kilogramspermetersquared') else None,
                'solid_precipitation': period.get('solidprecipitation_kilogramspermetersquared') if period.get('solidprecipitation_kilogramspermetersquared') else None,
                'snow_depth': period.get('snowdepth_meters') if period.get('snowdepth_meters') else None,
                'plant': plant,
            }

            weather_data = osparc_models.WeatherData.objects.create(**data)

    return "Successfully inserted data into database"
from django.conf.urls import url
from . import views


urlpatterns = [

    url('^upload_activities/$', views.UploadActivityList.as_view()),
    url('^upload_activities/(?P<pk>[0-9]+)$', views.UploadActivityDetail.as_view()),

    url('^plants/(?P<plant_pk>[0-9]+)/plant_time_series/$', views.PlantTimeSeriesList.as_view()),
    url('^plants/(?P<plant_pk>[0-9]+)/plant_time_series/(?P<pk>[0-9]+)$', views.PlantTimeSeriesDetail.as_view()),

    url('^plants/(?P<plant_pk>[0-9]+)/weather_data/$', views.WeatherDataList.as_view()),
    url('^plants/(?P<plant_pk>[0-9]+)/solar-anywhere-data/$', views.SolarAnywhereWeatherData.as_view()),
    # url('^organizations/(?P<org_pk>[0-9]+)/plants/(?P<plant_pk>[0-9]+)/plant_time_series/$', views.PlantTimeSeriesList.as_view()),
    # url('^organizations/(?P<org_pk>[0-9]+)/plants/(?P<plant_pk>[0-9]+)/plant_time_series/(?P<pk>[0-9]+)$', views.PlantTimeSeriesDetail.as_view()),

    url('^all-plants-list/$', views.AllPlantsList.as_view()), #strictly for oSPARC

    url('^organizations/(?P<org_pk>[0-9]+)/report_definitions/$', views.ReportDefinitionList.as_view()),
    url('^organizations/(?P<org_pk>[0-9]+)/report_definitions/(?P<pk>[0-9]+)$', views.ReportDefinitionDetail.as_view()),

    url('^organizations/(?P<org_pk>[0-9]+)/reports/$', views.ReportRunList.as_view()),
    url('^organizations/(?P<org_pk>[0-9]+)/reports/(?P<pk>[0-9]+)$', views.ReportRunDetail.as_view()),

    # osparc dashboard
    url('^geochart/', views.ListPlantsForGeoChart.as_view()),

    url('^aggregates/', views.AggregatesView.as_view()),

    url('^kpis/calc/', views.KPIsView.as_view()),
    url('^kpis/(?P<plant_pk>[0-9]+)/calc/$', views.KPIsSinglePlantView.as_view()),
    url('^kpis/multiple-plants-calc/$', views.KPIsMultiplePlantSelection.as_view()),
    url('^kpis/(?P<org_pk>[0-9]+)/list/$', views.CustomQueryList.as_view()),
    url('^kpis/(?P<org_pk>[0-9]+)/list/(?P<pk>[0-9]+)', views.CustomQueryDetail.as_view()),
    url('^query-task-status/(?P<id>.+)/$', views.query_task_status),
    url('^osparc-dashboard-stats/$', views.dashboard_stats),

    url('^docs/', views.schema_view),

]

import datetime
import dateparser
import sys
import collections
from core.models import Plant
from osparc.models import KPI
from osparc.serializers import KPISerializer
import math
import numpy as np
from costmodel import models as costmodel_models
from core import models as core_models

import decimal
import json
import math
from collections import Counter
import numpy as np
from functools import reduce
from itertools import groupby
from operator import add, itemgetter


class serialize_to_object(object):

    def __init__(self, d):
        self.__dict__ = d


class KpiTimeseriesElement:
    def __init__(self, plantId, timestamp, numerator, denominator):
        self.plantId = plantId
        self.timestamp = timestamp
        self.value = numerator / denominator


class KPIs(object):
    def median(self, lst):
        sortedLst = sorted(lst)
        lstLen = len(lst)
        index = (lstLen - 1) // 2
        if (lstLen % 2):
            return sortedLst[index]
        else:
            return (sortedLst[index] + sortedLst[index + 1]) / 2.0

    def buildKpi(self, entryList, name):
        firstEntry = datetime.date.today()
        lastEntry = datetime.datetime.strptime('01012001', '%d%m%Y').date()
        total = 0
        if len(entryList) > 0:
            minValue = sys.float_info.max
        else:
            minValue = 0
        maxValue = 0
        valueList = list()
        currentPlant = 0
        numberOfPlants = 0

        for entry in entryList:
            entry.timestamp = dateparser.parse(str(entry.timestamp)).date()
            if entry.plantId != currentPlant:
                currentPlant = entry.plantId
                numberOfPlants = numberOfPlants + 1  # there are multiple entries from the same plant
            # print( "currentPlant=%d, entry.plantId=%d, numberOfPlants=%d" % (currentPlant,entry.plantId,numberOfPlants))
            if entry.timestamp < firstEntry:
                firstEntry = entry.timestamp
            if entry.timestamp > lastEntry:
                lastEntry = entry.timestamp
            total += entry.value
            if entry.value < minValue:
                minValue = entry.value
            if entry.value > maxValue:
                maxValue = entry.value
            valueList.append(entry.value)

        kpi = collections.defaultdict(dict)
        kpi['name'] = name
        kpi['plants'] = numberOfPlants
        kpi['first_day'] = firstEntry
        kpi['last_day'] = lastEntry
        kpi['minimum'] = round(minValue, 3)
        kpi['maximum'] = round(maxValue, 3)
        if len(entryList) > 0:
            kpi['mean'] = round(total / len(entryList), 3)
        else:
            kpi['mean'] = 0
        if len(valueList) > 0:
            kpi['median'] = round(KPIs.median(self, valueList), 3)
        else:
            kpi['median'] = 0
        kpi['sample_interval'] = 'monthly'

        return kpi

    def divide(self, dict1, dict2):
        kpi = collections.defaultdict(dict)
        kpi['plants'] = min(dict1['plants'], dict2['plants'])
        kpi['first_day'] = max(dict1['first_day'], dict2['first_day'])
        kpi['last_day'] = min(dict1['last_day'], dict2['last_day'])
        kpi['minimum'] = round(dict1['minimum'] / dict2['minimum'], 3)
        kpi['maximum'] = round(dict1['maximum'] / dict2['maximum'], 3)
        kpi['mean'] = round(dict1['mean'] / dict2['mean'], 3)
        kpi['median'] = round(dict1['median'] / dict2['median'], 3)
        kpi['sample_interval'] = dict1['sample_interval']
        return kpi

    def saveKpi(self, kpi, name):
        existing = KPI.objects.all()
        try:
            existingKpi = KPI.objects.get(name=name)
            serializer = KPISerializer(existingKpi, data=kpi)
            valid = serializer.is_valid()
            serializer.save()
            return serializer.validated_data
        except:
            serializer = KPISerializer(data=kpi)
            valid = serializer.is_valid()
            serializer.save()
            return serializer.validated_data
        return

    def buildAndSaveKpi(self, entryList, name):
        return KpiMixin.saveKpi(self, KpiMixin.buildKpi(self, entryList, name), name)

    def findPlantsDcrating(self, plants, id):
        for plant in plants:
            if plant.id == id:
                return plant.dc_rating
        return None

    def calculatePlantKPIs(self, plants):

        # dc_rating, storage_capacity and storage State of Health
        dcList = list()
        storCapList = list()
        storSOHList = list()

        try:
            for plant in plants:
                if plant.dc_rating is not None:
                    dcList.append(KpiTimeseriesElement(plant.id, plant.activation_date, plant.dc_rating, 1))
                if plant.storage_original_capacity is not None:
                    storCapList.append(
                        KpiTimeseriesElement(plant.id, plant.activation_date, plant.storage_original_capacity, 1))
                    if plant.storage_current_capacity is not None:
                        storSOHList.append(
                            KpiTimeseriesElement(plant.id, plant.activation_date, plant.storage_current_capacity,
                                                 plant.storage_original_capacity))
        except:
            print "ERROR reading plant dc_rating or storage_capacity"
            return None

        # Fill in the plant-related KPIs
        result = collections.defaultdict(dict)

        # 1. DC Power Rating (rated DC power)
        result['dc_rating'] = KPIs.buildKpi(self, dcList, 'dc_rating')

        # 2. Storage Capacity
        result['storage_capacity'] = KPIs.buildKpi(self, storCapList, 'storage_capacity')

        # 3. Storage State of Health
        result['storage_state_of_health'] = KPIs.buildKpi(self, storSOHList, 'storage_state_of_health')

        return result

    def calculateTimeseriesKPIs(self, plants, timeseries):

        # First, get a list of each element that will contribute to each KPI
        # We get a separate list per KPI because not all time series elements contain all measurements
        ghiList = list()
        whList = list()
        yfList = list()
        yrList = list()

        try:
            for entry in timeseries:
                entry.time_stamp = dateparser.parse(str(entry.time_stamp))
                if entry.GHI_DIFF is not None:
                    ghiList.append(KpiTimeseriesElement(entry.plant, entry.time_stamp.date(), entry.GHI_DIFF, 1))
                if entry.WH_DIFF is not None:
                    whList.append(KpiTimeseriesElement(entry.plant, entry.time_stamp.date(), entry.WH_DIFF, 1))
                    if isinstance(entry.plant, Plant):
                        dc_rating = entry.plant.dc_rating
                    else:
                        dc_rating = KPIs.findPlantsDcrating(self, plants, entry.plant)
                    yfList.append(KpiTimeseriesElement(entry.plant, entry.time_stamp.date(), entry.WH_DIFF, dc_rating))
                if entry.HPOA_DIFF is not None:
                    yrList.append(KpiTimeseriesElement(entry.plant, entry.time_stamp.date(), entry.HPOA_DIFF, 1000))
        except Exception as e:
            print e
            return None

        result = collections.defaultdict(dict)

        # 1. GHI (daily insolation)
        result['monthly_insolation'] = KPIs.buildKpi(self, ghiList, 'monthly_insolation')

        # 2. WH (daily generated energy)
        result['monthly_generated_energy'] = KPIs.buildKpi(self, whList, 'monthly_generated_energy')

        # 3. YF (generated yield kWh/kWp)
        result['monthly_yield'] = KPIs.buildKpi(self, yfList, 'monthly_yield')

        # 4. YR (hpoa yield kWh/kWp)
        if len(yrList) > 0:
            denom = KPIs.buildKpi(self, yrList, '')
            result['performance_ratio'] = KPIs.divide(self, result['monthly_yield'], denom)
            result['performance_ratio']['name'] = 'performance_ratio'

        return result

    def calculateKPIs(self, plants, timeseries):
        def identity(x):
            return x

        def make_object(x):
            x.update(x.pop('fields'))
            x['id'] = x['pk']
            return serialize_to_object(x)

        if plants and isinstance(plants[0], dict):
            intermediate = make_object
        else:
            intermediate = identity

        plants = [intermediate(p) for p in plants]

        plantResult = KPIs.calculatePlantKPIs(self, plants)
        if plantResult == None:
            return None

        if timeseries and isinstance(timeseries[0], dict):
            intermediate = make_object
        else:
            intermediate = identity

        timeseries = [intermediate(t) for t in timeseries]

        timeseriesResult = KPIs.calculateTimeseriesKPIs(self, plants, timeseries)
        if timeseriesResult == None:
            result = dict(plantResult.items())
        else:
            result = dict(plantResult.items() + timeseriesResult.items())
        return result


def calculate_n_by_N(N, Q, R):
    P = 1-Q
    # term = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10]

    rows = 11
    columns = 2
    n_by_N_r = [[0 for x in range(columns)] for y in range(rows)]

    n_by_N_r[1][0] = 0.1
    n_by_N_r[2][0] = 0.2
    n_by_N_r[3][0] = 0.3
    n_by_N_r[4][0] = 0.4
    n_by_N_r[5][0] = 0.5
    n_by_N_r[6][0] = 0.6
    n_by_N_r[7][0] = 0.7
    n_by_N_r[8][0] = 0.8
    n_by_N_r[9][0] = 0.9
    n_by_N_r[10][0] = 1.0

    n_by_N_r[0][1] = pow(P, N)
    n_by_N_r[1][1] = n_by_N_r[0][1] + N * pow(P, (N - 1)) * Q
    n_by_N_r[2][1] = n_by_N_r[1][1] + N * (N - 1) * pow(P, (N - 2)) * pow(Q, 2) / math.factorial(2)
    n_by_N_r[3][1] = n_by_N_r[2][1] + N * (N - 1) * (N - 2) * pow(P, (N - 3)) * pow(Q, 3) / math.factorial(3)
    n_by_N_r[4][1] = n_by_N_r[3][1] + N * (N - 1) * (N - 2) * (N - 3) * pow(P, (N - 4)) * pow(Q, 4) / math.factorial(4)
    n_by_N_r[5][1] = n_by_N_r[4][1] + N * (N - 1) * (N - 2) * (N - 3) * (N - 4) * pow(P, (N - 5)) * pow(Q, 5) / math.factorial(5)
    n_by_N_r[6][1] = n_by_N_r[5][1] + N * (N - 1) * (N - 2) * (N - 3) * (N - 4) * (N - 5) * pow(P, (N - 6)) * pow(Q, 6) / math.factorial(6)
    n_by_N_r[7][1] = n_by_N_r[6][1] + N * (N - 1) * (N - 2) * (N - 3) * (N - 4) * (N - 5) * (N - 6) * pow(P, (N - 7)) * pow(Q, 7) / math.factorial(7)
    n_by_N_r[8][1] = n_by_N_r[7][1] + N * (N - 1) * (N - 2) * (N - 3) * (N - 4) * (N - 5) * (N - 6) * (N - 7) * pow(P, (N - 8)) * pow(Q, 8) / math.factorial(8)
    n_by_N_r[9][1] = n_by_N_r[8][1] + N * (N - 1) * (N - 2) * (N - 3) * (N - 4) * (N - 5) * (N - 6) * (N - 7) * (N - 8) * pow(P, (N - 9)) * pow(Q, 9) / math.factorial(9)
    n_by_N_r[10][1] = n_by_N_r[9][1] + N * (N - 1) * (N - 2) * (N - 3) * (N - 4) * (N - 5) * (N - 6) * (N - 7) * (N - 8) * (N - 9) * pow(P, (N - 10)) * pow(Q, 10) / math.factorial(10)

    # Calculating n_by_N using closest value
    n_by_N_r_columns = zip(*n_by_N_r)
    closest_R_index = n_by_N_r_columns[1].index(min(n_by_N_r_columns[1], key=lambda x: abs(x - float(R))))
    # n_by_N = n_by_N_r[closest_R_index][0]

    # Calculating n_by_N using Interpolate
    n_by_N_list = n_by_N_r_columns[0]
    R_list = n_by_N_r_columns[1]
    n_by_N = np.interp(float(R), R_list, n_by_N_list)

    return n_by_N

def plants_in_group(plant_group_id):

    plant_list = []

    plant_group = costmodel_models.PlantGroup.objects.get(id=plant_group_id)
    for plant in plant_group.plant.all():
        plant_list.append(plant)

    for group in plant_group.plant_group.all():
        more_plants = plants_in_group(group.id)
        for plant in more_plants:
            if plant not in plant_list:
                plant_list.append(plant)

    return plant_list

def merge_records_by(key, combine):
    """Returns a function that merges two records rec_a and rec_b.
       The records are assumed to have the same value for rec_a[key]
       and rec_b[key].  For all other keys, the values are combined
       using the specified binary operator.
    """
    return lambda rec_a, rec_b: {
        k: rec_a[k] if k == key else combine(float(rec_a[k]), float(rec_b[k]))
        for k in rec_a
    }


def merge_list_of_records_by(key, combine):
    """Returns a function that merges a list of records, grouped by
       the specified key, with values combined using the specified
       binary operator."""
    keyprop = itemgetter(key)
    return lambda lst: [
        reduce(merge_records_by(key, combine), records)
        for _, records in groupby(sorted(lst, key=keyprop), keyprop)
    ]


def get_results(plant_id, costmodel_id):
    total_labor_hours_per_year = []
    total_labor_cost = []
    total_matl_cost = []
    avg_annual_expense = []
    npv_life = []

    percentage_of_total_npv = []
    service_list = []
    service_instance_list = []
    service_id_list = []

    # Fetch the Plant and Cost Model
    costmodel = costmodel_models.CostModel.objects.get(id=costmodel_id)
    plant = core_models.Plant.objects.get(id=plant_id)
    plant_name = plant.name
    costmodel_name = costmodel.name

    if plant.plant_finance is None:
        return Response({'status': 'error', 'details': 'Plant Finance Object does not exist'}, status=400)

    if plant.plant_equipment is None:
        return Response({'status': 'error', 'details': 'Plant Equipment Object does not exist'}, status=400)

    for service_values in costmodel.service_values.all():
        service_id_list.append(service_values.id)
        service_list.append(service_values.service.name)
        service_instance_list.append(service_values.name)

        try:
            number_of_units = eval(service_values.number_of_units)
        except (ZeroDivisionError, TypeError, AttributeError):
            number_of_units = 0

        try:
            labor_hours_per_unit = eval(service_values.labor_hours_per_unit)
        except (ZeroDivisionError, TypeError, AttributeError):
            labor_hours_per_unit = 0

        try:
            material_cost_per_unit = eval(service_values.material_cost_per_unit)
        except (ZeroDivisionError, TypeError, AttributeError):
            material_cost_per_unit = 0

        # Calculate Total Labor Cost
        total_labor_cost.append(float(number_of_units) * float(labor_hours_per_unit) * float(service_values.labor_rate.rate_per_hour) * float(service_values.labor_rate.overhead_multiplier))

        # Calculate Total Material and Other Cost
        total_matl_cost.append(float(number_of_units) * float(material_cost_per_unit))

        # Calculate Total Labor Hours per Year
        total_labor_hours_per_year.append(float(number_of_units) * float(labor_hours_per_unit) / float(service_values.mean_interval))

    # Calculate Total Cost for Measure
    total_cost_for_measure = [x + y for x, y in zip(total_labor_cost, total_matl_cost)]
    d_total_cost_for_measure = ["%.2f" % v for v in total_cost_for_measure]

    total_matl_cost = [decimal.Decimal(x) for x in total_matl_cost]
    total_labor_cost = [decimal.Decimal(x) for x in total_labor_cost]
    d_total_labor_cost = ["%.2f" % v for v in total_labor_cost]
    d_total_labor_hours_per_year = ["%.2f" % v for v in total_labor_hours_per_year]

    # Calculate Annual Cash Flow
    columns = plant.plant_finance.analysis_period
    rows = costmodel.service_values.count()
    annual_cash_flow = [[0 for x in range(columns)] for y in range(rows)]
    d_annual_cash_flow = [[0 for x in range(columns)] for y in range(rows)]
    # npv_life = np.zeros(rows)

    warranty_years = 0

    # for i in range(rows):
    # for i in range(len(annual_cash_flow)) and service_values in costmodel.service_values.all():
    for i, k, l, m, service_values in map(None, range(len(annual_cash_flow)), total_cost_for_measure, total_labor_cost, total_matl_cost, costmodel.service_values.all()):
        # for j in range(columns):
        for j in range(len(annual_cash_flow[i])):

            # Determine the number of years covered by warranty
            if service_values.warranty_type.name == "Module (Product)":
                warranty_years = plant.plant_finance.module_warranty
            elif service_values.warranty_type.name == "Inverter":
                warranty_years = plant.plant_finance.inverter_warranty
            elif service_values.warranty_type.name == "EPC":
                warranty_years = plant.plant_finance.epc_warranty
            elif service_values.warranty_type.name == "Monitoring":
                warranty_years = plant.plant_finance.monitoring_warranty

            if j+1 > warranty_years:
                if service_values.failure_distribution_type.id == 1:
                    if service_values.mean_interval < 1.00:
                        # A
                        annual_cash_flow[i][j] = float((k/float(service_values.mean_interval)) * float(pow((1+(plant.plant_finance.inflation_rate/100)), j+1)))

                    else:
                        # B
                        annual_cash_flow[i][j] = (int((j+1)/service_values.mean_interval) - int((j+1-1)/service_values.mean_interval)) * float(k) * float(pow((1+(plant.plant_finance.inflation_rate/100)), (j+1)))

                else:
                    # WEIBULL.DIST(j, service_values.shape_factor, service_values.mean_interval, False)
                    x = j+1
                    a = service_values.shape_factor
                    b = service_values.mean_interval
                    weibull = float(a/pow(b, a)) * float(pow(x, a-1)) * float(math.exp(-(pow((x/b), a))))
                    # C
                    annual_cash_flow[i][j] = float(weibull * float(k) * float(pow((1+(plant.plant_finance.inflation_rate/100)), j+1)))

            else:
                if service_values.failure_distribution_type.id == 1:
                    if service_values.mean_interval < 1.00:
                        # D
                        annual_cash_flow[i][j] = float((float(m * (1-service_values.material_warranty_covered) + l * (1-service_values.labor_warranty_covered)) / service_values.mean_interval) * (pow((1+(plant.plant_finance.inflation_rate/100)), j+1)))
                    else:
                        # E
                        annual_cash_flow[i][j] = (int((j+1)/service_values.mean_interval) - int((j+1-1) / service_values.mean_interval)) * float(m * (1-service_values.material_warranty_covered) + l * (1-service_values.labor_warranty_covered)) * float(pow((1+(plant.plant_finance.inflation_rate/100)), j+1))

                else:
                    # WEIBULL.DIST(j, service_values.shape_factor, service_values.mean_interval, False)
                    x = j+1
                    a = service_values.shape_factor
                    b = service_values.mean_interval
                    weibull = float(a / pow(b, a)) * float(pow(x, a - 1)) * float(math.exp(-(pow((x / b), a))))
                    # F
                    annual_cash_flow[i][j] = float(weibull * float(m * (1-service_values.material_warranty_covered) + l * (1-service_values.labor_warranty_covered)) * float((pow((1+(plant.plant_finance.inflation_rate/100)), j+1))))

    npv = 0.00
    for i in range(len(annual_cash_flow)):
        # Calculate Average Annual Expense
        avg_annual_expense.append(sum(annual_cash_flow[i]) / len(annual_cash_flow[i]))
        d_avg_annual_expense = ["%.2f" % v for v in avg_annual_expense]

        values = [float(j) for j in (annual_cash_flow[i])]
        # Calculate NPV Project Life
        for key, value in enumerate(values):
            npv += (value / (pow((1 + float(plant.plant_finance.discount_rate/100)), key + 1)))
        npv_life.append(npv)
        npv = 0.00
        # npv_life.append(np.npv(float(plant.plant_finance.discount_rate/100), [float(j) for j in (annual_cash_flow[i])]))
        total_npv_life = sum(npv_life)
        d_total_npv_life = "%.2f" % total_npv_life
        d_npv_life = ["%.2f" % v for v in npv_life]

    sum_avg_annual_expense = sum(float(v) for v in avg_annual_expense)
    d_sum_avg_annual_expense = ["%.2f" % sum_avg_annual_expense]

    for i in range(len(npv_life)):
        # Calculate Percentage of Total NPV
        percentage_of_total_npv.append((npv_life[i]/reduce(lambda x, y: x + y, npv_life))*100)
        twodecimals_percentage_of_total_npv = ["%.2f" % v for v in percentage_of_total_npv]

    for i in range(len(annual_cash_flow)):
        d_annual_cash_flow[i] = ["%.2f" % v for v in annual_cash_flow[i]]
        # Calculate Sum of Annual Cash Flow
        sum_acf = [sum(x) for x in zip(*[[float(y) for y in x] for x in annual_cash_flow])]  # (*annual_cash_flow)]
        d_sum_acf = ["%.2f" % v for v in sum_acf]

        sum_annual_cash_flow = []
    for i, j in map(None, range(columns), d_sum_acf):
        sum_annual_cash_flow.append({'year': i+1, 'value': j})

    # Calculate NPV by O&M Type
    cost_per_year_by_om_type = Counter()
    for service_values, j in map(None, costmodel.service_values.all(), avg_annual_expense):
        cost_per_year_by_om_type[service_values.service.om_type.name] += float(j)

    npv_by_om_type = Counter()
    for service_values, j in map(None, costmodel.service_values.all(), npv_life):
        npv_by_om_type[service_values.service.om_type.name] += float(j)

    npv_by_activity = []

    for i, j in map(None, cost_per_year_by_om_type.items(), npv_by_om_type.items()):
        npv_by_activity.append(
             {'label': i[0], 'cost_per_year': "%.2f" %(i[1]), 'npv': "%.2f" % (j[1]),
              'npv_per_wp': "%.4f" % (j[1]/(plant.dc_rating*1000)),
              'value': "%.2f" % ((j[1]/total_npv_life)*100)})

    # Calculate NPV by Asset Type
    cost_per_year_by_om_category = Counter()
    for service_values, j in map(None, costmodel.service_values.all(), avg_annual_expense):
        cost_per_year_by_om_category[service_values.service.asset_type.name] += float(j)

    npv_by_om_category = Counter()
    for service_values, j in map(None, costmodel.service_values.all(), npv_life):
        npv_by_om_category[service_values.service.asset_type.name] += float(j)

    npv_life_by_om_category = []

    for i, j in map(None, cost_per_year_by_om_category.items(), npv_by_om_category.items()):
        npv_life_by_om_category.append(
             {'asset_type': i[0], 'cost_per_year': "%.2f" % (i[1]), 'npv': "%.2f" % (j[1]),
              'percentage_of_npv': "%.2f" % ((j[1] / total_npv_life)*100)})

    # Calculate NPV by Service Provider
    cost_per_year_by_sp = Counter()
    for service_values, j in map(None, costmodel.service_values.all(), avg_annual_expense):
        cost_per_year_by_sp[service_values.service_provider.name] += float(j)

    npv_by_sp = Counter()
    for service_values, j in map(None, costmodel.service_values.all(), npv_life):
        npv_by_sp[service_values.service_provider.name] += float(j)

    npv_life_by_sp = []

    for i, j in map(None, cost_per_year_by_sp.items(), npv_by_sp.items()):
        npv_life_by_sp.append(
             {'service_provider': i[0], 'cost_per_year': "%.2f" % (i[1]), 'npv': "%.2f" % (j[1]),
              'percentage_of_npv': "%.2f" % ((j[1] / total_npv_life)*100)})

    # Calculate Reserve Account
    columns = plant.plant_finance.analysis_period
    rows = costmodel.service_values.count()
    reserve_account = [[0 for x in range(columns)] for y in range(rows)]
    d_reserve_account = [[0 for x in range(columns)] for y in range(rows)]

    warranty_years = 0

    for i, k, l, m, service_values in map(None, range(len(reserve_account)), total_cost_for_measure, total_labor_cost, total_matl_cost, costmodel.service_values.all()):
        for j in range(len(reserve_account[i])):

            # Calculate No of Units
            try:
                number_of_units = eval(service_values.number_of_units)
            except (ZeroDivisionError, TypeError):
                number_of_units = 0

            # Determine the number of years covered by warranty
            if service_values.warranty_type.name == "Module (Product)":
                warranty_years = plant.plant_finance.module_warranty
            elif service_values.warranty_type.name == "Inverter":
                warranty_years = plant.plant_finance.inverter_warranty
            elif service_values.warranty_type.name == "EPC":
                warranty_years = plant.plant_finance.epc_warranty
            elif service_values.warranty_type.name == "Monitoring":
                warranty_years = plant.plant_finance.monitoring_warranty

            if j + 1 > warranty_years:
                if service_values.failure_distribution_type.id == 1:
                    if service_values.mean_interval < 1.00:
                        # A
                        reserve_account[i][j] = max(annual_cash_flow[i][j], (float((k / float(service_values.mean_interval)) * float(pow((1 + (plant.plant_finance.inflation_rate / 100)), j + 1)))))

                    else:
                        # B
                        reserve_account[i][j] = max(annual_cash_flow[i][j], (int((j+1) / service_values.mean_interval) - int((j+1-1) / service_values.mean_interval) * float(k) * float(pow((1 + (plant.plant_finance.inflation_rate / 100)), j + 1))))

                else:
                    # WEIBULL.DIST(j, service_values.shape_factor, service_values.mean_interval, False)
                    x = j + 1
                    a = service_values.shape_factor
                    b = service_values.mean_interval
                    weibull = float(a / pow(b, a)) * float(pow(x, a - 1)) * float(math.exp(-(pow((x / b), a))))

                    # n_by_N
                    n_by_N = calculate_n_by_N(number_of_units, weibull, plant.plant_finance.desired_confidence_that_reserve_covers_cost)

                    # C
                    # Old Reserve Account formula
                    # reserve_account[i][j] = max(annual_cash_flow[i][j], (pow((float(plant.desired_confidence_that_reserve_covers_cost)), ((1-weibull)/weibull)) * float(k) * float(pow((1 + (plant.plant_finance.inflation_rate / 100)), j + 1))))

                    # New Reserve Account formula - Uses n_by_N_calculation
                    reserve_account[i][j] = max(annual_cash_flow[i][j], (n_by_N * float(k) * float(pow((1 + (plant.plant_finance.inflation_rate / 100)), j + 1))))

                    # print("service_values = " + str(service_values.service.name) + " Year = " + str(j + 1) + " Reserve Account = " + str(reserve_account[i][j]) + " ACF = " + str(annual_cash_flow[i][j]))

            else:
                if service_values.failure_distribution_type.id == 1:
                    if service_values.mean_interval < 1.00:
                        # D
                        reserve_account[i][j] = max(annual_cash_flow[i][j], (float((float(m * (1 - service_values.material_warranty_covered) + l * (1 - service_values.labor_warranty_covered)) / service_values.mean_interval) * (pow((1 + (plant.plant_finance.inflation_rate / 100)), j + 1)))))
                    else:
                        # E
                        reserve_account[i][j] = max(annual_cash_flow[i][j], (float(float(int((j + 1) / service_values.mean_interval) - int((j + 1 - 1) / service_values.mean_interval)) * float(m * (1 - service_values.material_warranty_covered) + l * (1 - service_values.labor_warranty_covered)) * (pow((1 + (plant.plant_finance.inflation_rate / 100)), j + 1)))))

                else:
                    # WEIBULL.DIST(j, service_values.shape_factor, service_values.mean_interval, False)
                    x = j + 1
                    a = service_values.shape_factor
                    b = service_values.mean_interval
                    weibull = float(a / pow(b, a)) * float(pow(x, a - 1)) * float(math.exp(-(pow((x / b), a))))

                    # n_by_N
                    n_by_N = calculate_n_by_N(number_of_units, weibull, plant.plant_finance.desired_confidence_that_reserve_covers_cost)

                    # F
                    # Old Reserve Account formula
                    # reserve_account[i][j] = max(annual_cash_flow[i][j], (pow(float(plant.desired_confidence_that_reserve_covers_cost), ((1-weibull)/weibull)) * float(m * (1 - service_values.material_warranty_covered) + l * (1 - service_values.labor_warranty_covered)) * float((pow((1 + (plant.plant_finance.inflation_rate / 100)), j + 1)))))

                    # New Reserve Account formula
                    reserve_account[i][j] = max(annual_cash_flow[i][j], (n_by_N * float(m * (1 - service_values.material_warranty_covered) + l * (1 - service_values.labor_warranty_covered)) * float((pow((1 + (plant.plant_finance.inflation_rate / 100)), j + 1)))))

    for i in range(len(reserve_account)):
        d_reserve_account[i] = ["%.2f" % v for v in reserve_account[i]]
        # Calculate Sum of Reserve Account
        sum_ra = [sum(x) for x in zip(*[[float(y) for y in x] for x in reserve_account])]
        d_sum_ra = ["%.2f" % v for v in sum_ra]

        sum_reserve_account = []
    for i, j in map(None, range(columns), d_sum_ra):
        sum_reserve_account.append({'year': str(i + 1), 'value': j})

    annual_production = []
    for j in range(columns):
        annual_production.append({'year': j + 1,
                                  'annual_production_cash_flow': "%.2f" %(plant.dc_rating * plant.energy_yield*(pow((1-plant.plant_equipment.whole_system_degradation_rate), j+1))+0.0000000000001),
                                  'annual_production_reserve_account': "%.2f" %(plant.dc_rating * plant.energy_yield * (pow((1 - plant.plant_equipment.whole_system_degradation_rate), j + 1)) + 0.0000000000001)
                                  })

    annual_average = []
    for i, j in map(None, range(columns), sum_ra):
        annual_average.append({'year': str(i + 1),
                               'annual_average_cash_flow': "%.5f" %(j/(plant.dc_rating*plant.energy_yield*(pow((1-plant.plant_equipment.whole_system_degradation_rate), i+1))+0.0000000000001)),
                               'annual_average_reserve_account': "%.5f" %(j/(plant.dc_rating*plant.energy_yield*(pow((1-plant.plant_equipment.whole_system_degradation_rate), i+1))+0.0000000000001))
                               })

    ir = plant.plant_finance.inflation_rate / 100
    dr = plant.plant_finance.discount_rate / 100

    annualized_om_costs = total_npv_life / float(
        (1 + ir) / (dr - ir) * (1 - (pow(((1 + ir) / (1 + dr)), plant.plant_finance.analysis_period))))

    maximum_reserve_account = max([x['value'] for x in sum_reserve_account])

    # Calculate npv_annual_om_costs_per_kwh
    npv_annual_om_costs = 0.00
    annual_production_cash_flow = []
    for d in annual_production:
        annual_production_cash_flow.append(d['annual_production_cash_flow'])
    values = [float(j) for j in annual_production_cash_flow]

    for key, value in enumerate(values):
        npv_annual_om_costs += (value / (pow((1 + float(plant.plant_finance.discount_rate / 100)), key + 1)))

    final_results = [{'annualized_om_costs': "%.2f" % annualized_om_costs,
                      'annualized_unit_om_costs': "%.2f" % (annualized_om_costs / plant.dc_rating),
                      'maximum_reserve_account': maximum_reserve_account,
                      'npv_om_costs': d_total_npv_life,
                      'npv_per_wp': "%.4f" % (total_npv_life / (plant.dc_rating*1000)),
                      'npv_annual_om_costs_per_kwh': "%.5f" % (float(d_total_npv_life)/npv_annual_om_costs)}]

    # Calculate Failure Mode Analysis Data
    columns = plant.plant_finance.analysis_period  # service_values ID, Failure Mode, Cost of Failure, Expected Cost of Failure, Flag (O&M Cost < Expected Energy Cost)
    rows = costmodel.service_values.count()
    failure_mode_analysis = [[0 for x in range(columns)] for y in range(rows)]

    dict_failure_mode_analysis = []

    for k, service_values, acf in map(None, total_cost_for_measure, costmodel.service_values.all(), annual_cash_flow):

        if service_values.failure_mode_values is not None:
            for j, lcoe in map(None, service_values.failure_mode_values.all(), annual_average):
                if j is not None:
                    cost_of_failure = j.outage_time * float(lcoe['annual_average_cash_flow']) * float(plant.plant_equipment.inverter_capacity)

                    if j.failure_distribution.id == 1:
                        expected_cost_of_failure = []
                        for year in range(plant.plant_finance.analysis_period):
                            expected_cost_of_failure.append(cost_of_failure * j.failure_parameter_1)
                    elif j.failure_distribution.id == 2:
                        expected_cost_of_failure = []
                        for year in range(plant.plant_finance.analysis_period):
                            x = year
                            a = j.failure_parameter_1
                            b = j.failure_parameter_2
                            weibull = 1 - (math.exp(-(pow((x / b), a))))
                            expected_cost_of_failure.append(weibull * cost_of_failure)
                    # print("expected_cost_of_failure"+str(expected_cost_of_failure))
                    # print("acf"+str(acf))
                    flag = []
                    for a, b in map(None, acf, expected_cost_of_failure):
                        if a < b:
                            flag.append(1)
                        else:
                            flag.append(0)
                    dict_failure_mode_analysis.append({'service_type_name': service_values.service.name,
                                                       'service_name': service_values.name,
                                                       # 'Failure Mode': json.dumps(j.__dict__),
                                                       'failure_mode': j.failure_mode.name + ' ' + j.failure_mode.description,
                                                       'cost_of_failure': "%.2f" %(cost_of_failure),
                                                       'expected_cost_of_failure': ["%.2f" % v for v in expected_cost_of_failure],
                                                       'annual_cash_flow': ["%.2f" % v for v in acf],
                                                       'flag': flag})

    data = {'plant_name': plant_name, 'costmodel_name': costmodel_name, 'service_id_list': service_id_list,
            'service_list': service_list, 'service_instance_list': service_instance_list,
            'total_labor_hours_per_year': d_total_labor_hours_per_year, 'total_labor_cost': d_total_labor_cost,
            'total_material_and_other_cost': total_matl_cost, 'total_cost_for_measure': d_total_cost_for_measure,
            'average_annual_expense': d_avg_annual_expense, 'sum_average_annual_expense': d_sum_avg_annual_expense,
            'npv_project_life': d_npv_life, 'sum_npv_project_life': d_total_npv_life,
            'percentage_of_total_npv': twodecimals_percentage_of_total_npv,
            'annual_cash_flow': d_annual_cash_flow, 'sum_annual_cash_flow': sum_annual_cash_flow,
            'npv_by_activity': npv_by_activity, 'npv_by_om_category': npv_life_by_om_category,
            'npv_by_service_provider': npv_life_by_sp, 'final_results': final_results,
            'reserve_account': d_reserve_account, 'sum_reserve_account': sum_reserve_account,
            'annual_production': annual_production, 'annual_average': annual_average,
            'dict_failure_mode_analysis': dict_failure_mode_analysis
            }
    result_for_rq = data

    total_mc = [str(float(r)) for r in data['total_material_and_other_cost']]
    result_for_rq.update({'total_material_and_other_cost': total_mc })

    return result_for_rq
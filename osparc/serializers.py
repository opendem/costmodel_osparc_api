from rest_framework import serializers
from models import *
from django_celery_results.models import TaskResult


class UploadActivitySerializer(serializers.ModelSerializer):

    class Meta:
        model = UploadActivity
        fields = '__all__'


class PlantReportSerializer(serializers.ModelSerializer):

    class Meta:
        model = PlantReport
        fields = '__all__'


class PlantTimeSeriesSerializer(serializers.ModelSerializer):
    # id = serializers.ReadOnlyField() ??? Not sure why

    class Meta:
        model = PlantTimeSeries
        fields = '__all__'


class KPISerializer(serializers.ModelSerializer):
    class Meta:
        model = KPI
        fields = '__all__'


class ReportDefinitionSerializer(serializers.ModelSerializer):
    # id = serializers.ReadOnlyField() ??? Not sure why

    class Meta:
        model = ReportDefinition
        fields = '__all__'


class ReportRunSerializer(serializers.ModelSerializer):
    id = serializers.ReadOnlyField()
    kpis = KPISerializer(source='kpi_set', many=True, read_only=True)

    class Meta:
        model = ReportRun
        fields = '__all__'


class WeatherDataSerializer(serializers.ModelSerializer):

    class Meta:
        model = WeatherData
        fields = '__all__'

class CustomQuerySerializer(serializers.ModelSerializer):

    class Meta:
        model = CustomQuery
        fields = '__all__'

class TaskStatusSerializer(serializers.ModelSerializer):

    class Meta:
        model = TaskResult
        fields = '__all__'
# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2017-10-05 16:49
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0013_auto_20170926_2116'),
        ('django_celery_results', '0001_initial'),
        ('osparc', '0005_auto_20170913_1719'),
    ]

    operations = [
        migrations.CreateModel(
            name='CustomQuery',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('report_name', models.CharField(max_length=145)),
                ('start_date', models.DateField()),
                ('end_date', models.DateField()),
                ('date', models.DateTimeField(default=django.utils.timezone.now)),
                ('organization', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='core.Organization')),
                ('plants', models.ManyToManyField(to='core.Plant')),
                ('task', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='django_celery_results.TaskResult')),
                ('timeseries', models.ManyToManyField(to='osparc.PlantTimeSeries')),
            ],
        ),
    ]

import rest_framework_filters as filters
from models import *


class UploadActivityFilter(filters.FilterSet):

    class Meta:
        model = UploadActivity
        fields = {
            'plant_upload_time': '__all__',
            'most_recent_time_series_upload_time': '__all__',
            'status': '__all__',
        }


class PlantReportFilter(filters.FilterSet):

    class Meta:
        model = PlantReport
        fields = {
            'record_status': '__all__',
            'create_time': '__all__',
            'sample_interval': '__all__',
            'first_measurement_date': '__all__',
            'last_measurement_date': '__all__',
            'monthly_yield': '__all__',
            'performance_ratio': '__all__',
            'storage_state_of_health': '__all__',
        }


class PlantTimeSeriesFilter(filters.FilterSet):

    class Meta:
        model = PlantTimeSeries
        fields = {
            'time_stamp': '__all__',
            'sample_interval': '__all__',
            'WH_DIFF': '__all__',
            'GHI_DIFF': '__all__',
            'TMPAMB_AVG': '__all__',
            'HPOA_DIFF': '__all__',
            'record_status': '__all__',
        }


class TotalFilter(filters.FilterSet):

    class Meta:
        model = Total
        fields = {
            'dc_rating': '__all__',
            'storage_original_capacity': '__all__',
        }


class ReportDefinitionFilter(filters.FilterSet):

    class Meta:
        model = ReportDefinition
        fields = {
            'name': '__all__',
            'description': '__all__',
            'observation_start_date': '__all__',
            'observation_end_date': '__all__',
            'plant_filter_attribute': '__all__',
            'plant_filter_operation': '__all__',
            'plant_filter_value': '__all__',
        }


class ReportRunFilter(filters.FilterSet):

    class Meta:
        model = ReportRun
        fields = {
            'status': '__all__',
            'run_submit_time': '__all__',
            'run_start_time': '__all__',
            'run_complete_time': '__all__',
            'first_measurement_date': '__all__',
            'last_measurement_date': '__all__',
            'number_of_measurements': '__all__',
            'number_of_plants': '__all__',
            'total_dc_capacity': '__all__',
            'total_storage_capacity': '__all__',
        }


class KPIFilter(filters.FilterSet):

    class Meta:
        model = KPI
        fields = {
            'name': '__all__',
            'number_of_plants': '__all__',
            'sample_interval': '__all__',
            'first_day': '__all__',
            'last_day': '__all__',
            'mean': '__all__',
            'median': '__all__',
            'minimum': '__all__',
            'maximum': '__all__',
        }


class WeatherDataFilter(filters.FilterSet):

    class Meta:
        model = WeatherData
        fields = {
            'start_time': '__all__',
            'observation_time': '__all__',
            'end_time': '__all__',
            'global_horizontal_irradiance': '__all__',
            'direct_normal_irradiance': '__all__',
            'diffuse_horizontal_irradiance': '__all__',
            'irradiance_observation_type': '__all__',
            'ambient_temperature': '__all__',
            'ambient_temperature_observation_type': '__all__',
            'wind_speed': '__all__',
            'wind_speed_observation_type': '__all__',
            'relative_humidity': '__all__',
            'liquid_precipitation': '__all__',
            'solid_precipitation': '__all__',
            'snow_depth': '__all__',
        }


class CustomQueryFilter(filters.FilterSet):

    class Meta:
        model = CustomQuery
        fields = {
            'report_name': '__all__',
            'start_date': '__all__',
            'end_date': '__all__',
            'task_id': '__all__',
            #'organization': '__all__',
        }
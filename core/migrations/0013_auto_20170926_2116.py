# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2017-09-26 21:16
from __future__ import unicode_literals

import django.core.validators
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0012_auto_20170918_1731'),
    ]

    operations = [
        migrations.AlterField(
            model_name='plantequipment',
            name='azimuth',
            field=models.IntegerField(blank=True, null=True, validators=[django.core.validators.MaxValueValidator(360), django.core.validators.MinValueValidator(0)]),
        ),
        migrations.AlterField(
            model_name='plantequipment',
            name='tilt',
            field=models.IntegerField(blank=True, null=True, validators=[django.core.validators.MaxValueValidator(90), django.core.validators.MinValueValidator(0)]),
        ),
    ]

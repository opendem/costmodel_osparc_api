# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations

from core.models import (
    PVMarketSector, InverterType, ModuleType, MountingLocation,
    MountingType, RoofSlopeType, RoofType, InspectionTechnique,
    TrackingType
)

def forwards_func(apps, schema_editor):
    ## For PVMarketSector ##
    pv_market_list = [
        'Residential', 'Commercial', 'Utility'
    ]

    PVMarketSector = apps.get_model("core", "PVMarketSector")

    pv_market_instance_list = [
        PVMarketSector(name=pv, description=pv) for pv in pv_market_list
    ]

    PVMarketSector.objects.bulk_create(pv_market_instance_list)

    ## For InverterType ##
    inverter_types = [
        'String Inverter', 'Central Inverter', 'Microinverter'
    ]
    InverterType = apps.get_model("core", "InverterType")
    inverter_types_instance_list = [
        InverterType(name=inverter, description=inverter, replacement_cost=.00) for inverter in inverter_types
    ]
    InverterType.objects.bulk_create(inverter_types_instance_list)

    ## For ModuleType ##
    module_types = [
        'c_Si', 'mono_Si', 'p_Si', 'TFSC', 'a_Si', 'CdTe', 'CIGS', 'BIPV'
    ]
    ModuleType = apps.get_model("core", "ModuleType")
    module_types_instances = [
        ModuleType(name=mt, description=mt, degradation_rate=.00) for mt in module_types
    ]
    ModuleType.objects.bulk_create(module_types_instances)

    ## For MountingLocation ##
    mounting_locations = [
        'Rooftop', 'Ground Mount'
    ]
    MountingLocation = apps.get_model("core", "MountingLocation")
    mounting_location_instances = [
        MountingLocation(name=location, description=location) for location in mounting_locations
    ]
    MountingLocation.objects.bulk_create(mounting_location_instances)

    ## For MountingType ##
    mounting_types = [
        'Ballasted', 'Attached', 'Bipv', 'Pole'
    ]
    MountingType = apps.get_model("core", "MountingType")
    mounting_type_instances = [
        MountingType(name=mounting_type, description=mounting_type) for mounting_type in mounting_types
    ]
    MountingType.objects.bulk_create(mounting_type_instances)

    ## For RoofSlopeType ##
    roof_slopes = [
        'Flat', 'Sloped', 'Steep'
    ]
    RoofSlopeType = apps.get_model("core", "RoofSlopeType")
    slope_type_instances = [
        RoofSlopeType(name=slope, description=slope) for slope in roof_slopes
    ]
    RoofSlopeType.objects.bulk_create(slope_type_instances)

    ## For RoofType ##
    roof_types = [
        'Thermoplastic Polyolefin', 'Ethylene Propylene Diene Terpolymer',
        'Poly Vinyl Chloride', 'Built Up Bituminous', 'SBS Asphalt Shingle',
        'Wood Shingle', 'Composite Shingle', 'Slate', 'Metal Roof', 'Tile'
    ]
    RoofType = apps.get_model("core", "RoofType")
    roof_type_instances = [
        RoofType(name=rt, description=rt, repair_material_cost=.00, repair_labor_hours=.00) for rt in roof_types
    ]
    RoofType.objects.bulk_create(roof_type_instances)

    ## For InspectionTechnique ##
    inspection_techniques = [
        'Aerial', 'Roof Walk', 'Ground Walk'
    ]
    InspectionTechnique = apps.get_model("core", "InspectionTechnique")
    insp_tech_instances = [
        InspectionTechnique(name=it, description=it) for it in inspection_techniques
    ]
    InspectionTechnique.objects.bulk_create(insp_tech_instances)

    ## For TrackingType ##
    tracking_types = [
        'No Tracking', 'Single Axis Tracking', 'Dual Axis Tracking'
    ]
    TrackingType = apps.get_model("core", "TrackingType")
    tracking_instances = [
        TrackingType(name=tracking, description=tracking) for tracking in tracking_types
    ]
    TrackingType.objects.bulk_create(tracking_instances)

def reverse_func(apps, schema_editor):
    ## For PVMarketSector ##
    PVMarketSector = apps.get_model("core", "PVMarketSectore")
    pv_market_list = [
        'Residential', 'Commercial', 'Utility'
    ]
    for pv in pv_market_list:
        PVMarketSector.objects.filter(name=pv, description=pv).delete()

    ## For Inverter Type ##
    InverterType = apps.get_model("core", "InverterType")
    inverter_types = [
        'String Inverter', 'Central Inverter', 'Microinverter'
    ]
    for inverter in inverter_types:
        InverterType.objects.filter(name=inverter, description=inverter, replacement_cost=.00).delete()

    ## For ModuleType ##
    ModuleType = apps.get_model("core", "ModuleType")
    module_types = [
        'c_Si', 'mono_Si', 'p_Si', 'TFSC', 'a_Si', 'CdTe', 'CIGS', 'BIPV'
    ]
    for module in module_types:
        ModuleType.objects.filter(name=module, description=module, degradation_rate=.00).delete()  

    ## For MountingLocation ##
    MountingLocation = apps.get_model("core", "MountingLocation")
    mounting_locations = [
        'Rooftop', 'Ground Mount'
    ]
    for location in mounting_locations:
        MountingLocation.objects.filter(name=location, description=location).delete()

    ## For MountingType ##
    MountingType = apps.get_model("core", "MountingType")
    mounting_types = [
        'Ballasted', 'Attached', 'Bipv', 'Pole'
    ]
    for mounting_type in mounting_types:
        MountingType.objects.filter(name=mounting_type, description=mounting_type).delete()

    ## For RoofSlopeType ##
    RoofSlopeType = apps.get_model("core", "RoofSlopeType")
    roof_slopes = [
        'Flat', 'Sloped', 'Steep'
    ]
    for slope in roof_slopes:
        RoofSlopeType.objects.filter(name=slope, description=slope).delete()

    ## For RoofType ##
    RoofType = apps.get_model("core", "RoofType")
    roof_types = [
        'Thermoplastic Polyolefin', 'Ethylene Propylene Diene Terpolymer',
        'Poly Vinyl Chloride', 'Built Up Bituminous', 'SBS Asphalt Shingle',
        'Wood Shingle', 'Composite Shingle', 'Slate', 'Metal Roof', 'Tile'
    ]
    for rt in roof_types:
        RoofType.objects.filter(name=rt, 
                                description=rt, 
                                repair_material_cost=.00, 
                                repair_labor_hours=.00).delete()

    ## For InspectionTechnique ##
    InspectionTechnique = apps.get_model("core", "InspectionTechnique")
    inspection_techniques = [
        'Aerial', 'Roof Walk', 'Ground Walk'
    ]
    for it in inspection_techniques:
        InspectionTechnique.objects.filter(name=it, description=it).delete()

    ## For TrackingType ##
    TrackingType = apps.get_model("core", "TrackingType")
    tracking_types = [
        'No Tracking', 'Single Axis Tracking', 'Dual Axis Tracking'
    ]
    for tracking in tracking_types:
        TrackingType.objects.filter(name=tracking, description=tracking).delete()

class Migration(migrations.Migration):

    dependencies = [
        ('core', '0001_initial'),
    ]

    operations = [
        migrations.RunPython(forwards_func, reverse_func),
    ]
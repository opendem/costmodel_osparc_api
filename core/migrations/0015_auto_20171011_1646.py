# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2017-10-11 16:46
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0014_auto_20171011_1632'),
    ]

    operations = [
        migrations.AlterField(
            model_name='plant',
            name='plant_origin_id',
            field=models.CharField(blank=True, max_length=255, null=True),
        ),
    ]

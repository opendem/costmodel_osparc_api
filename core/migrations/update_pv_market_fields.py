# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations

from core.models import (
    PVMarketSector, InverterType, ModuleType, MountingLocation,
    MountingType, RoofSlopeType, RoofType, InspectionTechnique,
    TrackingType
)

def forwards_func(apps, schema_editor):
    ## For PVMarketSector ##
    pv_market_list = [
        'Community Solar', 'Industrial', 'Agricultural'
    ]

    PVMarketSector = apps.get_model("core", "PVMarketSector")

    pv_market_instance_list = [
        PVMarketSector(name=pv, description=pv) for pv in pv_market_list
    ]

    PVMarketSector.objects.bulk_create(pv_market_instance_list)

def reverse_func(apps, schema_editor):
    ## For PVMarketSector ##
    PVMarketSector = apps.get_model("core", "PVMarketSectore")
    pv_market_list = [
        'Community Solar', 'Industrial', 'Agricultural'
    ]
    for pv in pv_market_list:
        PVMarketSector.objects.filter(name=pv, description=pv).delete()

class Migration(migrations.Migration):

    dependencies = [
        ('core', '0001_initial'),
    ]

    operations = [
        migrations.RunPython(forwards_func, reverse_func),
    ]

# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations

from core.models import EnvironmentalCondition

def forwards_func(apps, schema_editor):
    ec_list = [
        'SNOW', 'HUMID', 'HOT', 'POLLEN', 'HIGH_WIND',
        'HAIL', 'SALT_AIR', 'DIESEL_SOOT', 'INDUSTRIAL_EMISSIONS',
        'BIRD_POPULATION', 'CONSTRUCTION_SITE_NEARBY', 'SAND_OR_DUST',
        'HIGH_INSOLATION'
    ]
    EnvironmentalCondition = apps.get_model("core", "EnvironmentalCondition")

    ec_instance_list = [
        EnvironmentalCondition(name=ec, description=ec) for ec in ec_list
    ]

    EnvironmentalCondition.objects.bulk_create(ec_instance_list)


def reverse_func(apps, schema_editor):
    # so reverse_func() should delete them.
    EnvironmentalCondition = apps.get_model("core", "EnvironmentalCondition")
    ec_list = [
        'SNOW', 'HUMID', 'HOT', 'POLLEN', 'HIGH_WIND',
        'HAIL', 'SALT_AIR', 'DIESEL_SOOT', 'INDUSTRIAL_EMISSIONS',
        'BIRD_POPULATION', 'CONSTRUCTION_SITE_NEARBY', 'SAND_OR_DUST',
        'HIGH_INSOLATION'
    ]

    for ec in ec_list:
        EnvironmentalCondition.objects.filter(name=ec, description=ec).delete()

class Migration(migrations.Migration):

    dependencies = [
        ('core', '0001_initial'),
    ]

    operations = [
        migrations.RunPython(forwards_func, reverse_func),
    ]

from rest_framework import permissions
from django.db.models import Q


class UserPermissions(permissions.BasePermission):

    def has_permission(self, request, view):

        # Allow get requests for all
        if request.method == 'GET':
            return True
        else:
            if request.user.user_permission.name == 'Write' or request.user.user_permission.name == 'Admin' or request.user.is_superuser:
                return True

    def has_object_permission(self, request, view, obj):

        # Allow get requests for all
        if request.method == 'GET':
            return True
        else:
            if request.user.user_permission.name == 'Write' or request.user.user_permission.name == 'Admin' or request.user.is_superuser:
                return True


class DataProviderPermissions(permissions.BasePermission):

    def has_permission(self, request, view):

        # Allow get requests for all
        if request.method == 'GET':
            return True
        else:
            if request.user.user_permission.name == 'Write' or request.user.user_permission.name == 'Write Data' or request.user.user_permission.name == 'Admin' or request.user.is_superuser:
                return True

    def has_object_permission(self, request, view, obj):

        # Allow get requests for all
        if request.method == 'GET':
            return True
        else:
            if request.user.user_permission.name == 'Write' or request.user.user_permission.name == 'Write Data' or request.user.user_permission.name == 'Admin' or request.user.is_superuser:
                return True


class IsSuperUser(permissions.BasePermission):

    def has_permission(self, request, view):

        if request.user.is_superuser:
            return True
        else:
            return False

    def has_object_permission(self, request, view, obj):

        if request.user.is_superuser:
            return True
        else:
            return False


class CanEditEnnumerations(permissions.BasePermission):

    def has_permission(self, request, view):

        # Allow get requests for all
        if request.method == 'GET':
            return True
        elif request.method != 'GET' and request.user.is_superuser:
            return True
        else:
            return False

    def has_object_permission(self, request, view, obj):

        # Allow get requests for all
        if request.method == 'GET':
            return True
        elif request.method != 'GET' and request.user.is_superuser:
            return True
        else:
            return False


class IsAdminUser(permissions.BasePermission):

    def has_permission(self, request, view):

        if request.user.is_superuser or request.user.user_permission.name == 'Admin':
            return True
        else:
            return False

    def has_object_permission(self, request, view, obj):

        if request.user.is_superuser or request.user.user_permission.name == 'Admin':
            return True
        else:
            return False


class BelongsToOrganization(permissions.BasePermission):

    def has_permission(self, request, view):

        if not request.resolver_match.kwargs.get('org_pk'):
            org_pk = request.resolver_match.kwargs.get('pk')
        else:
            org_pk = request.resolver_match.kwargs.get('org_pk')

        print(org_pk)

        if request.user.is_superuser or request.user.organization_id == long(org_pk):
            return True
        # else:
        #     return False

        # Allow default instances accessible as readonly
        elif request.method == 'GET' and long(org_pk) == 1:
            return True
        else:
            return False

    def has_object_permission(self, request, view, obj):

        if not request.resolver_match.kwargs.get('org_pk'):
            org_pk = request.resolver_match.kwargs.get('pk')
        else:
            org_pk = request.resolver_match.kwargs.get('org_pk')

        if request.user.is_superuser or request.user.organization_id == long(org_pk):
            return True

        # else:
        #     return False

        # Allow default instances accessible as readonly
        elif request.method == 'GET' and long(org_pk) == 1:
            return True
        else:
            return False


class HasCostModelLicense(permissions.BasePermission):

    def has_permission(self, request, view):

        if request.user.organization.cost_model_license is True:
            return True
        else:
            return False

    def has_object_permission(self, request, view, obj):

        if request.user.organization.cost_model_license is True:
            return True
        else:
            return False


class HasOsparcLicense(permissions.BasePermission):

    def has_permission(self, request, view):

        if request.user.organization.osparc_license is True:
            return True
        else:
            return False

    def has_object_permission(self, request, view, obj):

        if request.user.organization.osparc_license is True:
            return True
        else:
            return False



from django.conf.urls import url
from . import views
from django.conf.urls import include
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [

    # Authentication and Authorization

    url(r'^permission_denied/$', views.permission_denied),
    url(r'^unauthorized/$', views.unauthorized, name='unauthorized'),

    url(r'^login/$', views.login_view),
    url(r'^logout/$', views.logout_view),
    url(r'^reset_password/$', views.reset_password_view),
    url(r'^forgot-password/$', views.forgot_password_view),
    url(r'^forgot-password-complete/$', views.forgot_password_complete),

    # Organizations, User Permissions and Users

    url(r'^organizations/$', views.OrganizationList.as_view(), name='organization_list'),
    url(r'^organizations/(?P<pk>[0-9]+)/$', views.OrganizationDetail.as_view()),

    url(r'^user_permissions/$', views.UserPermissionList.as_view()),
    url(r'^user_permissions/(?P<pk>[0-9]+)/$', views.UserPermissionDetail.as_view()),

    url(r'^users/$', views.UserList.as_view()),
    url(r'^users/(?P<pk>[0-9]+)/$', views.UserDetail.as_view()),

    url(r'^organizations/(?P<org_pk>[0-9]+)/users/$', views.OrganizationUserList.as_view()),
    url(r'^organizations/(?P<org_pk>[0-9]+)/users/(?P<pk>[0-9]+)/$', views.OrganizationUserDetail.as_view()),

    # Plants

    url(r'^organizations/(?P<org_pk>[0-9]+)/plants/$', views.PlantList.as_view(), name='plant_list'),
    url(r'^organizations/(?P<org_pk>[0-9]+)/no-pagination-plants/$', views.plant_dropdown),
    url(r'^organizations/(?P<org_pk>[0-9]+)/plants/(?P<pk>[0-9]+)/$', views.PlantDetail.as_view()),
    url(r'^plant/count/(?P<org_pk>[0-9]+)/$', views.PlantCountView.as_view(), name='plant-count'),

    url(r'^plant_equipments/$', views.PlantEquipmentList.as_view()),
    url(r'^plant_equipments/(?P<pk>[0-9]+)/$', views.PlantEquipmentDetail.as_view()),

    url(r'^plant_finances/$', views.PlantFinanceList.as_view()),
    url(r'^plant_finances/(?P<pk>[0-9]+)/$', views.PlantFinanceDetail.as_view()),

    # Enumerations

    url(r'^inverter_types/$', views.InverterTypeList.as_view()),
    url(r'^inverter_types/(?P<pk>[0-9]+)/$', views.InverterTypeDetail.as_view()),

    url(r'^module_types/$', views.ModuleTypeList.as_view(), name='module_type_list'),
    url(r'^module_types/(?P<pk>[0-9]+)/$', views.ModuleTypeDetail.as_view()),

    url(r'^mounting_locations/$', views.MountingLocationList.as_view(), name='mounting_location_list'),
    url(r'^mounting_locations/(?P<pk>[0-9]+)/$', views.MountingLocationDetail.as_view()),

    url(r'^pv_market_sectors/$', views.PVMarketSectorList.as_view(), name='market_sector_list'),
    url(r'^pv_market_sectors/(?P<pk>[0-9]+)/$', views.PVMarketSectorDetail.as_view()),

    url(r'^environmental_conditions/$', views.EnvironmentalConditionList.as_view(), name='environmental_condition_list'),
    url(r'^environmental_conditions/(?P<pk>[0-9]+)/$', views.EnvironmentalConditionDetail.as_view()),

    url(r'^roof_types/$', views.RoofTypeList.as_view(), name='roof_type_list'),
    url(r'^roof_types/(?P<pk>[0-9]+)/$', views.RoofTypeDetail.as_view()),

    url(r'^roof_slope_types/$', views.RoofSlopeTypeList.as_view(), name='roof_slope_list'),
    url(r'^roof_slope_types/(?P<pk>[0-9]+)/$', views.RoofSlopeTypeDetail.as_view()),

    url(r'^mounting_types/$', views.MountingTypeList.as_view(), name='mounting_type_list'),
    url(r'^mounting_types/(?P<pk>[0-9]+)/$', views.MountingTypeDetail.as_view()),

    url(r'^tracking_types/$', views.TrackingTypeList.as_view(), name='tracking_type_list'),
    url(r'^tracking_types/(?P<pk>[0-9]+)/$', views.TrackingTypeDetail.as_view()),

    url(r'^inspection_techniques/$', views.InspectionTechniqueList.as_view(), name='inspection_technique_list'),
    url(r'^inspection_techniques/(?P<pk>[0-9]+)/$', views.InspectionTechniqueDetail.as_view()),

    url(r'^weather_sources/$', views.WeatherSourceList.as_view()),
    url(r'^weather_sources/(?P<pk>[0-9]+)/$', views.WeatherSourceDetail.as_view()),

    url(r'^csi-import/$', views.csi_import_view),
    ]

urlpatterns += [
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
]
# FOR MEDIA FILES #
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

from __future__ import unicode_literals

import uuid

from django.db import models
from django.core.mail import send_mail
from django.contrib.auth.models import PermissionsMixin
from django.contrib.auth.base_user import BaseUserManager
from django.contrib.auth.base_user import AbstractBaseUser
from django.utils.translation import ugettext_lazy as _
from django.core.validators import MaxValueValidator, MinValueValidator


class UserManager(BaseUserManager):
    use_in_migrations = True

    def _create_user(self, email, password, **extra_fields):
        """
        Creates and saves a User with the given email and password.
        """
        if not email:
            raise ValueError('The given email must be set')
        email = self.normalize_email(email)
        user = self.model(email=email, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, email, password=None, **extra_fields):
        extra_fields.setdefault('is_superuser', False)
        return self._create_user(email, password, **extra_fields)

    def create_superuser(self, email, password, **extra_fields):
        extra_fields.setdefault('is_superuser', True)
        extra_fields.setdefault(
            'organization',
            # returns tuple (Object, Boolean) - only need Object.
            Organization.objects.get_or_create(
                name='Default', cost_model_license=True, osparc_license=True
            )[0]
        )
        extra_fields.setdefault(
            'user_permission',
            # returns tuple (Object, Boolean) - only need Object.
            UserPermission.objects.get_or_create(name='Admin')[0]
        )

        if extra_fields.get('is_superuser') is not True:
            raise ValueError('Superuser must have is_superuser=True.')

        return self._create_user(email, password, **extra_fields)


class Organization(models.Model):
    name = models.CharField(unique=True, max_length=45)
    description = models.CharField(max_length=254, blank=True, null=True)
    cost_model_license = models.BooleanField(default=False)
    osparc_license = models.BooleanField(default=False)

    def __str__(self):
        return self.name


class User(AbstractBaseUser, PermissionsMixin):
    email = models.EmailField(_('email address'), unique=True)
    first_name = models.CharField(_('first name'), max_length=30, blank=True)
    last_name = models.CharField(_('last name'), max_length=30, blank=True)
    date_joined = models.DateTimeField(_('date joined'), auto_now_add=True)
    is_active = models.BooleanField(_('active'), default=True)
    organization = models.ForeignKey(Organization, on_delete=models.CASCADE)
    user_permission = models.ForeignKey('UserPermission')
    failure_analysis_available = models.BooleanField(default=False)
    is_staff = models.BooleanField(_('staff status'), default=True)

    objects = UserManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    class Meta:
        verbose_name = _('user')
        verbose_name_plural = _('users')

    def get_full_name(self):
        '''
        Returns the first_name plus the last_name, with a space in between.
        '''
        full_name = '%s %s' % (self.first_name, self.last_name)
        return full_name.strip()

    def get_short_name(self):
        '''
        Returns the short name for the user.
        '''
        return self.first_name

    def email_user(self, subject, message, from_email=None, **kwargs):
        '''
        Sends an email to this User.
        '''
        send_mail(subject, message, from_email, [self.email], **kwargs)


class UserPermission(models.Model):
    name = models.CharField(unique=True, max_length=45)
    description = models.CharField(max_length=254, blank=True, null=True)

    def __str__(self):
        return self.name

def get_default_weather_source():
    return WeatherSource.objects.get(id=6).id


class Plant(models.Model):
    uuid = models.CharField(max_length=254, blank=True, null=True)
    name = models.CharField(max_length=45)
    description = models.CharField(max_length=254, blank=True, null=True)
    activation_date = models.DateField()

    address_line_1 = models.CharField(max_length=254, blank=True, null=True)
    address_line_2 = models.CharField(max_length=254, blank=True, null=True)
    city = models.ForeignKey('costmodel.City', blank=True, null=True)
    county = models.CharField(max_length=100, blank=True, null=True)
    state = models.ForeignKey('costmodel.State', blank=True, null=True)
    country = models.CharField(max_length=100, blank=True, null=True)
    postal_code = models.CharField(max_length=6, blank=True, null=True)
    latitude = models.CharField(max_length=16, blank=True, null=True)
    longitude = models.CharField(max_length=16, blank=True, null=True)
    time_zone = models.CharField(max_length=64, blank=True, null=True)

    dc_rating = models.FloatField(blank=True, null=True)     # kilo Watts
    derate_factor = models.FloatField(blank=True, null=True)
    energy_yield = models.FloatField(blank=True, null=True)

    storage_original_capacity = models.FloatField(blank=True, null=True)
    storage_current_capacity = models.FloatField(blank=True, null=True)
    storage_state_of_charge = models.FloatField(blank=True, null=True)

    pv_market_sector = models.ForeignKey('PVMarketSector', models.SET_NULL, blank=True, null=True)
    plant_finance = models.OneToOneField('PlantFinance', on_delete=models.CASCADE, blank=True, null=True)
    plant_equipment = models.OneToOneField('PlantEquipment', on_delete=models.CASCADE, blank=True, null=True)
    plant_report = models.OneToOneField('osparc.PlantReport', on_delete=models.CASCADE, blank=True, null=True)
    organization = models.ForeignKey(Organization, related_name='plant', on_delete=models.CASCADE)
    environmental_condition = models.ManyToManyField('EnvironmentalCondition', blank=True, null=True)
    cost_model = models.ManyToManyField('costmodel.CostModel', related_name='plant', blank=True, null=True)
    active_cost_model = models.ForeignKey('costmodel.CostModel', related_name='active_cost_model', blank=True, null=True)

    weather_source = models.ForeignKey('WeatherSource', default=get_default_weather_source, null=True, blank=True)
    upload_activity = models.OneToOneField('osparc.UploadActivity', on_delete=models.CASCADE, blank=True, null=True)
    record_status = models.IntegerField(default=1)
    version_creation_time = models.DateTimeField(auto_now_add=True)
    version_id = models.IntegerField(default=1)
    solar_anywhere_site = models.CharField(max_length=64, blank=True, null=True)
    # limit_choices_to = {'costmodel': 'costmodel'},
    # limit_choices_to = {Q(organization=2) | Q(organization='self.org_pk')}
    plant_origin = models.CharField(max_length=255, blank=True, null=True)
    plant_origin_id = models.CharField(max_length=255, blank=True, null=True)
    plant_creation_date = models.DateTimeField(auto_now_add=True, blank=True, null=True)

    def __str__(self):
        return self.name

    def _get_site_area(self):
        try:
            return "%.2f" % ((1.94 / float(self.plant_equipment.module_power)) * float(273 / self.plant_equipment.gcr) * self.dc_rating / 1000)
        except TypeError:
            return 0

    site_area = property(_get_site_area)


class PlantFinance(models.Model):
    system_installed_cost = models.FloatField(blank=True, null=True)

    analysis_period = models.IntegerField(blank=True, null=True)
    discount_rate = models.FloatField(blank=True, null=True)
    inflation_rate = models.FloatField(blank=True, null=True)
    desired_confidence_that_reserve_covers_cost = models.FloatField(blank=True, null=True)

    epc_warranty = models.IntegerField(blank=True, null=True)
    inverter_warranty = models.IntegerField(blank=True, null=True)
    module_warranty = models.IntegerField(blank=True, null=True)
    monitoring_warranty = models.IntegerField(blank=True, null=True)

    working_hours = models.IntegerField(blank=True, null=True)


class PlantEquipment(models.Model):
    inverter_type = models.ForeignKey('InverterType', models.SET_NULL, blank=True, null=True)
    inverter_replacement_cost = models.FloatField(blank=True, null=True)
    inverter_capacity = models.FloatField(blank=True, null=True)
    number_of_inverters = models.IntegerField(blank=True, null=True)

    number_of_transformers = models.IntegerField(blank=True, null=True)

    module_type = models.ForeignKey('ModuleType', models.SET_NULL, blank=True, null=True)
    module_degradation_rate = models.FloatField(blank=True, null=True)  # per year
    balance_of_system_degradation_rate = models.FloatField(blank=True, null=True)  # per year

    module_efficiency = models.FloatField(blank=True, null=True)
    module_power = models.FloatField(blank=True, null=True)
    modules_per_row = models.IntegerField(blank=True, null=True)
    modules_per_string = models.IntegerField(blank=True, null=True)

    mounting_type = models.ForeignKey('MountingType', models.SET_NULL, blank=True, null=True)
    mounting_location = models.ForeignKey('MountingLocation', models.SET_NULL, blank=True, null=True)

    roof_slope_type = models.ForeignKey('RoofSlopeType', models.SET_NULL, blank=True, null=True)
    roof_type = models.ForeignKey('RoofType', models.SET_NULL, blank=True, null=True)

    inspection_technique = models.ForeignKey('InspectionTechnique', models.SET_NULL, blank=True, null=True)
    tracking = models.ForeignKey('TrackingType', models.SET_NULL, blank=True, null=True)
    array_area_per_roof_attachment = models.FloatField(blank=True, null=True)
    combiner_boxes_per_dcd = models.IntegerField(blank=True, null=True)
    foundations_per_row = models.IntegerField(blank=True, null=True)
    gcr = models.FloatField(blank=True, null=True)

    rows_per_tracked_block = models.IntegerField(blank=True, null=True)
    strings_per_combiner_box = models.IntegerField(blank=True, null=True)
    tilt = models.IntegerField(
        blank=True, null=True,
        validators=[MaxValueValidator(360), MinValueValidator(0)]
    )
    azimuth = models.IntegerField(
        blank=True, null=True,
        validators=[MaxValueValidator(360), MinValueValidator(0)]
     )

    def _get_array_area(self):
        try:
            return float(self.plant.dc_rating / (self.module_efficiency / 100))
        except TypeError:
            return 0

    array_area = property(_get_array_area)

    def _get_number_of_modules(self):
        try:
            return int(round((self.plant.dc_rating * 1000) / self.module_power))
        except TypeError:
            return 0

    number_of_modules = property(_get_number_of_modules)

    def _get_number_of_strings(self):
        try:
            return int(round((self.plant.dc_rating * 1000) / self.module_power / self.modules_per_string))
        except TypeError:
            return 0

    number_of_strings = property(_get_number_of_strings)

    def _get_number_of_combiner_boxes(self):
        if self.strings_per_combiner_box == 0:
            return 0
        else:
            try:
                result = self.number_of_strings / self.strings_per_combiner_box
                return result
            except TypeError:
                return 0

    number_of_combiner_boxes = property(_get_number_of_combiner_boxes)

    def _get_number_of_dc_disconnects(self):
        try:
            if self.number_of_combiner_boxes == 0:
                return 1
            elif self.combiner_boxes_per_dcd == 0:
                return 1
            else:
                return int(round(self.number_of_combiner_boxes / self.combiner_boxes_per_dcd))
        except TypeError:
            return 0

    number_of_dc_disconnects = property(_get_number_of_dc_disconnects)

    def _get_number_of_roof_attachments(self):
        if self.mounting_type.id == 1:
            return 0
        else:
            return int(round(self.array_area / float(self.array_area_per_roof_attachment)))

    number_of_roof_attachments = property(_get_number_of_roof_attachments)

    def _get_total_tracking_blocks(self):
        try:
            return int(round((self.plant.dc_rating * 1000) / (self.rows_per_tracked_block * self.modules_per_row * self.module_power)))
        except TypeError:
            return 0

    total_tracking_blocks = property(_get_total_tracking_blocks)

    def _get_total_rows(self):
        try:
            return int(round(self.rows_per_tracked_block * self.total_tracking_blocks))
        except TypeError:
            return 0

    total_rows = property(_get_total_rows)

    def _get_whole_system_degradation_rate(self):
        try:
            return self.module_degradation_rate + self.balance_of_system_degradation_rate
        except TypeError:
            return 0

    whole_system_degradation_rate = property(_get_whole_system_degradation_rate)


class PVMarketSector(models.Model):
    name = models.CharField(unique=True, max_length=45)
    description = models.CharField(max_length=254, blank=True, null=True)

    def __str__(self):
        return self.name


class InverterType(models.Model):
    name = models.CharField(unique=True, max_length=45)
    description = models.CharField(max_length=254, blank=True, null=True)
    replacement_cost = models.FloatField(blank=True, null=True)

    def __str__(self):
        return self.name


class ModuleType(models.Model):
    name = models.CharField(unique=True, max_length=45)
    description = models.CharField(max_length=254, blank=True, null=True)
    degradation_rate = models.FloatField(blank=True, null=True)

    def __str__(self):
        return self.name


class MountingLocation(models.Model):
    name = models.CharField(unique=True, max_length=45)
    description = models.CharField(max_length=254, blank=True, null=True)

    def __str__(self):
        return self.name


class EnvironmentalCondition(models.Model):
    name = models.CharField(unique=True, max_length=45)
    description = models.CharField(max_length=254, blank=True, null=True)

    def __str__(self):
        return self.name


class RoofType(models.Model):
    name = models.CharField(unique=True, max_length=45)
    description = models.CharField(max_length=254, blank=True, null=True)
    repair_material_cost = models.FloatField(blank=True, null=True)
    repair_labor_hours = models.FloatField(blank=True, null=True)

    def __str__(self):
        return self.name


class RoofSlopeType(models.Model):
    name = models.CharField(unique=True, max_length=45)
    description = models.CharField(max_length=254, blank=True, null=True)

    def __str__(self):
        return self.name


class MountingType(models.Model):
    name = models.CharField(unique=True, max_length=45)
    description = models.CharField(max_length=254, blank=True, null=True)

    def __str__(self):
        return self.name


class TrackingType(models.Model):
    name = models.CharField(unique=True, max_length=45)
    description = models.CharField(max_length=254, blank=True, null=True)

    def __str__(self):
        return self.name


class InspectionTechnique(models.Model):
    name = models.CharField(unique=True, max_length=45)
    description = models.CharField(max_length=254, blank=True, null=True)

    def __str__(self):
        return self.name


class WeatherSource(models.Model):
    name = models.CharField(unique=True, max_length=45)
    description = models.CharField(max_length=254, blank=True, null=True)

    def __str__(self):
        return self.name

class FileUploader(models.Model):
    task_id = models.CharField(max_length=1000)
    time_series = models.FileField(upload_to='time_series-data/')
    plant_create = models.FileField(upload_to='plant-create-data/')

import os
import csv
import requests
import json
import urllib2
import zipfile
import StringIO
from django.http import JsonResponse
from rest_framework import generics, filters, status, mixins, permissions
from core.permissions import *
from serializers import *
from django.views.decorators.csrf import ensure_csrf_cookie, csrf_exempt
from django.contrib.auth import authenticate, login, logout
from django.db import IntegrityError
from rest_framework.response import Response
from rest_framework.decorators import api_view
from .mixins import DepthSerializerMixin
from costmodel import models as costmodel_models
from osparc import models as osparc_models
from osparc import serializers as osparc_serializers
import datetime
from osparc.mixins import KPIs
from django.db.models import Q
from django.conf import settings
from filters import *
from core.tasks import schedule_import_plants_and_ts

from django.core.files.storage import default_storage
from django.core.files.base import ContentFile
from django.core import serializers

from rest_framework import parsers
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import viewsets
from rest_framework.renderers import JSONRenderer

## PAGINATION ##
from .pagination import (
    DefaultLimitOffsetPagination,
    DefaultPageNumberPagination
)

## FOR FORGOT EMAIL ##
from django.contrib.auth.tokens import default_token_generator
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.template.loader import render_to_string
from django.core.mail import send_mail


# Authentication and Authorization

@api_view(['GET'])
def permission_denied(request, version):

    return Response({'detail': 'Permission Denied'}, status=403)


@api_view(['GET'])
def unauthorized(request, version):

    return Response({'detail': 'Unauthorized'}, status=401)


@ensure_csrf_cookie
@api_view(['GET'])
def login_view(request, version):
    email = request.GET.get('email', '')
    username = email
    password = request.GET.get('password', '')
    user = authenticate(username=username, password=password)

    if user is not None:
        last_login = user.last_login
        if user.is_active:
            login(request, user)

            return Response({
                'status': 'success', 'id': user.id, 'email': user.email, 'organization_id': user.organization_id,
                'user_permission': user.user_permission.name,
                'failure_analysis_available': user.failure_analysis_available,
                'cost_model_license': user.organization.cost_model_license,
                'osparc_license': user.organization.osparc_license,
                'is_superuser': user.is_superuser,
                'last_login': last_login})
        else:
            return Response({
                'status': 'error: disabled account', 'id': '', 'email': '', 'organization_id': '',
                'user_permission': '', 'failure_analysis_available': '',
                'cost_model_license': '', 'osparc_license': ''})
    else:
        return Response({
            'status': 'error: invalid credentials', 'id': '', 'email': '', 'organization_id': '', 'user_permission': '',
            'failure_analysis_available': '', 'cost_model_license': '', 'osparc_license': ''})


@api_view(['GET'])
def logout_view(request, version):

    logout(request)
    return Response({'status': 'success'})


@api_view(['GET'])
def reset_password_view(request, version):
    email = request.GET.get('email', '')
    username = email
    password = request.GET.get('password', '')
    new_password = request.GET.get('new_password', '')

    user = authenticate(email=email, password=password)

    if user is not None:
        if user.is_active:
            try:
                user = User.objects.get(email=email)
                user.set_password(new_password)
                user.save()
                return Response({'status': 'success'})
            except User.DoesNotExist:
                return Response({'status': 'error: User does not exist'})
        else:
            return Response({'status': 'error: disabled account'})
    else:
        return Response({'status': 'error: invalid credentials'})


@api_view(['GET'])
def forgot_password_view(request, version):
    email = request.GET.get('email', '')
    user = User.objects.get(email=email)
    
    if user is not None:
        data = {
            'email': user.email,
            'domain': settings.SUNSPEC_DOMAIN, #ec2-18-221-93-174.us-east-2.compute.amazonaws.com
            'site_name': 'APSuite',
            'uid': urlsafe_base64_encode(force_bytes(user.pk)),
            'user': user,
            'token': default_token_generator.make_token(user),
            'protocol': 'http',
        }
        
        subject_template_name = 'password_reset_subject.txt'
        email_template_name = 'password_reset_email.html'
        email_template = render_to_string(email_template_name, data)
        send_mail(
            'Password Reset for APSuite',
            'Password Reset for APSuite',
            settings.DEFAULT_FROM_EMAIL,
            [user.email],
            html_message=email_template,
            fail_silently=False,
        )
        return Response({'status': 'success'})
    else:
        return Response({'status': 'Error: your email does not exist in our system, please try again with a different email.'})

@api_view(['GET'])
def forgot_password_complete(request, version):
    email = request.GET.get('email', '')
    password = request.GET.get('password')
    uidb64 = request.GET.get('uidb64')
    token = request.GET.get('token')
    
    assert uidb64 is not None and token is not None
    try:
        uid = urlsafe_base64_decode(uidb64)
        user = User.objects.get(pk=uid)
    except:
        user = None
    if user is not None and default_token_generator.check_token(user, token):
        user.set_password(password)
        user.save()
        return Response({'status': 'success'})
    else:
        return Response({'status': 'Password did not save correctly'})

# Organizations, User Permissions and Users

class UserList(DepthSerializerMixin, generics.ListCreateAPIView):

    queryset = User.objects.all()
    serializer_class = UserSerializer
    # filter_backends = (filters.DjangoFilterBackend,)
    # filter_fields = ('email',)
    filter_class = UserFilter
    permission_classes = (permissions.IsAuthenticated, IsSuperUser)


class UserDetail(DepthSerializerMixin, generics.RetrieveUpdateDestroyAPIView):

    queryset = User.objects.all()
    serializer_class = UserSerializer
    permission_classes = (permissions.IsAuthenticated,)


class OrganizationUserList(DepthSerializerMixin, generics.ListCreateAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer
    permission_classes = (permissions.IsAuthenticated, BelongsToOrganization, IsAdminUser)
    filter_class = OrganizationUserFilter

    def get_queryset(self):
        org_pk = self.kwargs['org_pk']
        return self.queryset.filter(organization=org_pk)


class OrganizationUserDetail(DepthSerializerMixin, generics.RetrieveUpdateDestroyAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer
    permission_classes = (
        permissions.IsAuthenticated, BelongsToOrganization, IsAdminUser
    )

    def get_queryset(self):
        org_pk = self.kwargs['org_pk']
        return self.queryset.filter(organization=org_pk)


class OrganizationList(DepthSerializerMixin, generics.ListCreateAPIView):
    queryset = Organization.objects.all()
    serializer_class = OrganizationSerializer
    # filter_backends = (filters.DjangoFilterBackend,)
    # filter_fields = ('name',)
    filter_class = OrganizationFilter
    permission_classes = (permissions.IsAuthenticated, IsSuperUser)


class OrganizationDetail(generics.RetrieveUpdateDestroyAPIView):

    queryset = Organization.objects.all()
    serializer_class = OrganizationSerializer
    permission_classes = (permissions.IsAuthenticated, BelongsToOrganization, CanEditEnnumerations)


class UserPermissionList(generics.ListCreateAPIView):
    queryset = UserPermission.objects.all()
    serializer_class = UserPermissionSerializer
    filter_class = UserPermissionFilter
    permission_classes = (permissions.IsAuthenticated, UserPermissions)


class UserPermissionDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = UserPermission.objects.all()
    serializer_class = UserPermissionSerializer
    permission_classes = (permissions.IsAuthenticated, UserPermissions)


# Plants

class PlantList(DepthSerializerMixin, generics.ListCreateAPIView):

    queryset = Plant.objects.all()
    serializer_class = PlantSerializer
    permission_classes = (permissions.IsAuthenticated, BelongsToOrganization, UserPermissions)
    # filter_backends = (filters.DjangoFilterBackend,)
    filter_class = PlantFilter
    pagination_class = DefaultLimitOffsetPagination

    def get_queryset(self):
        org_pk = self.kwargs['org_pk']
        uuid = self.request.query_params.get('uuid', None)
        if uuid is not None:
            queryset = self.queryset.filter(Q(organization=org_pk) & Q(uuid=uuid))
        else:
            queryset = self.queryset.filter(organization=org_pk)
        return queryset

    def post(self, request, org_pk, version):
        serializer = PlantSerializer(data=request.data)
        if serializer.is_valid():
            print("Serializer is valid")
            plant = serializer.save()
            # plant = self.create(request)
            print(plant)
            print("Serializer saved")

            # Create a cost model and assign it as the active cost model
            print("Generating cost model")
            cost_model_id = generate_cm_for_plant(plant.id)
            print("Cost Model Generated")
            p = Plant.objects.get(id=plant.id)
            p.cost_model.add(costmodel_models.CostModel.objects.get(id=cost_model_id))
            p.save()
            print("cost model added")
            plant.active_cost_model = costmodel_models.CostModel.objects.get(id=cost_model_id)
            plant.save()
            print("active cost model added")

            # Create an Upload Activity and a Plant Report
            plant.upload_activity = osparc_models.UploadActivity.objects.create()
            plant.plant_report = osparc_models.PlantReport.objects.create()
            plant.save()

            # Generate a random UUID if not provided
            if plant.uuid is None or plant.uuid == "":
                plant.uuid = str(uuid.uuid4())
                plant.save()

            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class NoPaginationPlantList(generics.ListAPIView):

    queryset = Plant.objects.all()
    serializer_class = PlantSerializer
    permission_classes = (permissions.IsAuthenticated, BelongsToOrganization, UserPermissions)
    filter_class = PlantFilter

    def get_queryset(self):
        org_pk = self.kwargs['org_pk']
        uuid = self.request.query_params.get('uuid', None)
        if uuid is not None:
            queryset = self.queryset.filter(Q(organization=org_pk) & Q(uuid=uuid))
        else:
            queryset = self.queryset.filter(organization=org_pk)
        return queryset


@api_view(['GET'])
def plant_dropdown(request, version, org_pk):
    plants = Plant.objects.filter(organization=org_pk).values_list("id", "name", "cost_model")
    
    new_dict = {}
    for pl in plants:
        if pl[0] in new_dict:
            new_dict[pl[0]]['cost_model'].append(str(pl[-1]))
        else:
            new_dict[pl[0]] = {'id': pl[0], 'name': pl[1], 'cost_model': [str(pl[-1])]}

    return Response({
        'data': [ {'id': pl[-1]['id'], 'name': pl[-1]['name'], 'cost_model': ','.join(pl[-1]['cost_model'])} for pl in new_dict.items()]
    })


class PlantDetail(DepthSerializerMixin, generics.RetrieveUpdateDestroyAPIView):

    queryset = Plant.objects.all()
    serializer_class = PlantSerializer
    permission_classes = (permissions.IsAuthenticated, BelongsToOrganization, UserPermissions)

    def get_queryset(self):
        org_pk = self.kwargs['org_pk']
        return self.queryset.filter(organization=org_pk)

    def create_and_save_report(self, plant):
        myrecords = osparc_models.PlantTimeSeries.objects.filter(plant_id__exact=plant.id)

        plants = [plant]

        mixin = KPIs()
        kpis = mixin.calculateKPIs(plants, myrecords)

        try:
            firstmeasdate = kpis['PerformanceRatio']['firstday']
        except:
            firstmeasdate = None
        try:
            lastmeasdate = kpis['PerformanceRatio']['lastday']
        except:
            lastmeasdate = None
        try:
            monthlyyield = kpis['MonthlyYield']['mean']  # production yield kWh/kWdc
        except:
            monthlyyield = None
        try:
            pr = kpis['PerformanceRatio']['mean']  # performance ratio yf/yr
        except:
            pr = None
        try:
            soh = kpis['StorageStateOfHealth']['mean']
        except:
            soh = None

        plant_report_data = {'record_status': 1,
                             'create_time': datetime.datetime.now(),
                             'sample_interval': 'monthly',
                             'first_measurement_date': firstmeasdate,
                             'last_measurement_date': lastmeasdate,
                             'monthly_yield': monthlyyield,
                             'performance_ratio': pr,
                             'storage_state_of_health': soh}

        # we save the report regardless of whether there were time series elements - the report is complete
        serializer = osparc_serializers.PlantReportSerializer(plant.plant_report, data=plant_report_data)
        if serializer.is_valid():
            serializer.save()
        else:
            print "ERROR saving plant report:", serializer.errors

    def get(self, request, org_pk, pk, version):
        plant = Plant.objects.get(pk=pk)

        if plant.plant_report is not None and plant.plant_report.record_status == 9:
            # must create plant report
            self.create_and_save_report(plant)

            # the report was built & saved so we have to get the updated plant
            plant = Plant.objects.get(pk=pk)

        serializer = PlantSerializer(plant)
        return Response(serializer.data)


class PlantCountView(APIView):
    """
    View to return the count of plants
    """
    renderer_classes = (JSONRenderer, )

    def get(self, request, org_pk, version, format=None):
        plants = Plant.objects.filter(organization=org_pk)
        plant_count = plants.count()
        content = {'plant_count': plant_count}
        return Response(content)


def generate_cm_for_plant(plant_id):
    plant = Plant.objects.get(id=plant_id)
    cm_name = str(plant.name)+' Cost Model'

    # service = [9, 10, 11, 12, 13, 14, 17, 21, 22, 42, 48,
    #            56, 73, 75, 78, 79, 80, 81, 83, 85, 87, 88, 91,
    #            109, 110, 112, 113, 114, 115, 127, 142]

    service = [10, 11, 12, 13, 14, 15, 18, 22, 23, 43, 49,
               57, 74, 76, 79, 80, 81, 82, 84, 86, 88, 89, 92,
               110, 111, 113, 114, 115, 116, 128, 131]

    if plant.pv_market_sector is not None and plant.pv_market_sector.name != 'Residential':
        service.extend([3, 9, 19, 26, 30, 41, 42, 46, 47, 48, 50, 51, 52, 53, 54, 59, 70, 77])

    if plant.pv_market_sector is not None and plant.pv_market_sector.name == 'Utility':
        service.extend([44, 45])

    if plant.plant_equipment is not None:

        if plant.pv_market_sector is not None and plant.pv_market_sector.name == 'Utility' and plant.plant_equipment.mounting_location is not None and plant.plant_equipment.mounting_location.name == 'Ground Mount':
            service.append(130)

        if plant.plant_equipment.inverter_type is not None and plant.plant_equipment.inverter_type.name != 'Central Inverter':
            service.append(129)

        if plant.plant_equipment.inverter_type is not None and plant.plant_equipment.inverter_type.name == 'Central Inverter':
            service.extend([16, 17, 56, 58, 60, 61, 62, 63, 83, 85, 87,
                            91, 93, 94, 95, 96, 97, 98, 99, 100, 101,
                            102, 103, 104, 105, 106, 107, 108, 109, 126, 127])

        if plant.plant_equipment.mounting_location is not None and plant.plant_equipment.mounting_location.name == 'Ground Mount':
            service.extend([2, 7, 90, 112])

        if plant.plant_equipment.mounting_location is not None and plant.plant_equipment.mounting_location.name == 'Rooftop':
            service.extend([117, 118, 135])

        if plant.plant_equipment.inspection_technique is not None and plant.plant_equipment.inspection_technique.name != 'Aerial':
            service.extend([21, 72, 73, 75])

        if plant.plant_equipment.inspection_technique is not None and plant.plant_equipment.inspection_technique.name == 'Aerial':
            service.append(133)

        if plant.plant_equipment.inverter_type is not None and plant.plant_equipment.inverter_type.name == 'Microinverter':
            service.append(71)

        if plant.plant_equipment.inverter_type is not None and plant.plant_equipment.inverter_type.name != 'Microinverter':
            service.append(78)

        if plant.plant_equipment.mounting_type is not None and plant.plant_equipment.mounting_type.name == 'Ballasted':
            service.append(24)

        if plant.plant_equipment.inspection_technique is not None and plant.plant_equipment.inspection_technique.name == 'Aerial' and plant.pv_market_sector is not None and plant.pv_market_sector.name == 'Residential':
            service.append(25)

        if plant.plant_equipment.tracking is not None and plant.plant_equipment.tracking.name == 'Dual Axis Tracking':
            service.extend([40, 69])

        if plant.plant_equipment.tracking is not None and plant.plant_equipment.tracking.name != 'No Tracking':
            service.extend([29, 31, 32, 33, 34, 35, 36, 37, 38, 39, 55, 65, 67])

        if plant.plant_equipment.mounting_location is not None and plant.plant_equipment.mounting_location.name == 'Rooftop' and (plant.plant_equipment.mounting_type is not None and (plant.plant_equipment.mounting_type.name == 'Attached' or plant.plant_equipment.mounting_type.name == 'Both ballasted and attached')):
            service.append(132)

        if plant.plant_equipment.mounting_location is not None and plant.plant_equipment.mounting_location.name == 'Rooftop' and (plant.plant_equipment.mounting_type is not None and plant.plant_equipment.mounting_type.name != 'Attached'):
            service.append(135)

    if plant.environmental_condition is not None:
        for key in plant.environmental_condition.all():
            if key.name == 'Bird Populations':
                service.extend([8, 64])
            if key.name == 'Snow':
                service.append(4)
            if key.name == 'Pollen':
                service.append(6)
            if key.name == 'Sand/Dust':
                service.append(5)

    print("CREATING COST MODEL")

    cost_model = costmodel_models.CostModel.objects.create(name=cm_name, organization_id=plant.organization_id, defined_by_id=2)
    print("cost model created")
    cost_model.description = 'Automatically Generated for '+str(plant.name)
    cost_model.service_values = service
    cost_model.save()
    print("cost model saved")
    return cost_model.id


class PlantEquipmentList(DepthSerializerMixin, generics.ListCreateAPIView):
    queryset = PlantEquipment.objects.all()
    serializer_class = PlantEquipmentSerializer
    filter_class = PlantEquipmentFilter
    #permission_classes = (permissions.IsAuthenticated, UserPermissions)


class PlantEquipmentDetail(DepthSerializerMixin, generics.RetrieveUpdateDestroyAPIView):
    queryset = PlantEquipment.objects.all()
    serializer_class = PlantEquipmentSerializer
    #permission_classes = (permissions.IsAuthenticated, UserPermissions)


class PlantFinanceList(DepthSerializerMixin, generics.ListCreateAPIView):
    queryset = PlantFinance.objects.all()
    serializer_class = PlantFinanceSerializer
    filter_class = PlantFinanceFilter
    permission_classes = (permissions.IsAuthenticated, UserPermissions)


class PlantFinanceDetail(DepthSerializerMixin, generics.RetrieveUpdateDestroyAPIView):
    queryset = PlantFinance.objects.all()
    serializer_class = PlantFinanceSerializer
    permission_classes = (permissions.IsAuthenticated, UserPermissions)


# Enumerations
class InverterTypeList(generics.ListCreateAPIView):
    queryset = InverterType.objects.all()
    serializer_class = InverterTypeSerializer
    filter_class = InverterTypeFilter


class InverterTypeDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = InverterType.objects.all()
    serializer_class = InverterTypeSerializer


class ModuleTypeList(generics.ListCreateAPIView):
    queryset = ModuleType.objects.all()
    serializer_class = ModuleTypeSerializer
    filter_class = ModuleTypeFilter


class ModuleTypeDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = ModuleType.objects.all()
    serializer_class = ModuleTypeSerializer


class MountingLocationList(generics.ListCreateAPIView):

    queryset = MountingLocation.objects.all()
    serializer_class = MountingLocationSerializer
    filter_class = MountingLocationFilter


class MountingLocationDetail(generics.RetrieveUpdateDestroyAPIView):

    queryset = MountingLocation.objects.all()
    serializer_class = MountingLocationSerializer


class PVMarketSectorList(generics.ListCreateAPIView):

    queryset = PVMarketSector.objects.all()
    serializer_class = PVMarketSectorSerializer
    filter_class = PVMarketSectorFilter


class PVMarketSectorDetail(generics.RetrieveUpdateDestroyAPIView):

    queryset = PVMarketSector.objects.all()
    serializer_class = PVMarketSectorSerializer


class EnvironmentalConditionList(generics.ListCreateAPIView):

    queryset = EnvironmentalCondition.objects.all()
    serializer_class = EnvironmentalConditionSerializer
    filter_class = EnvironmentalConditionFilter


class EnvironmentalConditionDetail(generics.RetrieveUpdateDestroyAPIView):

    queryset = EnvironmentalCondition.objects.all()
    serializer_class = EnvironmentalConditionSerializer


class RoofTypeList(generics.ListCreateAPIView):

    queryset = RoofType.objects.all()
    serializer_class = RoofTypeSerializer
    filter_class = RoofTypeFilter


class RoofTypeDetail(generics.RetrieveUpdateDestroyAPIView):

    queryset = RoofType.objects.all()
    serializer_class = RoofTypeSerializer


class RoofSlopeTypeList(generics.ListCreateAPIView):

    queryset = RoofSlopeType.objects.all()
    serializer_class = RoofSlopeTypeSerializer
    filter_class = RoofSlopeTypeFilter


class RoofSlopeTypeDetail(generics.RetrieveUpdateDestroyAPIView):

    queryset = RoofSlopeType.objects.all()
    serializer_class = RoofSlopeTypeSerializer


class MountingTypeList(generics.ListCreateAPIView):

    queryset = MountingType.objects.all()
    serializer_class = MountingTypeSerializer
    filter_class = MountingTypeFilter


class MountingTypeDetail(generics.RetrieveUpdateDestroyAPIView):

    queryset = MountingType.objects.all()
    serializer_class = MountingTypeSerializer


class TrackingTypeList(generics.ListCreateAPIView):

    queryset = TrackingType.objects.all()
    serializer_class = TrackingTypeSerializer
    filter_class = TrackingTypeFilter


class TrackingTypeDetail(generics.RetrieveUpdateDestroyAPIView):

    queryset = TrackingType.objects.all()
    serializer_class = TrackingTypeSerializer


class InspectionTechniqueList(generics.ListCreateAPIView):

    queryset = InspectionTechnique.objects.all()
    serializer_class = InspectionTechniqueSerializer
    filter_class = InspectionTechniqueFilter


class InspectionTechniqueDetail(generics.RetrieveUpdateDestroyAPIView):

    queryset = InspectionTechnique.objects.all()
    serializer_class = InspectionTechniqueSerializer


class WeatherSourceList(generics.ListCreateAPIView):
    queryset = WeatherSource.objects.all()
    serializer_class = WeatherSourceSerializer
    filter_class = WeatherSourceFilter


class WeatherSourceDetail(generics.RetrieveUpdateDestroyAPIView):

    queryset = WeatherSource.objects.all()
    serializer_class = WeatherSourceSerializer


@api_view(['GET'])
def csi_import_view(request, version):
    plant_origin = request.GET.get('origin')
    org_id = request.GET.get('org_id')
    user_email = request.GET.get('user_email')
    if 'CSI' in plant_origin:
        plant_file_url = 'https://www.californiasolarstatistics.ca.gov/data_downloads/working/'
        req = requests.get(plant_file_url)
        if req.status_code == 200 and plant_origin and org_id:
            task = schedule_import_plants_and_ts.delay(plant_origin, org_id, user_email)
            return Response({'status': 'success'})
        else:
            return Response({'status': 'It looks like CSI data sheets are unavailable at this time, please try again later'})

    if 'PVOUTPUT' in plant_origin:
        # Need to ensure that the pvoutput.org API is working so we can do a test
        # request to validate that we can consume their API
        api_key = 'ac2ca9078472d50d677c965154840e829d61e942'
        get_system_url = 'https://pvoutput.org/service/r2/getsystem.jsp'
        payload = {
            'sid': '55789',
            'key': api_key,
            'sid1': '5473',
        }
        pvoutput_req = requests.get(get_system_url, params=payload)
        if pvoutput_req.status_code == 200:
            task = schedule_import_plants_and_ts.delay(
                plant_origin, 
                org_id, 
                user_email
            )
            return Response({'status': 'success'})
        else:
            return Response({'status': 'It looks like PVOutput.org data is unavailable at this time, please try again later'})


from core.models import Plant
from costmodel.models import City
import requests
import re

states = {
        'AK': 'Alaska',
        'AL': 'Alabama',
        'AR': 'Arkansas',
        'AS': 'American Samoa',
        'AZ': 'Arizona',
        'CA': 'California',
        'CO': 'Colorado',
        'CT': 'Connecticut',
        'DC': 'District of Columbia',
        'DE': 'Delaware',
        'FL': 'Florida',
        'GA': 'Georgia',
        'GU': 'Guam',
        'HI': 'Hawaii',
        'IA': 'Iowa',
        'ID': 'Idaho',
        'IL': 'Illinois',
        'IN': 'Indiana',
        'KS': 'Kansas',
        'KY': 'Kentucky',
        'LA': 'Louisiana',
        'MA': 'Massachusetts',
        'MD': 'Maryland',
        'ME': 'Maine',
        'MI': 'Michigan',
        'MN': 'Minnesota',
        'MO': 'Missouri',
        'MP': 'Northern Mariana Islands',
        'MS': 'Mississippi',
        'MT': 'Montana',
        'NA': 'National',
        'NC': 'North Carolina',
        'ND': 'North Dakota',
        'NE': 'Nebraska',
        'NH': 'New Hampshire',
        'NJ': 'New Jersey',
        'NM': 'New Mexico',
        'NV': 'Nevada',
        'NY': 'New York',
        'OH': 'Ohio',
        'OK': 'Oklahoma',
        'OR': 'Oregon',
        'PA': 'Pennsylvania',
        'PR': 'Puerto Rico',
        'RI': 'Rhode Island',
        'SC': 'South Carolina',
        'SD': 'South Dakota',
        'TN': 'Tennessee',
        'TX': 'Texas',
        'UT': 'Utah',
        'VA': 'Virginia',
        'VI': 'Virgin Islands',
        'VT': 'Vermont',
        'WA': 'Washington',
        'WI': 'Wisconsin',
        'WV': 'West Virginia',
        'WY': 'Wyoming'
}

def update_plants_that_dont_have_uuid():
    filtered_plants = Plant.objects.filter(uuid=None)
    for plant in filtered_plants:
        plant.uuid = str(uuid.uuid4())
        plant.save()

def get_lat_long():
    # I already ran this method on production instance, 
    # so the lat, long for cities in CA are in the db
    cities = City.objects.filter(state=3924) #state is California
    google_api_key = 'AIzaSyAW1BYBGbfPXqiO31ulqJF7RM5ZyNcJEh8'
    for city in cities:
        try:
            data = requests.get('https://maps.googleapis.com/maps/api/geocode/json?address={},+CA&key=AIzaSyAW1BYBGbfPXqiO31ulqJF7RM5ZyNcJEh8'.format(city.name))
            data = data.json()
            latitude = data['results'][0]['geometry']['location']['lat']
            longitude = data['results'][0]['geometry']['location']['lng']
            print(city.name, latitude, longitude)
            City.objects.filter(name=city.name, state=3924).update(latitude=latitude, longitude=longitude)
        except:
            print city.name


# need to call this view when getting lat, long from pvoutput
def update_address_by_lat_long(lat, long):
    google_api_key = 'AIzaSyAW1BYBGbfPXqiO31ulqJF7RM5ZyNcJEh8'
    #example_url = 'https://maps.googleapis.com/maps/api/geocode/json?latlng=34.290657,-118.536841&key=AIzaSyAW1BYBGbfPXqiO31ulqJF7RM5ZyNcJEh8'
    url = 'https://maps.googleapis.com/maps/api/geocode/json?latlng={},{}&key={}'.format(lat, long, google_api_key)
    data = requests.get(url)
    data = data.json()
    full_address = u''.join(data['results'][0]['formatted_address']).encode('utf-8').split(',')
    address_line_1 = full_address[0]
    city = full_address[1].replace(" ", "")
    state = re.sub(" \d+", " ", full_address[2])
    state = states.get(state.replace(" ", ""))
    country = full_address[3].replace(" ", "")

    return address_line_1, city, state, country

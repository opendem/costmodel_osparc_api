# from django_filters.rest_framework import FilterSet, filters
import rest_framework_filters as filters
from models import *
from costmodel import models as costmodel_models
from osparc import models as osparc_models
from costmodel.filters import CostModelFilter
from osparc.filters import PlantReportFilter, UploadActivityFilter


class OrganizationFilter(filters.FilterSet):

    class Meta:
        model = Organization
        fields = {
            'name': '__all__',
        }


class UserPermissionFilter(filters.FilterSet):

    class Meta:
        model = UserPermission
        fields = {
            'name': '__all__',
        }


class UserFilter(filters.FilterSet):
    user_permission = filters.RelatedFilter(filterset=UserPermissionFilter, queryset=UserPermission.objects.all())
    organization = filters.RelatedFilter(filterset=OrganizationFilter, queryset=Organization.objects.all())

    class Meta:
        model = User
        fields = {
            'email': '__all__',
            'first_name': '__all__',
            'last_name': '__all__',
            'date_joined': '__all__',
            'is_active': '__all__',
            'failure_analysis_available': '__all__',
        }


class OrganizationUserFilter(filters.FilterSet):
    user_permission = filters.RelatedFilter(filterset=UserPermissionFilter, queryset=UserPermission.objects.all())

    class Meta:
        model = User
        fields = {
            'email': '__all__',
            'first_name': '__all__',
            'last_name': '__all__',
            'date_joined': '__all__',
            'is_active': '__all__',
            'failure_analysis_available': '__all__',
        }


class PVMarketSectorFilter(filters.FilterSet):

    class Meta:
        model = PVMarketSector
        fields = {
            'name': '__all__',
        }


class InverterTypeFilter(filters.FilterSet):

    class Meta:
        model = InverterType
        fields = {
            'name': '__all__',
        }


class ModuleTypeFilter(filters.FilterSet):

    class Meta:
        model = ModuleType
        fields = {
            'name': '__all__',
        }


class MountingTypeFilter(filters.FilterSet):

    class Meta:
        model = MountingType
        fields = {
            'name': '__all__',
        }


class MountingLocationFilter(filters.FilterSet):

    class Meta:
        model = MountingLocation
        fields = {
            'name': '__all__',
        }


class RoofSlopeTypeFilter(filters.FilterSet):

    class Meta:
        model = RoofSlopeType
        fields = {
            'name': '__all__',
        }


class RoofTypeFilter(filters.FilterSet):

    class Meta:
        model = RoofType
        fields = {
            'name': '__all__',
        }


class InspectionTechniqueFilter(filters.FilterSet):

    class Meta:
        model = InspectionTechnique
        fields = {
            'name': '__all__',
        }


class TrackingTypeFilter(filters.FilterSet):

    class Meta:
        model = TrackingType
        fields = {
            'name': '__all__',
        }


class WeatherSourceFilter(filters.FilterSet):

    class Meta:
        model = WeatherSource
        fields = {
            'name': '__all__',
        }


class EnvironmentalConditionFilter(filters.FilterSet):

    class Meta:
        model = EnvironmentalCondition
        fields = {
            'name': '__all__',
        }


class PlantFinanceFilter(filters.FilterSet):

    class Meta:
        model = PlantFinance
        fields = {
            'system_installed_cost': '__all__',

            'analysis_period': '__all__',
            'discount_rate': '__all__',
            'inflation_rate': '__all__',
            'desired_confidence_that_reserve_covers_cost': '__all__',

            'epc_warranty': '__all__',
            'inverter_warranty': '__all__',
            'module_warranty': '__all__',
            'monitoring_warranty': '__all__',

            'working_hours': '__all__'
        }


class PlantEquipmentFilter(filters.FilterSet):
    inverter_type = filters.RelatedFilter(filterset=InverterTypeFilter, queryset=InverterType.objects.all())
    module_type = filters.RelatedFilter(filterset=ModuleTypeFilter, queryset=ModuleType.objects.all())
    mounting_type = filters.RelatedFilter(filterset=MountingTypeFilter, queryset=MountingType.objects.all())
    mounting_location = filters.RelatedFilter(filterset=MountingLocationFilter, queryset=MountingLocation.objects.all())
    roof_slope_type = filters.RelatedFilter(filterset=RoofSlopeTypeFilter, queryset=RoofSlopeType.objects.all())
    roof_type = filters.RelatedFilter(filterset=RoofTypeFilter, queryset=RoofType.objects.all())
    inspection_technique = filters.RelatedFilter(filterset=InspectionTechniqueFilter, queryset=InspectionTechnique.objects.all())
    tracking = filters.RelatedFilter(filterset=TrackingTypeFilter, queryset=TrackingType.objects.all())

    class Meta:
        model = PlantEquipment
        fields = {
            'inverter_replacement_cost': '__all__',
            'inverter_capacity': '__all__',
            'number_of_inverters': '__all__',

            'number_of_transformers': '__all__',

            'module_efficiency': '__all__',
            'module_power': '__all__',
            'modules_per_row': '__all__',
            'modules_per_string': '__all__',

            'array_area_per_roof_attachment': '__all__',
            'combiner_boxes_per_dcd': '__all__',
            'foundations_per_row': '__all__',
            'gcr': '__all__',
            'rows_per_tracked_block': '__all__',
            'strings_per_combiner_box': '__all__',
            'tilt': '__all__',
            'azimuth': '__all__',

            # 'array_area': '__all__',
            # 'number_of_modules': '__all__',
            # 'number_of_strings': '__all__',
            # 'number_of_combiner_boxes': '__all__',
            # 'number_of_dc_disconnects': '__all__',
            # 'number_of_roof_attachments': '__all__',
            # 'total_tracking_blocks': '__all__',
            # 'total_rows': '__all__',

        }


class PlantFilter(filters.FilterSet):
    pv_market_sector = filters.RelatedFilter(filterset=PVMarketSectorFilter, queryset=PVMarketSector.objects.all())
    plant_finance = filters.RelatedFilter(filterset=PlantFinanceFilter, queryset=PlantFinance.objects.all())
    plant_equipment = filters.RelatedFilter(filterset=PlantEquipmentFilter, queryset=PlantEquipment.objects.all())
    plant_report = filters.RelatedFilter(filterset=PlantReportFilter, queryset=osparc_models.PlantReport.objects.all())
    environmental_condition = filters.RelatedFilter(filterset=EnvironmentalConditionFilter, queryset=EnvironmentalCondition.objects.all())
    cost_model = filters.RelatedFilter(filterset=CostModelFilter, queryset=costmodel_models.CostModel.objects.all())
    active_cost_model = filters.RelatedFilter(filterset=CostModelFilter, queryset=costmodel_models.CostModel.objects.all())
    weather_source = filters.RelatedFilter(filterset=WeatherSourceFilter, queryset=WeatherSource.objects.all())
    upload_activity = filters.RelatedFilter(filterset=UploadActivityFilter, queryset=osparc_models.UploadActivity.objects.all())

    class Meta:
        model = Plant
        fields = {
            'uuid': '__all__',
            'name': '__all__',
            'description': '__all__',
            'activation_date': '__all__',

            'address_line_1': '__all__',
            'address_line_2': '__all__',
            'city': '__all__',
            'county': '__all__',
            'state': '__all__',
            'country': '__all__',
            'postal_code': '__all__',
            'latitude': '__all__',
            'longitude': '__all__',
            'time_zone': '__all__',

            'dc_rating': '__all__',
            'derate_factor': '__all__',
            'energy_yield': '__all__',
            'storage_original_capacity': '__all__',
            'storage_current_capacity': '__all__',
            'storage_state_of_charge': '__all__',

            'record_status': '__all__',
            'version_creation_time': '__all__',
            'version_id': '__all__',
            'solar_anywhere_site': '__all__',

            # 'site_area': '__all__',

        }

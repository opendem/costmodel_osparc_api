import csv
import requests, zipfile, StringIO
import json
import urllib2
import calendar
import dateparser
import datetime
import re
import uuid
import time
from bs4 import BeautifulSoup
from requests.auth import HTTPBasicAuth
import xml.etree.ElementTree as ET
from celery import shared_task

from django.core.mail import send_mail
from django.template.loader import render_to_string
from django.conf import settings
from django.core.exceptions import (
    MultipleObjectsReturned, ObjectDoesNotExist
)

from django.conf import settings
#from costmodel_osparc_api.settings import STATIC_ROOT

from core.models import (
    PlantEquipment, PlantFinance, Plant,
    InverterType, ModuleType, MountingType,
    MountingLocation, RoofSlopeType, RoofType,
    InspectionTechnique, TrackingType, PVMarketSector,
    EnvironmentalCondition, Organization, WeatherSource,
)
from core.models import Plant
from osparc.models import PlantTimeSeries
from costmodel.models import State, City
from core.utils import states, update_address_by_lat_long


class PlantImport(object):
    """
    Encapsulates functionality to download and parse
    plants data files and store them into database.
    """

    origin_name = None
    plant_file_url = None
    timeseries_file_url = None

    def __init__(self):
        self.read_files()
        self.create_objects()

    def create_objects(self):
        plants_created = 0
        ts_created = 0
        for plant in self.plants_data:
            if not plant[0]:
                continue
            plantobj = self.get_or_create_plant(plant)
            plants_created += 1
            for ts_entry in self.timeseries_data[plant[0]]:
                all_ts = PlantTimeSeries.objects.filter(plant=plantobj, time_stamp=dateparser.parse(ts_entry[-2]))
                if not all_ts:
                    ts_id = self.create_timeseries(ts_entry, plantobj)
                    ts_created += 1
        # need to return plants_created and ts_created, so that we have a count to return to user in the email
        return plants_created, ts_created

    def read_files(self):
        plant_response = requests.get(self.plant_file_url, stream=True)
        plant_response.raise_for_status()
        plant_response = zipfile.ZipFile(StringIO.StringIO(plant_response.content))
        plant_response.extractall()

        plant_file = plant_response.open(plant_response.filelist[0])

        timeseries_response = requests.get(self.timeseries_file_url, stream=True)
        timeseries_response.raise_for_status()
        self.timeseries_data = {}

        row = 0
        for ts_entry in timeseries_response.iter_lines():
            if row == 0:
                row += 1
                continue

            ts_entry = tuple(ts_entry.strip().split(','))
            if ts_entry[0] in self.timeseries_data:
                self.timeseries_data[ts_entry[0]].append(ts_entry[1:])
            else:
                self.timeseries_data[ts_entry[0]] = [ts_entry[1:]]

        self.plants_data = []
        row = 0
        for plant_entry in plant_file.readlines():
            if row == 0:
                row += 1
                continue

            plant_entry = plant_entry.strip().split(',')
            if self.is_plant_valid(plant_entry) and plant_entry[0] in self.timeseries_data:
                self.plants_data.append(plant_entry)
            # else
            # skip the record

    def is_plant_valid(self, data):
        raise Exception("Implement this method")

    def get_plant_kwargs(self, data):
        raise Exception("Implement this method")

    def get_plant_finance_kwargs(self, data):
        raise Exception("Implement this method")

    def get_plant_equipment_kwargs(self, data):
        raise Exception("Implement this method")

    def get_timeseries_kwargs(self, data):
        raise Exception("Implement this method")

    def create_plant_finance(self, data):
        finance = PlantFinance.objects.create(
            **self.get_plant_finance_kwargs(data)
        )
        return finance

    def create_plant_equipment(self, data):
        equipment = PlantEquipment.objects.create(
            **self.get_plant_equipment_kwargs(data)
        )
        return equipment

    def get_or_create_plant(self, data):
        # check if plant exists in database already
        try:
            plant = Plant.objects.get(plant_origin_id=self.get_plant_kwargs(data)['plant_origin_id'])
        except ObjectDoesNotExist:
            plant_equipment = self.create_plant_equipment(data)
            plant_finance = self.create_plant_finance(data)

            plant_kwargs = self.get_plant_kwargs(data)
            plant_kwargs.update({
                'plant_equipment': plant_equipment,
                'plant_finance': plant_finance
            })
            plant = Plant.objects.create(**plant_kwargs)
            
        return plant

    def get_weather_data(self, site_name, latitude, longitude, start_time, end_time):
        try:
            site_data = self.weather_data_payload(site_name, latitude, longitude, start_time, end_time)
            soup = BeautifulSoup(site_data, "html.parser")
            data_periods = soup.find_all('weatherdataperiods')
            all_data = []
            for data in data_periods:
                data_period = soup.find_all('weatherdataperiod')
                for period in data_period:
                    if not period.get('ambienttemperature_degreesc') or not period.get('globalhorizontalirradiance_wattspermetersquared'):
                        continue
                    data = {
                        'global_horizontal_irradiance': period['globalhorizontalirradiance_wattspermetersquared'],
                        'ambient_temperature': period['ambienttemperature_degreesc'],
                    }
                    all_data.append(data)
        except:
            all_data = []
        return all_data

    def create_timeseries(self, data, plant):
        ts_kwargs = self.get_timeseries_kwargs(data, plant)
        timeseries = PlantTimeSeries.objects.create(**ts_kwargs)

    def get_plant_attribs(self):
        return Plant._fields

    def weather_data_payload(self, site_name, latitude, longitude, start_time, end_time):
        url = "https://data.solaranywhere.com/api/v2/WeatherData"
        username = "APSuite@sunspec.org"
        password = "}%N[ja)8u#"
        querystring = {"key":"SUNSX3F2"}
        payload = """<CreateWeatherDataRequest xmlns="http://service.solaranywhere.com/api/v2">
                 <Sites>
                  <Site Name="{}" Latitude="{}" Longitude="{}" />
                 </Sites>
                 <Options
                 WeatherDataSource="SolarAnywhere3_2"
                 WeatherDataPreference = "Auto"
                 PerformTimeShifting = "false"
                 StartTime="{}"
                 EndTime="{}"
                 SpatialResolution_Degrees="0.1"
                 TimeResolution_Minutes="60"
                 OutputFields="StartTime,ObservationTime,EndTime,GlobalHorizontalIrradiance_WattsPerMeterSquared,DirectNormalIrradiance_WattsPerMeterSquared,DiffuseHorizontalIrradiance_WattsPerMeterSquared,IrradianceObservationType,AmbientTemperature_DegreesC,AmbientTemperatureObservationType,WindSpeed_MetersPerSecond,WindSpeedObservationType,WindDirection_Degrees,RelativeHumidity_Percent,SnowDepth_Meters,LiquidPrecipitation_KilogramsPerMeterSquared,SolidPrecipitation_KilogramsPerMeterSquared"
                 SummaryOutputFields="TotalGlobalHorizontalIrradiance"/>
                </CreateWeatherDataRequest>""".format(site_name, latitude, longitude, start_time, end_time)
        headers = {
            'content-type': "text/xml; charset=utf-8",
            'content-length': "length",
        }
        response = requests.post(url,auth = HTTPBasicAuth(username,password),data=payload,headers=headers,params=querystring)
        root = ET.fromstring(response.content)
        public_id = root.attrib.get("WeatherRequestId")
        print public_id
        print start_time
        print end_time
        #GET WeatherDataResult
        url2 = "https://data.solaranywhere.com/api/v2/WeatherDataResult/"
        request_number = 0
        max_request_number = 100
        while(request_number < max_request_number):
            data = requests.get(url2 + public_id,auth = HTTPBasicAuth(username,password))
            radicle = ET.fromstring(data.content)
            status = radicle.attrib.get("Status")
            if status == 'Done':
                return data.content
            else:
                #maybe need a diff return status here and use status above to implement
                # a check in our payload
                request_number = request_number + 1


class CSIPlantImport(PlantImport):
    origin_name = "CSI"
    plant_file_url = 'https://www.californiasolarstatistics.ca.gov/data_downloads/working/'
    timeseries_file_url = 'https://www.californiasolarstatistics.ca.gov/data_downloads/measured_production/'

    def is_plant_valid(self, data):
        if len(data) < 119:
            return False

        if not data[33]:
            return False

        if not re.match(r'\d{4,5}', data[21]):
            return False

        return True

    def get_plant_kwargs(self, data):
        # get state name equivalent from utils
        try:
            for k, v in states.iteritems():
                if data[20] != 'CA':
                    state_name = 'California'
                if k in data[20]:
                    state_name = v

            # get state
            state = State.objects.filter(name=state_name, country=231)[0]
        
            # get market sector
            sectors = ['Residential', 'Commercial']
            if any(sector in data[15] for sector in sectors):
                market_sector = PVMarketSector.objects.get(name=data[15])
            else:
                market_sector = PVMarketSector.objects.get(id=1)

            plant_data = {
                'uuid': str(uuid.uuid4()),
                'plant_origin': self.origin_name,
                'plant_origin_id': data[0],
                'name': data[0],
                'county': data[19] + " County",
                'state': state,
                'postal_code': data[21],
                'time_zone': "America/Los Angeles",
                'dc_rating': float(data[8]) * 1000,
                'derate_factor': float(data[11])/float(data[8]),
                'storage_original_capacity': float(data[8]),
                'storage_current_capacity': float(data[8]) - (float(data[8]) * 0.667),
                'storage_state_of_charge': 0,
                'pv_market_sector': market_sector,
                'activation_date': data[33],
                'organization': Organization.objects.get(pk=1),
                'weather_source': WeatherSource.objects.get(pk=6),
            }

            # get cities
            try:
                # because of faulty data in the csv, sometimes data[18] is not an 
                # actual city, therefore we must test for that scenario and give
                # a diff a general CA lat, long so that we can actually retrieve 
                # weather data
                city = City.objects.filter(name=data[18], state=3924)[0]
                plant_data.update({
                    'city': city,
                    'latitude': city.latitude,
                    'longitude': city.longitude
                })
            except:
                plant_data.update({
                    'latitude': 36.744698,
                    'longitude': -121.360946,
                })
                
        except:
            plant_data = {}
        
        return plant_data

    def get_plant_finance_kwargs(self, data):
        try:
            system_cost = data[7]
            if not isinstance(system_cost, int):
                system_cost = None
        except:
            system_cost = None
        return {
            'system_installed_cost': system_cost,
        }

    def get_plant_equipment_kwargs(self, data):
        try:
            tilt = data[111]
            if not isinstance(tilt, int):
                tilt = None
        except IndexError:
            tilt = None
        try:
            azimuth = data[110]
            if not isinstance(azimuth, int):
                azimuth = None
        except IndexError:
            azimuth = None

        return { 
            'tilt': tilt,
            'azimuth': azimuth,
        }

    def get_timeseries_kwargs(self, data, plant):
        
        start_time = dateparser.parse(data[9])
        if start_time > dateparser.parse('1 day ago'):
            start_time = dateparser.parse('1 day ago')

        end_time = start_time + datetime.timedelta(days=1)

        time_tpl = '%Y-%m-%dT%H:%M:%S+00:00'
        stfmt = start_time.strftime(time_tpl)
        etfmt = end_time.strftime(time_tpl)

        weather_data = self.get_weather_data(
            plant.name,
            plant.latitude,
            plant.longitude,
            stfmt,
            etfmt)
        
        if weather_data:
            total_ghi = sum([int(dt['global_horizontal_irradiance']) for dt in weather_data])
            tmpamb = [int(dt['ambient_temperature']) for dt in weather_data]
            avg_tmpamb = float(sum(tmpamb) / len(tmpamb))

        else:
            total_ghi = None
            avg_tmpamb = None

        return {
            'time_stamp': data[9],
            'sample_interval': int((calendar.mdays[int(data[-2][5:7])] * 24) * 60),
            'WH_DIFF': int(data[10]),
            'GHI_DIFF': total_ghi,
            'TMPAMB_AVG': avg_tmpamb,
            'HPOA_DIFF': None,
            'plant': plant,
        }


class PVOutputPlantImport(PlantImport):
    origin_name = "PVOUTPUT"

    def is_plant_valid(self, data):
        if len(data) < 13: 
            return False

        if not data[12]:
            return False

        return True

    def read_files(self):
        api_key = 'ac2ca9078472d50d677c965154840e829d61e942'

        # get list of system ids located in the US based on CSV
        # provided from PVOutput.org
        list_of_ids_csv_file = settings.STATIC_ROOT + '/task_files/pvoutput_live_sid_us_20170714.csv'       
        file = open(list_of_ids_csv_file, 'rb')
        Reader = csv.reader(file)
        Data = list(Reader)

        list_of_ids = [data[0] for data in Data[1:]]

        self.plants_data = []
        get_system_url = 'https://pvoutput.org/service/r2/getsystem.jsp'
        for id in list_of_ids:
            payload = {
                'sid': '55789',
                'key': api_key,
                'sid1': id,
            }
            pvoutput_req = requests.get(get_system_url, params=payload)
            # can only do 300 requests per house so we need a time.sleep
            time.sleep(12) # change this to 12 secs when running the full import
            pvoutput_data = u''.join(pvoutput_req.text).encode('ascii', 'ignore').split(',')
            pvoutput_data.append(payload['sid1'])

            if self.is_plant_valid(pvoutput_data):
                self.plants_data.append(pvoutput_data)

        self.pv_weather_data = {}
        weather_url = 'https://pvoutput.org/service/r2/getoutput.jsp'
        for id in list_of_ids:
            payload = {
                'sid': '55789',
                'key': api_key,
                'sid1': id,
                'insolation': 1,    
            }
            pvoutput_weather_req = requests.get(weather_url, params=payload)
            # can only do 300 requests per house so we need a time.sleep
            time.sleep(12) # change this to 12 secs when running the full import
            weather_req_data = str(pvoutput_weather_req.text).split(';')
            all_data = [d.split(',') for d in weather_req_data]
            self.pv_weather_data[id] = all_data

    def create_objects(self):
        plants_created = 0
        ts_created = 0
        
        for plant in self.plants_data:
            if not plant[0]:
                continue
            plantobj = self.get_or_create_plant(plant)
            plants_created += 1
            for ts_entry in self.pv_weather_data[plant[-1]]:
                #check to see if object already exists in db
                all_ts = PlantTimeSeries.objects.filter(
                    plant=plantobj,
                    time_stamp=dateparser.parse(
                        ts_entry[0],
                        settings={'DATE_ORDER': 'YMD'}).strftime('%Y-%m-%d')
                )
                if not all_ts:
                    ts_id = self.create_timeseries(ts_entry, plantobj)
                    ts_created += 1

        return plants_created, ts_created

    def get_plant_kwargs(self, data):
    # order of data is: System Name [0], System Size(watts) [1], Postal Code [2],
    # Number of Panels [3], Panel Power(watts) [4], Panel Brand [5], No of Inverters [6],
    # Inverter Power(watts) [7], Inverter Brand [8], Orientation [9], Tilt [10]
    # Shade [11], Install Date(yyyymmdd) [12], Latitude [13], Longitude [14], plant_origin_id [-1]
        plant_data = {
            'uuid': str(uuid.uuid4()),
            'plant_origin': self.origin_name,
            'plant_origin_id': data[-1],
            'name': data[0],
            'postal_code': data[2],
            'dc_rating': float(data[1]),
            'storage_original_capacity': float(data[1]),
            'storage_current_capacity': float(data[1]) - (float(data[1]) * 0.667),
            'activation_date': dateparser.parse(data[12], settings={'DATE_ORDER': 'YMD'}).strftime('%Y-%m-%d'),
            'organization': Organization.objects.get(pk=1),
            'storage_state_of_charge': 0,
            'latitude': data[13],
            'longitude': data[14],
            'weather_source': WeatherSource.objects.get(pk=6),
        }

        address_components = update_address_by_lat_long(str(data[13]), str(data[14]))
        try:
            state_no = State.objects.get(country=231, name=address_components[2])
        except ObjectDoesNotExist:
            state_no = '3924'

        try:
            city_no = City.objects.filter(name=address_components[1], state=state_no)
        except ObjectDoesNotExist:
            city_no = None

        plant_data.update({
            'state': state_no,
            'city': city_no[0] if city_no else None,
            'address_line_1': address_components[0],
            'country': address_components[3],
        })
        
        return plant_data

    def get_plant_finance_kwargs(self, data):
        pf_data = {
            'system_installed_cost': 2.5*int(data[1]),
        }
        return pf_data

    def get_plant_equipment_kwargs(self, data):
        if not 'Na' in data[10]:
            tilt = int(data[10].split('.')[0])
        else:
            tilt = None

        pe_data = {
            'tilt': tilt,
            'inverter_capacity': float(data[7]),
            'number_of_inverters': int(data[6]),
            'module_power': float(data[4]),
        }
        return pe_data

    def get_timeseries_kwargs(self, data, plant):
        start_time = dateparser.parse(data[0], settings={'DATE_ORDER': 'YMD'})

        if start_time > dateparser.parse('1 day ago'):
            start_time = dateparser.parse('1 day ago')

        end_time = start_time + datetime.timedelta(days=1)

        time_tpl = '%Y-%m-%dT%H:%M:%S+00:00'
        stfmt = start_time.strftime(time_tpl)
        etfmt = end_time.strftime(time_tpl)

        weather_data = self.get_weather_data(
            plant.name,
            plant.latitude,
            plant.longitude,
            stfmt,
            etfmt)
        
        if weather_data:
            total_ghi = sum([int(dt['global_horizontal_irradiance']) for dt in weather_data])
            tmpamb = [int(dt['ambient_temperature']) for dt in weather_data]
            avg_tmpamb = float(sum(tmpamb) / len(tmpamb))

        else:
            total_ghi = None
            avg_tmpamb = None

        ts_data = {
            'time_stamp': dateparser.parse(
                data[0],
                settings={'DATE_ORDER': 'YMD'}).strftime('%Y-%m-%d'),
            'sample_interval': 1440,
            'WH_DIFF': int(data[1]),
            'GHI_DIFF': total_ghi,
            'TMPAMB_AVG': avg_tmpamb,
            'HPOA_DIFF': None,
            'plant': plant,
        }

        return ts_data


class PVDAQPlantImport(PlantImport):
    origin_name = "PVDAQ"
    api_key = 'x7RWtcDWye7xSQ4IkyLqU1yAXhdLoigzau8LfQyo'


@shared_task
def schedule_import_plants_and_ts(origin, org_id, user_email):
    if 'CSI' in origin:
        csi = CSIPlantImport()
        plant_count, timeseries_count = csi.create_objects()

    if 'PVOUTPUT' in origin:
        pvoutput = PVOutputPlantImport()
        plant_count, timeseries_count = pvoutput.create_objects()

    data = {
        'user_email': user_email,
        'domain': settings.SUNSPEC_DOMAIN,
        'plant_count': plant_count,
        'timeseries_count': timeseries_count,
    }
    email_template_name = 'plant_and_ts_import_email.html'
    email_template = render_to_string(email_template_name, data)
    send_mail(
        'Asset Performance Suite - Plants and TimeSeries Created',
        'Asset Performance Suite - Plants and TimeSeries Created',
        settings.DEFAULT_FROM_EMAIL,
        [user_email],
        html_message=email_template,
        fail_silently=False,
    )

    return origin + " plant import Successfully completed."

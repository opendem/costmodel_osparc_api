from rest_framework import serializers
from models import *


# Organizations, User Permissions and Users

class OrganizationSerializer(serializers.ModelSerializer):

    class Meta:
        model = Organization
        fields = ('id', 'name', 'description', 'plant', 'cost_model', 'cost_model_license', 'osparc_license')


class UserPermissionSerializer(serializers.ModelSerializer):

    class Meta:
        model = UserPermission
        fields = '__all__'


class UserSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = '__all__'

    def create(self, validated_data):
        user = User(email=validated_data['email'])
        user.first_name = validated_data['first_name']
        user.last_name = validated_data['last_name']
        user.set_password(validated_data['password'])
        user.organization = validated_data['organization']
        user.user_permission = validated_data['user_permission']
        user.failure_analysis_available = validated_data['failure_analysis_available']
        user.save()
        # user.groups.add(validated_data['groups'][0])
        # user.save()
        return user


# Plants

class PlantSerializer(serializers.ModelSerializer):
    site_area = serializers.ReadOnlyField()

    class Meta:
        model = Plant
        fields = '__all__'


class PlantEquipmentSerializer(serializers.ModelSerializer):
    array_area = serializers.ReadOnlyField()
    number_of_modules = serializers.ReadOnlyField()
    number_of_strings = serializers.ReadOnlyField()
    number_of_combiner_boxes = serializers.ReadOnlyField()
    number_of_dc_disconnects = serializers.ReadOnlyField()
    number_of_roof_attachments = serializers.ReadOnlyField()
    total_rows = serializers.ReadOnlyField()
    total_tracking_blocks = serializers.ReadOnlyField()
    whole_system_degradation_rate = serializers.ReadOnlyField()

    class Meta:
        model = PlantEquipment
        fields = '__all__'


class PlantFinanceSerializer(serializers.ModelSerializer):

    class Meta:
        model = PlantFinance
        fields = '__all__'


# Enumerations

class MountingLocationSerializer(serializers.ModelSerializer):

    class Meta:
        model = MountingLocation
        fields = '__all__'


class MountingTypeSerializer(serializers.ModelSerializer):

    class Meta:
        model = MountingType
        fields = '__all__'


class PVMarketSectorSerializer(serializers.ModelSerializer):

    class Meta:
        model = PVMarketSector
        fields = '__all__'


class EnvironmentalConditionSerializer(serializers.ModelSerializer):

    class Meta:
        model = EnvironmentalCondition
        fields = '__all__'


class RoofTypeSerializer(serializers.ModelSerializer):

    class Meta:
        model = RoofType
        fields = '__all__'


class RoofSlopeTypeSerializer(serializers.ModelSerializer):

    class Meta:
        model = RoofSlopeType
        fields = '__all__'


class TrackingTypeSerializer(serializers.ModelSerializer):

    class Meta:
        model = TrackingType
        fields = '__all__'


class InspectionTechniqueSerializer(serializers.ModelSerializer):

    class Meta:
        model = InspectionTechnique
        fields = '__all__'


class InverterTypeSerializer(serializers.ModelSerializer):

    class Meta:
        model = InverterType
        fields = '__all__'


class ModuleTypeSerializer(serializers.ModelSerializer):

    class Meta:
        model = ModuleType
        fields = '__all__'


class WeatherSourceSerializer(serializers.ModelSerializer):

    class Meta:
        model = WeatherSource
        fields = '__all__'

class FileUploaderSerializer(serializers.ModelSerializer):

    class Meta:
        model = FileUploader
        fields = '__all__'

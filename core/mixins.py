class DepthSerializerMixin(object):
    def get_serializer_class(self, *args, **kwargs):
        serializer_class = super(DepthSerializerMixin, self).get_serializer_class(*args, **kwargs)
        query_params = self.request.query_params
        depth = query_params.get('depth', None)
        if depth is not None:
            serializer_class.Meta.depth = int(depth)
        else:
            serializer_class.Meta.depth = 0
        return serializer_class
